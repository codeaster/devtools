# set devtools environment
DEVTOOLS_ROOT=$(readlink -n -f $(dirname $(dirname ${BASH_SOURCE})))

# all
PATH=${DEVTOOLS_ROOT}/bin:${DEVTOOLS_ROOT}/bin/maint:$PATH
export PATH

PYTHONPATH=${DEVTOOLS_ROOT}/lib:$PYTHONPATH
export PYTHONPATH

. "${DEVTOOLS_ROOT}/etc/devtools_functions.sh"

# source lmod.sh/modules.sh if necessary
module list > /dev/null 2>&1
ok_mod=$?
if [ ${ok_mod} -ne 0 ]; then
    source_module=/etc/profile.d/lmod.sh
    if [ -f "${source_module}" ]; then
        . "${source_module}"
    else
        source_module=/etc/profile.d/modules.sh
        if [ -f "${source_module}" ]; then
            . "${source_module}"
        else
            source_module=
        fi
    fi
fi

# alias pour thg en Python2
alias thg='PYTHONPATH= thg'


host=$(detect_host)

# cronos
if [ "${host}" = "cronos" ]; then
    ASTER_INSTALLROOT=/software/restricted/simumeca/aster
    export PATH=${ASTER_INSTALLROOT}/bin:${PATH}
fi

# gaia
if [ "${host}" = "gaia" ]; then
    ASTER_INSTALLROOT=/projets/simumeca
    ASTER_ROOT=${ASTER_INSTALLROOT}
    ASTER_ETC=${ASTER_ROOT}/etc
    # for as_run
    module load python/3.6.5
    export PATH=${ASTER_INSTALLROOT}/bin:${PATH}
    # To be able to submit a job with Slurm
    export SBATCH_WCKEY=P11YB:ASTER
    export SLURM_WCKEY=P11YB:ASTER
fi

export ASTER_ROOT
export ASTER_INSTALLROOT
