#
# set environment to build the 'oldstable' version + MPI
#

export ASTER_BRANCH_ID=v14_python3

# devtools environment
DEVTOOLS_ROOT=$(dirname $(dirname ${BASH_SOURCE}))
. ${DEVTOOLS_ROOT}/etc/env_legacy.sh $*

# cronos - do not source std environment
if [ "${DEVTOOLS_COMPUTER_ID}" = "cronos" ]; then
    . ${ASTER_INSTALLROOT}/prerequisites/20190513-med41/gcc8-mkl-ompi4/cronos_mpi.sh
    # keep rootdir first (for as_run)
    export PATH=${ASTER_INSTALLROOT}/bin:${PATH}
    return
fi


. ${DEVTOOLS_ROOT}/etc/env_oldstable.sh $*

# eole
if [ "$DEVTOOLS_COMPUTER_ID" = "eole" ]; then
    module load mkl/2017.0.098 impi/2017.0.098
fi

# gaia
if [ "$DEVTOOLS_COMPUTER_ID" = "gaia" ]; then
    module purge
    module load ifort/2019.0.045 icc/2019.0.045 mkl/2019.0.045 impi/2019.0.045
    export CC=mpiicc
    export FC=mpiifort
    export CXX=mpiicc
fi

# calibre9
if [ "$DEVTOOLS_COMPUTER_ID" = "calibre9" ]; then
    true
fi

# clap0f0q
if [ "$DEVTOOLS_COMPUTER_ID" = "clap0f0q" ]; then
    echo "MPI configuration unsupported for 'clap0f0q'"
fi
