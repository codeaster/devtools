# devtools helper functions

_error()
{
    # usage: _error message
    echo "ERROR: ${1}"
    echo
    exit 1
}

env_printf()
{
    [ "${DEVTOOLS_ENV_SILENT}" = 1 ] && return
    printf "$1"
}

source_codeaster_env()
{
    # usage: source_codeaster_env PREFIX
    #   source code_aster environment (very similar to src/waf.main)
    declare -a src=( "." "src" "../src" "${1}/src" "${HOME}/dev/codeaster/src" )
    WAF_SUFFIX="${WAF_SUFFIX:-mpi}"
    # source the environment only if DEVTOOLS_COMPUTER_ID is not already defined
    env_printf "checking environment... "
    if [ -z "${DEVTOOLS_COMPUTER_ID}" ]; then
        host=$(detect_host)
        if [ ! -z "${host}" ]; then
            WAFBUILD_ENV=""
            for path in ${src[@]}; do
                path=$(readlink -n -f "${path}")
                export WAFBUILD_ENV="$(get_wafbuild_env ${host} "${path}")"
                if [ -e "${WAFBUILD_ENV}" ]; then
                    . "${WAFBUILD_ENV}"
                    env_printf "loading ${WAFBUILD_ENV}\n"
                    break
                fi
            done
            if [ -z "${WAFBUILD_ENV}" ]; then
                unset WAFBUILD_ENV
                env_printf "no found\n"
            fi
        else
            env_printf "no found\n"
        fi
    else
        if [ -z "${WAFBUILD_ENV}" ]; then
            export WAFBUILD_ENV="$(get_wafbuild_env ${DEVTOOLS_COMPUTER_ID} .)"
        fi
        if [ -e ${WAFBUILD_ENV} ]; then
            env_printf "already set: ${WAFBUILD_ENV}\n"
        else
            env_printf "no found: ${WAFBUILD_ENV}\n"
            unset WAFBUILD_ENV
        fi
    fi
}

detect_host()
{
    if [ ! -z "${SINGULARITY_NAME}" ]; then
        local wbe=$(ls /opt/public/*_${WAF_SUFFIX}.sh 2> /dev/null)
        if [ ! -z "${wbe}" ]; then
            plt=$(basename "${wbe}" | sed -e "s%_${WAF_SUFFIX}\.sh$%%")
            printf "${plt}"
            return
        fi
    fi
    if [ -f /software/restricted/simumeca/aster/cronos ]; then
        printf cronos
    elif [ -f /projets/simumeca/eole ]; then
        printf eole
    elif [ -f /projets/simumeca/gaia ]; then
        printf gaia
    elif egrep -q "Scibian 10.0" /etc/issue; then
        printf scibian10
    elif egrep -q "Scibian 9.0" /etc/issue; then
        printf scibian9
    elif egrep -q "Calibre 9|Debian GNU/Linux 8" /etc/issue; then
        printf calibre9
    elif [ -d /efs/software/codeaster ]; then
        printf hpc-aws-rh8
    fi
    printf ""
}

get_wafbuild_env()
{
    # usage: get_wafbuild_env platform prefix
    local plt="${1}"
    local wbe="/opt/public/${plt}_${WAF_SUFFIX}.sh"
    if [ ! -f "${wbe}" ]; then
        wbe="${2}/env.d/${plt}_${WAF_SUFFIX}.sh"
        [ ! -f "${wbe}" ] && wbe=""
    fi
    printf "${wbe}"
}

rotatelog()
{
    # usage: rotatelog logfile
    local logf="${1}"
    rm -f "${logf}".9
    for ((i=8; i>=1; i--)); do
        if [ -f "${logf}".${i} ]; then
            mv "${logf}".${i} "${logf}".$[${i}+1]
        fi
    done
    if [ -f "${logf}" ]; then
        mv "${logf}" "${logf}".1
    fi
    touch "${logf}"
}
