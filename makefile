#:This makefile gives convenient shortcuts to build and check code_aster.
#:
#:Targets:
#:  test            Execute unittests
#:
#:  <testname>      An unknown target is treated as a testname, same 'make test n=testname'
#:
#:Environment variables:
#:  OPTS            Options passed to ctest, example OPTS='-VV' or OPTS='--progress'
#:
#:Check all unittests:
#:      make test
#:
#:Execute a unittest:
#:      make test n=test_hooks
#:or:
#:      make test_hooks

SHELL = /bin/bash
PREFIX := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
OPTS ?= -j $$(nproc)

.PHONY: help test

n ?=
test:
	@( \
		cd $(PREFIX)/share/test ; \
		paths="$(PREFIX)/lib:$(PREFIX)/share/test" ; \
		if [ -z "$${PYTHONPATH}" ]; then \
			export PYTHONPATH="$${paths}" ; \
		else \
			export PYTHONPATH="$${paths}:$${PYTHONPATH}" ; \
		fi ; \
		opts=( "$(OPTS)" ) ; \
		[ ! -z "$(n)" ] && opts+=( "-R" "$(n)" ) ; \
		( set -x ; ctest $${opts[@]} ) ; \
	)

help : makefile
	@sed -n 's/^#://p' $<

#%:
#	@make --no-print-directory test n="$@"
