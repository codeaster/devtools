# Development Tools for code_aster

> Home page: <https://www.code-aster.org/>
>
> Forge: <https://gitlab.com/codeaster/>

**code_aster** source files are dispatched into 3 repositories.

- [src][1]: containing Python, C/C++,
  Fortran source files, its build scripts and most of the testcases,
- *validation*: few testcase files with proprietary datas,
- *data*: material datas that can not be freely distributed.

Other independent repositories exist:

- [devtools][2]: contains helper scripts, **this repository**.
- [changelog][3]: publishes the changelog of each incremental version.

## Content of the [devtools][2] repository

This repository contains some scripts to help to configure the environment,
to configure hooks in the repositories,
to check the sources files...

It also contains python modules to manipulate fortran source files and make
automatically some cleanings in the source files.

Usually the repositories are cloned under `$HOME/dev/codeaster`.

One may add these values to your environment (for example in your `~/.bashrc`)::

```bash
export PATH=$HOME/dev/codeaster/devtools/bin:$PATH
export LD_LIBRARY_PATH=$HOME/dev/codeaster/devtools/lib:$LD_LIBRARY_PATH
```

## Branches

These both branches are existing together:

- `main`: The branch to be used with Git repositories. Some features may be
  not yet available using Git.

- `default`: The development branch to be used with Mercurial repositories.
  It should only be maintained now.

[1]: ../../../../src
[2]: ../../../../devtools
[3]: ../../../../changelog
