#!/bin/bash -e

env

echo "+ downloading version file..."
wget --no-check-certificate ${ROOT_URL}/src/-/raw/${CI_COMMIT_REF_NAME}/env.d/version.sh \
    || wget --no-check-certificate ${ROOT_URL}/src/-/raw/${REFREV}/env.d/version.sh

echo "+ checking for requirements..."
source version.sh
if [ "${VERSION}" != "${PREREQ_VERSION}" ]; then
    echo "Docker image (${PREREQ_VERSION}) and prerequisites version (${VERSION}) are inconsistent"
    exit 1
fi
