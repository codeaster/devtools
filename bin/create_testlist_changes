#!/bin/bash
# --------------------------------------------------------------------
# Copyright (C) 1991 - 2020 - EDF R&D - www.code-aster.org
# This file is part of code_aster.
#
# code_aster is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# code_aster is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with code_aster.  If not, see <http://www.gnu.org/licenses/>.
# --------------------------------------------------------------------

# wrapper script for devtools entrypoints

set_prefix() {
    local this=$(readlink -n -f "$1")
    bindir=$(dirname "${this}")
    prefix=$(dirname "${bindir}")
}

set_prefix "${0}"

usage()
{
    echo "usage: ${0} [arguments] [changed-files]"
    echo
    echo "  This script shows the list of testcases that depend on the current"
    echo "  changes (using 'git status') or the given filenames (only basenames"
    echo "  are considered)."
    echo
    echo "arguments:"
    echo "  -o/--output FILE    destination file for the testlist"
    echo "                      (default: printed on stdout)"
    exit 1
}

_error()
{
    # usage: _error messsage
    echo "ERROR: ${1}"
    echo
    usage
}

create_testlist_changes_main()
{
    local output
    OPTS=$(getopt -o ho: --long help,output: -n $(basename $0) -- "$@")
    if [ $? != 0 ] ; then
        _error "invalid arguments." >&2
    fi
    eval set -- "$OPTS"
    while true; do
        case "$1" in
            -h | --help ) usage ;;
            -o | --output ) output="$2"; shift ;;
            -- ) shift; break ;;
            * ) break ;;
        esac
        shift
    done
    local -a args=( "${@}" )
    local cmt
    [ -z "${output}" ] && cmt="# "
    [ ! -d astest ] && _error "'astest' directory not found here!"

    local -a changes
    if [ ${#args[@]} -eq 0 ]; then
        changes=( $(git diff --name-only astest | sed -e 's%astest/%%g') )
    else
        for file in "${args[@]}"; do
            changes=( "${changes[@]}" $(basename "${file}") )
        done
    fi

    echo "${cmd}${#changes[@]} files changed, searching testcases depending on..."

    local testlist=$(mktemp)
    pushd astest > /dev/null
    for file in "${changes[@]}"; do
        grep -l "${file}" *.export | sed -e 's%\.export%%g' >> "${testlist}"
    done
    popd > /dev/null
    if [ -z "${output}" ]; then
        sort -u "${testlist}"
    else
        sort -u "${testlist}" > "${output}"
    fi
    rm -f "${testlist}"
    return $?
}

create_testlist_changes_main "${@}"
exit $?
