#!/usr/bin/env python3
# coding: utf-8

"""
    %(prog)s [options]

A "pretxnchangegroup" hook that checks that there is only one head in a branch.

Returns 0 on success, != 0 if an error occurred.
"""

import os
import argparse

from aslint.i18n import _
from aslint.logger import SetLevelAction, logger

from repository_api import hg


def main(reponame):
    """Main function."""
    if reponame == 'codeaster-xx':
        from hgaster.hooks import single_head_asterxx as hook
    else:
        from hgaster.hooks import single_head_per_branch as hook
    assert os.environ.get("HG_NODE"), _("'HG_NODE' should point the first new "
                                        "changeset")
    repo = hg.Repository(".")
    return hook(repo, os.environ["HG_NODE"])


if __name__ == '__main__':
    # command arguments parser
    parser = argparse.ArgumentParser(
        usage=__doc__,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-g', '--debug', action=SetLevelAction)

    parser.add_argument('-R', '--repository',
        help=_("use the checkings parameters for this repository"))

    args = parser.parse_args()
    logger.update_from_status(main(args.repository))
    logger.exit()
