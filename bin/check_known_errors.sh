#!/bin/bash

_usage()
{
    echo "Check for known errors in testcases execution."
    echo
    echo "Usage: $(basename $0) [options] RESUDIR [RESUDIR [...]]"
    echo
    echo "Options:"
    echo
    echo "  --help (-h)            Print this help information and exit."
    echo
    echo "  --verbose (-v)         Show more informations."
    echo
    echo "  --branch (-b) BRANCH   Branch name."
    echo
    exit "${1:-1}"
}

check_known_errors_main()
{
    local this=`readlink -n -f $0`
    local prefix=$(dirname $(dirname $this))

    local branch="default"
    local verbose=0
    local option
    OPTS=$(getopt -o hb:v --long help,branch:,verbose -n $(basename $0) -- "$@")
    if [ $? != 0 ] ; then
        echo "invalid arguments." ; _usage 2
    fi
    eval set -- "$OPTS"
    while true; do
        case "$1" in
            -h | --help ) _usage ;;
            -v | --verbose ) verbose=1 ;;
            -b | --branch ) branch="$2"; shift ;;
            -- ) shift; break ;;
            * ) break ;;
        esac
        shift
    done
    local resudir="${@}"
    local log=$(mktemp)

    local known_errors=${prefix}/lib/aslint/test/list_errors.${branch}

    echo -n "Checking if there are known errors for branch ${branch}... "

    (
        if [ ! -f ${known_errors} ]; then
            echo "Unsupported branch: ${branch}"
            echo "File not found: ${known_errors}"
            return 1
        fi

        # errors in results directories
        local errors=$(mktemp)
        for resd in ${resudir}
        do
            local resf=${resd}/Testing/Temporary/CTestCostData.txt
            if [ ! -f ${resf} ]; then
                echo "no such file: ${resf}"
                return 1
            fi
            grep -B 0 -A 100000 -- --- ${resf} | grep ASTER | sed -e 's/.*_//' >> ${errors}
        done
        sort -u ${errors} > ${errors}.sorted
        rm -f ${errors}

        # compare to known errors
        local new=$(mktemp)
        local ref=$(mktemp)
        egrep -v '^ *#' ${known_errors} | sort -u > ${ref}

        diff -ubB ${ref} ${errors}.sorted | grep -v ${errors}.sorted | egrep '^\+' > ${new}
        rm -f ${ref} ${errors}.sorted

        if [ `wc -l ${new} | awk '{print $1}'` != 0 ]; then
            echo "There are new errors:"
            cat ${new}
            return 1
        fi

        return 0
    ) >> ${log} 2>&1
    ret=$?

    test "${ret}" = "0" && ok=ok || ok=failed
    echo ${ok}

    if [ "${ok}" != "ok" ] || [ ${verbose} -eq 1 ]
    then
        printf "\nOutput+Error:\n"
        cat ${log}
    fi
    rm -f ${log}

    return ${ret}
}

check_known_errors_main "$@"
exit $?
