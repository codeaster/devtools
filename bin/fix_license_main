#!/usr/bin/env python3
# coding=utf-8

"""
    %prog [options] [filenames]

This script adds the license and copyright in source files.
MUST BE RUN from the code_aster/src repository.

Example:

    fix_license path/to/file1.F90 path/to/file2.py [...]
"""

import os.path as osp
import re
import time
from optparse import OptionParser

from aslint.utils import apply_in_sandbox, end_with_newline
from aslint.logger import logger, setlevel
from aslint.filetype import get_file_type
from aslint.baseutils import is_binary
from repository_api import is_repository

OLDGPL = """THIS PROGRAM IS FREE SOFTWARE; YOU CAN REDISTRIBUTE IT AND/OR MODIFY
IT UNDER THE TERMS OF THE GNU GENERAL PUBLIC LICENSE AS PUBLISHED BY
THE FREE SOFTWARE FOUNDATION; EITHER VERSION 2 OF THE LICENSE, OR
(AT YOUR OPTION) ANY LATER VERSION.
THIS PROGRAM IS DISTRIBUTED IN THE HOPE THAT IT WILL BE USEFUL, BUT
WITHOUT ANY WARRANTY; WITHOUT EVEN THE IMPLIED WARRANTY OF
MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. SEE THE GNU
GENERAL PUBLIC LICENSE FOR MORE DETAILS.
YOU SHOULD HAVE RECEIVED A COPY OF THE GNU GENERAL PUBLIC LICENSE
ALONG WITH THIS PROGRAM; IF NOT, WRITE TO EDF R&D CODE_ASTER,
1 AVENUE DU GENERAL DE GAULLE, 92141 CLAMART CEDEX, FRANCE.
"""

GPL = """This file is part of code_aster.

code_aster is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

code_aster is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with code_aster.  If not, see <http://www.gnu.org/licenses/>.
"""

longest = max([len(i) for i in GPL.splitlines()])

re_cedf = re.compile("^(.*COPYRIGHT.*[0-9]{4}.*)[0-9]{4}(.*EDF .*)$", re.M | re.I)
re_cpcase = re.compile("copyright", re.I)
year = time.strftime("%Y")

re_old = ".*".join([re.escape(i.strip()) for i in OLDGPL.splitlines()])
re_old_force = ".*".join([re.escape(i.strip()) for i in OLDGPL.split()])
re_new = ".*".join([re.escape(i.strip()) for i in GPL.splitlines()])

re_coding = re.compile("^#( *(?:|\-\*\- *|en)coding.*)\n", re.M)
add_coding = "coding=utf-8"
add_copy = "Copyright (C) 1991 - 2017  EDF R&D - www.code-aster.org\n"


def change_text(txt, typ="py"):
    """Call subfunctions to change 'txt'"""
    re_old_used = re_old
    endc = ""
    if typ in ("py", "catalo", "build", "test", "datg", "mater"):
        if typ == "datg" and "FINSF" in txt:
            return txt
        typ = "py"
        cmt = "#"
        if "* COPYRIGHT (C)" in txt:
            cmt = "*"
    elif typ in ("for", "hf"):
        cmt = "!"
    elif typ in ("c", "h", "cxx", "hxx", "mfront"):
        typ = "c"
        cmt = "/*"
        endc = "*/"
        re_old_used = re_old_force
    else:
        return txt

    fmt = "{{0}} {{1:{0}}} {{2}}".format(longest)

    def fline(line):
        line = line.strip().lstrip(cmt).rstrip(endc).strip()
        return fmt.format(cmt, line, endc).strip()

    lic_lines = GPL.splitlines()
    lic_lines = [fline(i) for i in lic_lines]
    lic = "\n".join(lic_lines + [""])
    separ = fline("-" * longest)

    # preliminary cleanup
    re_clean1 = re.compile("CLAMART CEDEX, FRANCE\. *\n({0} *\n)+".format(re.escape(cmt)), re.M)
    txt = re_clean1.sub("CLAMART CEDEX, FRANCE.\n", txt)

    re_sep = "(?:{0}[=\+\-\* ]+ *{1}\n)*"
    expr = re_sep + "{0} +" + re_old_used + " *{1}\n" + re_sep
    reg_old = re.compile(expr.format(re.escape(cmt), re.escape(endc)), re.I | re.M | re.DOTALL)

    expr = re_sep + "{0} +" + re_new + " *{1}\n" + re_sep
    reg_new = re.compile(expr.format(re.escape(cmt), re.escape(endc)), re.I | re.M | re.DOTALL)

    re_copy = re.compile("^({0} *COPYRIGHT.*)\n".format(re.escape(cmt)), re.M | re.I)
    copyright = re_copy.findall(txt)
    txt = re_coding.sub("", txt)
    txt = re_copy.sub("", txt)

    # if not reg_old.search(txt):
    #     without separators
    #     reg_old = re.compile(re_old_used, re.I | re.M | re.DOTALL)
    txt = reg_new.sub("", txt)
    txt = reg_old.sub("", txt)

    new = ""
    if typ == "py" and cmt == "#":
        new += fline(add_coding) + "\n"

    new += separ + "\n"
    if not copyright:
        copyright.append(fline(add_copy))
    for line in copyright:
        copy = re_cpcase.sub("Copyright", line)
        copy = re_cedf.sub("\g<1>{0} - EDF R&D - www.code-aster.org".format(year), copy)
        new += fline(copy) + "\n"

    txt = new.strip() + "\n" + lic + separ + "\n" + "\n" + txt.lstrip()

    return txt


def change_file(fname):
    """Change the content of a file."""
    with open(fname, "r") as fobj:
        txt = fobj.read()
    ftyp = get_file_type(fname)
    txt = change_text(txt, ftyp)
    with open(fname, "w") as fobj:
        fobj.write(end_with_newline(txt))


def check_ascii_binary(fname):
    """print ascii/binary detection"""
    if not osp.isfile(fname):
        logger.error("not a file: %s", fname)
        return
    typ = is_binary(fname) and "binary" or "ascii"
    logger.info("%s: %s", typ, fname)
    if typ == "ascii":
        change_file(fname)


def loop_on_files(*args):
    """Apply a function"""
    apply_in_sandbox(check_ascii_binary, args)


if __name__ == "__main__":
    # command arguments parser
    parser = OptionParser(usage=__doc__)
    parser.add_option(
        "-g", "--debug", action="callback", callback=setlevel, help="add debug informations"
    )
    opts, args = parser.parse_args()
    if len(args) == 0:
        parser.error("files ?")

    if not is_repository("."):
        parser.error("`fix_license` must be run from the code_aster/src " "repository.")
    loop_on_files(*args)
