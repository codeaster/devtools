#!/usr/bin/python3
# coding: utf-8

"""odj_eda [options]

Prepare the html files pour the next EDA.

- extract the list of the solved issues for all developers,
- build an html representation of the text of these issues,
- create a table of contents listing the developers,
- update the index of all EDA.

If the EDA does not take place on monday, use the `--date` option to store
the files under the right directory.
"""

import sys
import os
import os.path as osp
from time import localtime, strftime
import calendar
import shutil
from glob import glob
import re
from optparse import OptionParser

from api_roundup import build_histor, get_solved_issues_eda


def next_monday():
    """Return the date of the next monday"""
    now = localtime()
    num = int(strftime("%w", now))
    lundi = 1
    delta = 7 - num + 1
    if num <= lundi:
        delta = delta - 7
    time = calendar.timegm(now) + delta * 3600 * 24
    lun = localtime(time)
    date = strftime("%d/%m/%Y", lun)
    return date


class EDA(object):
    """Embeed the report generated for the eda"""

    header = """<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
 <html>
    <title>code_aster - %s</title>
    <style type="text/css">
       body {
          font-family: monospace;
          text-align: justify;
       }
       .titre {
          text-align: center;
          font-family: Arial;
          font-size: 150%%;
       }
       .soustitre {
          text-align: center;
          font-family: Arial;
          font-size: 100%%;
       }
       .note {
          text-align: left;
          font-style: italic;
          font-size: 100%%;
       }
       pre {
          font-family: monospace;
       }
       a:link {
          color: #000099;
          text-decoration: none;
       }
       a:visited {
          color: #999999;
          text-decoration: none;
       }
       a:hover {
          background-color: #ccccff;
          text-decoration: underline overline;
       }
       a.menu:link, a.menu:visited {
          background-color: #cadcf5;
          text-decoration: underline overline;
       }
       a.erreur:link, a.erreur:visited {
          color: white;
          text-decoration: underline overline;
       }
       a.alarme:link, a.alarme:visited {
          color: black;
          text-decoration: underline overline;
       }
       .erreur {
          background-color: #c14040;
          color: white;
       }
       .alarme {
          background-color: #ffeccb;
          color: black;
       }
       table {
          border-collapse: collapse;
          border: 1px solid #ffffff;
          color: #000000;
       }
       tr.entete {
          background-color: #cadcf5;
          color: #2d77be;
          margin: 50px;
          padding: 150px;
       }
       tr.ligne_paire {
          background-color: #e5e5e5;
       }
       tr.ligne_impaire {
          background-color: #ffffff;
       }
       td {
       }
    </style>
 <head>
 </head>
 <body>
 """
    footer = """</body>
 </html>
 """
    titre_sommaire = """
 <p class="titre">
 EDA - %s
 </p>
 """
    titre_index = """
 <p class="titre">
 Index des EDA
 </p>
 """
    head_sommaire = """<table align="center">\n"""
    foot_sommaire = "</table>\n"
    ligne_entete = """<tr class="entete">\n"""
    ligne = ['<tr class="ligne_paire">\n', '<tr class="ligne_impaire">\n']
    champ = "<td>&nbsp;%s&nbsp;</td>"
    notes_entete = """<br /><br />
 <hr width="20%%" align="left" />
 <p class="note">Notes :</p>
 <OL>\n"""
    notes_ligne = """<li class="note">%s\n"""
    notes_fin = "</OL>\n"
    fin_ligne = """</tr>\n"""
    url_gitlab_issues = "https://gitlab.com/groups/codeaster/-/issues"

    def __init__(self, base, date, add_external=False):
        """Initialisation"""
        self.cnx = None
        self.list_dev = None
        self.dict_issue = None
        self.add_gitlab = add_external
        self.base = base
        self.date = date
        if not self.date:
            self.date = next_monday()
        if re.search("[0-9]{2}/[0-9]{2}/[0-9]{4}", self.date) is None:
            print("Invalid date (expect format jj/mm/aaaa) : %s" % self.date)
            sys.exit(4)
        l_dat = self.date.split("/")
        l_dat.reverse()
        year = l_dat[0]
        rep = "eda" + "".join(l_dat)
        print("Next date : %s" % self.date)

        self.dest = osp.join(self.base, year, rep)
        self.fsom = osp.join(self.dest, "sommaire.html")
        self.flist = osp.join(self.dest, "liste_fiches")
        self.findex = osp.join(self.base, "index.html")
        self.l_dev = []
        # create destination direcory
        if osp.exists(self.dest):
            os.chdir(self.base)
            shutil.rmtree(self.dest)
        os.makedirs(self.dest)

    def generate_histor(self):
        """Generate an histor file for each developer"""
        if self.list_dev is None:
            self.dict_issue = get_solved_issues_eda()
            # ignore Europlexus
            self.dict_issue.pop("Europlexus", None)
            self.list_dev = list(self.dict_issue.keys())
            # write the list of issues
            text = []
            for resp, issues in list(self.dict_issue.items()):
                text.append("# {0}".format(resp))
                text.extend([str(i) for i in issues])
                text.append("")
            with open(self.flist, "w") as fobj:
                fobj.write(os.linesep.join(text))
        for dev in self.list_dev:
            text = build_histor(self.dict_issue[dev], "html")
            fhist = self._fich(dev, "histor.html")
            if not osp.isdir(self._fich(dev, "")):
                os.makedirs(self._fich(dev, ""))
            with open(fhist, "w") as fobj:
                fobj.write(text)
            print("write:", fhist)

    def _fich(self, dev, typ):
        """Retourne le nom du fichier"""
        return osp.abspath(osp.join(self.dest, dev, typ))

    def _lien(self, fich, texte=None, ref=None, attrs=""):
        """Retourne un lien vers `fich`.
        Si `ref` est présent, on fait un lien relatif par rapport à ce fichier.
        """
        fmt = """<a %s href="%s">%s</a>"""
        adr = fich
        if not adr.startswith("http"):
            adr = osp.abspath(fich)
        if ref is not None:
            ref = osp.abspath(osp.normpath(osp.dirname(ref)))
            dirref = ref.split(os.sep)
            diradr = adr.split(os.sep)
            i = 0
            while i < len(dirref) and i < len(diradr) and dirref[i] == diradr[i]:
                i += 1
            rest_ref = len(dirref) - i
            l_res = [os.pardir] * rest_ref + diradr[i:]
            adr = os.sep.join(l_res)
        if texte is None:
            texte = osp.basename(adr)
        return fmt % (attrs, adr, texte)

    def sommaire(self):
        """Produit un sommaire"""
        txt = [self.header % "Sommaire"]
        txt.append(
            self._lien(self.findex, texte="Retour au sommaire", ref=self.fsom, attrs='class="menu"')
        )
        txt.append("<br />")
        l_champs = ("login", "&nbsp;")
        txt.append(self.titre_sommaire % self.date)
        txt.extend(
            [
                '<p class="soustitre">',
                self._lien(self.flist, texte="liste des fiches", ref=self.fsom),
                "</p>",
            ]
        )
        txt.extend([self.head_sommaire, self.ligne_entete])
        txt.extend([self.champ % s for s in l_champs])
        txt.append(self.fin_ligne)

        i = 0
        for dev in self.list_dev:
            i += 1
            fhist = self._fich(dev, "histor.html")
            lienH = self._lien(fhist, "histor", ref=self.fsom)
            txt.append(self.ligne[i % 2])
            txt.extend([self.champ % s for s in (dev, lienH)])
            txt.append(self.fin_ligne)
        # add link to gitlab issues
        if self.add_gitlab:
            i += 1
            lienH = self._lien(self.url_gitlab_issues, "link")
            txt.append(self.ligne[i % 2])
            txt.extend([self.champ % s for s in ("gitlab.com", lienH)])
            txt.append(self.fin_ligne)
        txt.append(self.foot_sommaire)
        txt.append(self.footer)
        with open(self.fsom, "w") as fobj:
            fobj.write("".join(txt))
        print("table of contents:", self.fsom)

    def index(self):
        """Produit un index"""
        l_som = glob(osp.abspath(osp.join(self.base, "*", "*", "sommaire.html")))
        l_som.sort()
        l_som.reverse()
        txt = [self.header % "Index", self.titre_index, self.head_sommaire, self.ligne_entete]
        txt.append(self.champ % "date")
        txt.append(self.fin_ligne)

        i = 0
        for som in l_som:
            i += 1
            mat = re.search("/eda([0-9]{4})([0-9]{2})([0-9]{2})", som)
            if mat is None:
                print("<A> Nom de répertoire invalide : ", som)
                continue
            date = "%s/%s/%s" % (mat.group(3), mat.group(2), mat.group(1))
            lien_somm = self._lien(som, date, ref=self.findex)

            txt.append(self.ligne[i % 2])
            txt.append(self.champ % lien_somm)
            txt.append(self.fin_ligne)

        txt.append(self.foot_sommaire)

        heure_maj = "Derni&egrave;re mise &agrave; jour le %s." % strftime(
            "%d/%m/%Y &agrave; %H:%M:%S", localtime()
        )

        txt.append(self.notes_entete)
        txt.append(self.notes_ligne % heure_maj)
        txt.append(
            self.notes_ligne % "La mise à jour de cette page est "
            "effectu&eacute;e le lundi matin toutes les demi-heures "
            "avant l'EDA."
        )
        txt.append(
            self.notes_ligne % "Les utilisateurs ayant suffisamment "
            "de privil&egrave;ges peuvent lancer le script `odj_eda` "
            "depuis le compte dédié sur ce serveur."
        )
        txt.append(self.notes_fin)

        txt.append(self.footer)
        with open(self.findex, "w") as fobj:
            fobj.write("".join(txt))
        print("%6d eda dans l'index" % len(l_som))
        print("index:", self.findex)


if __name__ == "__main__":
    print("-" * 75)
    print(" Session started  %s" % strftime("%a %b %d %Y - %H:%M:%S"))

    # command arguments parser
    parser = OptionParser(usage=__doc__)
    parser.add_option(
        "--base",
        dest="base",
        action="store",
        help="directory where to write the files",
    )
    parser.add_option(
        "--date",
        dest="date",
        action="store",
        help="date of the next EDA (default %s)" % repr(next_monday()),
    )
    parser.add_option(
        "--extern",
        dest="add_external",
        action="store_true",
        help="add link to external bugtracker(default False)",
    )
    opts, args = parser.parse_args()
    if not opts.base:
        parser.error("'--base' option is mandatory")
    reunion = EDA(base=opts.base, date=opts.date)
    reunion.generate_histor()
    reunion.sommaire()
    reunion.index()

    print(" Session ended    %s" % strftime("%a %b %d %Y - %H:%M:%S"))
    print()
