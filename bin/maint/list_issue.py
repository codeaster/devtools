#!/usr/bin/env python3
# coding: utf-8

"""
    %prog [options] REV1 REV2

Print the list of issues solved between two revisions: ]REV1, REV2]
REV1 is excluded, REV2 is included.

"""


from optparse import OptionParser

from aslint.i18n import _
from aslint.logger import logger, setlevel
from aslint.string_utils import convert
from hgaster.ext_utils import get_issues_from_descr
from repository_api import get_rev_log


def get_log(rev1, rev2):
    """Extract the history between 2 revisions"""
    out = get_rev_log(".", "%s..%s" % (rev1, rev2), fmt="Revision %H\n%s\n")
    return convert(out)


if __name__ == "__main__":
    # command arguments parser
    parser = OptionParser(usage=__doc__)
    parser.add_option(
        "-g", "--debug", action="callback", callback=setlevel, help="add debug informations"
    )
    parser.add_option(
        "--rex",
        dest="filter",
        action="store_const",
        const="rex",
        help="return the issues solved in REX",
    )
    parser.add_option(
        "--bitbucket",
        dest="filter",
        action="store_const",
        const="bitbucket",
        help="return the issues solved in Bitbucket",
    )
    opts, args = parser.parse_args()
    if len(args) != 2:
        parser.error("exactly two arguments are required")
    text = get_log(*args)
    logger.debug(text)
    issues = get_issues_from_descr(text, filter=opts.filter)
    print("\n".join(map(str, issues)))
