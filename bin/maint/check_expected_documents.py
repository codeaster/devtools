#!/usr/bin/env python3
# coding: utf-8

"""
    %prog [options] filename_of_list_of_issues

Check that the expected documents have been committed.
"""

import sys
from optparse import OptionParser

from api_doc import changelog_doc
from api_roundup import get_connection, get_documents
from api_roundup.utils import read_issue_list
from aslint.i18n import _
from aslint.logger import logger, setlevel
from hgaster.ext_utils import get_issues_from_descr


def check_expected_documents(ids):
    """Check the status of some issues.

    Arguments:
        ids (list[int]): Identifiers of the issues.

    Returns:
        bool: *True* if all documents have been committed, *False* otherwise.
    """
    ok = True
    with get_connection() as rex:
        for num in set(ids):
            docs = get_documents(str(num), rex)
            if not docs:
                logger.info(_("no changes needed for #{0}").format(num))
            for key in docs:
                try:
                    descr = " ".join(changelog_doc(key))
                except ValueError:
                    descr = ""
                logger.debug("changelog: " + descr)
                done = get_issues_from_descr(descr, bracket=False)
                msg = _("issue #{0} needs changes in {1}: {2}")
                if num in done:
                    logger.info(msg.format(num, key, _("found")))
                else:
                    ok = False
                    logger.warning(
                        msg.format(num, key, _("issue number not found in document history"))
                    )
    return ok


if __name__ == "__main__":
    # command arguments parser
    parser = OptionParser(usage=__doc__)
    parser.add_option(
        "-g", "--debug", action="callback", callback=setlevel, help="add debug informations"
    )
    opts, args = parser.parse_args()
    if len(args) != 1:
        parser.error("exactly one argument is required")

    ok = check_expected_documents(read_issue_list(args[0]))
    sys.exit(0 if ok else 1)
