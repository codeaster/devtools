#!/usr/bin/env python3
# coding: utf-8

"""
    %(prog)s [options] [REV]

Set of commands to manage the workflow of a request for integration

This command is dedicated to the administrators that have to integrate a
candidate revision. It offers several elementary actions (see options).

Note:
    REV is compulsory for --start, --differ, --test, --close, --refuse
    and --cancel, it is the revision proposed for integration.
    --close and --refuse should only be called by script after the testcases
    run.

The workflow is (actions are between [])::

                [check]                                                        .
        [start]   /\                                                           .
       ------->   \/   [test]         [close]                                  .
 PEND  <-------  RUN  ------->  TEST  ------> closed, removed from the queue
    \   [differ]     \               \                                         .
     \                \ [cancel]      \ [refuse]                               .
      \ [cancel]       \               \                                       .
       `-> removed      `-> removed     `-> removed                            .
"""

import argparse
import sys

from aslint.i18n import _
from aslint.logger import SetLevelAction, logger
from hgaster.ascommands import Integration
from hgaster.ext_utils import newui
from repository_api import hg, repository_type

if sys.version_info < (3, 5):
    raise ValueError("This script requires Python >=3.5")


def main(path, opts):
    """Main function."""
    assert repository_type(path) == "hg", "only supported for Mercurial repositories"
    repo = hg.Repository(path)
    return Integration(newui(), repo, *opts.intrev, **vars(opts)).run()


if __name__ == "__main__":
    # command arguments parser
    parser = argparse.ArgumentParser(
        usage=__doc__, formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument("-g", "--debug", action=SetLevelAction)

    parser.add_argument(
        "-n",
        "--dry-run",
        action="store_true",
        default=False,
        help=_("does nothing, just show what it would be done"),
    )
    parser.add_argument(
        "--local",
        action="store_true",
        default=False,
        help=_(
            "do not make remote connections, updates will not be "
            "downloaded, no request for integration will be submitted"
        ),
    )
    parser.add_argument(
        "-r",
        "--rev",
        metavar=_("REFREV"),
        help=_(
            "last ancestor revision already integrated. "
            "For submit, only use if --local "
            "is enabled because it can not be retreived from the remote "
            "repository"
        ),
    )

    parser.add_argument(
        "-m", "--message", metavar=_("TEXT"), help=_("message sent to the developer")
    )

    parser.add_argument(
        "--start",
        action="store_true",
        help=_(
            "Initiate the integration of the revision REV and notify the "
            "developer that an integrator is working on its proposal. "
            "The status of the request is changed to RUN."
        ),
    )
    parser.add_argument(
        "--startall", action="store_true", help=_("Apply start on all pending requests.")
    )
    parser.add_argument(
        "--check",
        action="store_true",
        help=_("Run the different checkings similar to the ones run at " "the submission."),
    )
    parser.add_argument(
        "--test",
        action="store_true",
        help=_(
            "Mark the parent revision to be tested. If the changesets have "
            "been rearranged, the parent revision is different of REV, in "
            "this case, it is mentionned in the commit message. The status "
            "of the request is changed to TEST (needs pass testcases). "
            "The developer is notified."
        ),
    )
    parser.add_argument(
        "--testall", action="store_true", help=_("Apply test on all requests in the RUN status.")
    )
    parser.add_argument(
        "--close",
        action="store_true",
        help=_(
            "The request for integration is closed because testcases passed "
            "with success. Called automatically when testcases are "
            "finished. The developer is notified of the closure."
        ),
    )
    parser.add_argument(
        "--closeall", action="store_true", help=_("Apply close on all requests in the TEST status.")
    )
    parser.add_argument(
        "--refuse",
        action="store_true",
        help=_(
            "The request for integration is closed because testcases "
            "failed. Called automatically when testcases are finished. "
            "The developer is notified of the closure."
        ),
    )
    parser.add_argument(
        "--differ",
        action="store_true",
        help=_(
            "Differ the integration of the revision REV and notify the "
            "developer that its proposal will be analyzed later. "
            "The status of the request is reset to PEND."
        ),
    )
    parser.add_argument(
        "--cancel",
        action="store_true",
        help=_(
            "The request for integration is cancelled, the revision won't "
            "be integrated until a new request will be submitted. "
            "The developer is notified of the removal."
        ),
    )
    parser.add_argument(
        "--status",
        action="store_true",
        help=_(
            "Return the current status of the request for integration of a "
            "revision or of all registered requests."
        ),
    )

    parser.add_argument(
        "intrev", metavar="REV", nargs="*", help=_("Identifier of the proposed revision.")
    )

    args = parser.parse_args()

    logger.update_from_status(main(".", args))
    logger.exit()
