#!/usr/bin/env python3
# coding: utf-8
"""
    %prog [options] HIST

Send the HISTOR file to the developers

"""

import os
import os.path as osp
import re
import tempfile
from optparse import OptionParser
from subprocess import call

from aslint.config import ASCFG
from aslint.i18n import _
from aslint.logger import logger, setlevel
from hgaster.ext_utils import sendmail

MSG = """
<p>
Publication de l'histor de la version {tag}.
</p>
<p>
Lien :
<a href="https://gitlab.pleiade.edf.fr/codeaster/changelog/-/blob/main/histor/v{major}/{tag}">
https://gitlab.pleiade.edf.fr/codeaster/changelog/-/blob/main/histor/v[{major}/{tag}
</a>
</p>
"""


def get_subject_from(fname, subject=None):
    """Return the subject by reading the header of the histor file"""
    with open(fname, "r") as fobj:
        text = fobj.read()
    if subject is None:
        retit = re.compile("^ *Version *(?:[0-9]+\.){1,2}[0-9]+.*du .*", re.M)
        mat = retit.findall(text)
        assert len(mat) > 0, "not found"
        subject = mat[0]
    return subject, text


def read_input(prompt):
    """Read text on stdin"""
    print(prompt, end=" ")
    lines = []
    try:
        while True:
            lines.append(input())
    except EOFError:
        pass
    finally:
        text = os.linesep.join(lines)
    return text


if __name__ == "__main__":
    # command arguments parser
    parser = OptionParser(usage=__doc__)
    parser.add_option(
        "-g",
        "--debug",
        action="callback",
        callback=setlevel,
        help="add debug informations",
    )
    parser.add_option(
        "-s",
        "--subject",
        action="store",
        help="subject of email, else automatically read from the histor file",
    )
    parser.add_option(
        "--to",
        action="store",
        help="destination address (default: aster-devel mailing list)",
    )
    parser.add_option("--attachment", action="append", help="attached file")
    parser.add_option("-n", "--dry-run", action="store_true", help="do not send the email")
    parser.add_option(
        "--no-teams",
        dest="teams",
        action="store_false",
        default=True,
        help="do not send to Teams channel",
    )
    opts, args = parser.parse_args()
    if len(args) > 1:
        parser.error("at most one argument is required")
    if len(args) == 1:
        subject, text = get_subject_from(args[0], subject=opts.subject)
    else:
        subject = opts.subject or input("Subject: ")
        text = read_input("Body (type <Ctrl+D> to exit):\n")
    logger.info(_("Email subject: %s"), subject)

    dest = opts.to or "devel"
    sendmail(
        dest,
        subject,
        text,
        fromaddr=ASCFG.get("notify.codeaster"),
        bcc=True,
        footer=False,
        attach=opts.attachment,
        dry_run=opts.dry_run,
    )

    tag = osp.basename(args[0])
    msg = MSG.format(tag=tag, major=tag.split(".")[0])
    if opts.teams:
        if not opts.dry_run:
            with tempfile.NamedTemporaryFile(mode="w", delete=False) as ftmp:
                ftmp.write(msg)
                tmpname = ftmp.name
            cmd = [
                "send_msteams",
                "--from",
                "no-reply@edf.fr",
                "--subject",
                subject,
                "--html",
                tmpname,
            ]
            call(cmd)
        else:
            logger.info(_("Teams message: {0}").format(msg))
