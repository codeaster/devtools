#!/usr/bin/env python3
# coding: utf-8

"""
    %prog [options] [FILE]|[list-of-issues-numbers]

Make the difference between the expected issues (in 'valide_EDA' status)
and those already integrated (given in a file).

"""


import os.path as osp
from optparse import OptionParser

from api_roundup import get_expected_issues
from aslint.i18n import _
from aslint.logger import logger, setlevel
from create_histor import read_issue_list


def make_list_expected(args, branch, product=None):
    """Make the list of expected issues minus those already integrated"""
    integ = []
    for arg in args:
        if not osp.isfile(arg):
            integ.append(int(arg))
        else:
            integ.extend(read_issue_list(arg))
    dble = [i for i in integ if integ.count(i) > 1]
    if dble:
        dble = list(set(dble))
        dble.sort()
        logger.warn(_("seen more than once: %s"), dble)
    integ = list(set(integ))
    integ.sort()
    issues = get_expected_issues(branch, product)
    valid = list(set(issues.keys()))
    valid.sort()
    logger.debug("integrated issues: %s", integ)
    logger.debug("validated issues: %s", valid)
    logger.info(_("%6d integrated issues"), len(integ))
    logger.info(_("%6d validated issues"), len(valid))
    not_valid = list(set(integ).difference(valid))
    not_valid.sort()
    if not_valid:
        logger.warn(_("integrated but not validated: %s"), not_valid)
        logger.warn(_("%6d integrated issues but not validated"), len(not_valid))
    not_integ = list(set(valid).difference(integ))
    not_integ.sort()
    return issues, not_integ


def print_expected(issues, not_integ, product=None):
    """Format the list of expected issues"""
    logger.info(_("expected issues: %s"), not_integ)
    logger.info(_("%6d expected issues"), len(not_integ))
    fmt = (
        "%(id)6d %(assignedto)-10s "
        "%(corrVdev)s %(verCorrVdev)s %(corrVexpl)s %(verCorrVexpl)s "
        "%(produit)s: %(title)s"
    )
    for num in not_integ:
        if product and issues[num]["produit"] not in product:
            continue
        print(fmt % issues[num])


if __name__ == "__main__":
    # command arguments parser
    parser = OptionParser(usage=__doc__)
    parser.add_option(
        "-g", "--debug", action="callback", callback=setlevel, help="add debug informations"
    )
    parser.add_option("-b", "--branch", action="store", default="default", help="branch name")
    parser.add_option(
        "--product", action="append", default=None, help="limit to one or more products"
    )
    opts, args = parser.parse_args()
    issues, not_integ = make_list_expected(args, opts.branch, opts.product)
    if not_integ:
        print_expected(issues, not_integ, opts.product)
