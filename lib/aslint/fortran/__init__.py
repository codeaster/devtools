# coding=utf-8

"""This package gives convenient functions to check fortran source files.

Some functions are fortran77 specific and must be improved to check
future fortran90 source files.

For examples of uses, see `examples/*.py`.
"""
