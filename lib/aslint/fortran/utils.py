# coding=utf-8

"""Utilities for fortran checkings"""

import os
import os.path as osp
from glob import glob
import pickle


def build_call_tree(dsrc):
    """Build the tree of subroutines called by each one and the reverse one."""
    lcalls = set()
    for dbib in [osp.join(dsrc, subd) for subd in ('bibfor', 'bibf90')]:
        for info in os.walk(dbib):
            lcalls.update(glob(osp.join(info[0], '*.calls')))
    tree = {}
    rev_tree = {}
    for fname in lcalls:
        with open(fname, 'r') as fobj:
            called = fobj.read().split()
        sub = osp.basename(osp.splitext(fname)[0])
        tree[sub] = called
        for subc in called:
            rev_tree[subc] = rev_tree.get(subc, []) + [sub, ]
    with open('call_tree.pick', 'wb') as pick:
        pickle.dump(tree, pick)
        pickle.dump(rev_tree, pick)
    return tree, rev_tree


def read_call_tree():
    """Unpickle the call trees"""
    with open('call_tree.pick', 'rb') as pick:
        tree = pickle.load(pick)
        rev_tree = pickle.load(pick)
    return tree, rev_tree


def sub2filename(subprog, depth=2):
    """Search the filename of the given subprogram"""
    sdir = ('bibfor', 'bibf90')
    suffixes = ('.f', '.F', '.f90', '.F90')
    for dirn in sdir:
        for suf in suffixes:
            for level in range(depth):
                path = [dirn] + ['*'] * level + [subprog + suf]
                found = glob(osp.join(*path))
                if found:
                    return found[0]
    return None
