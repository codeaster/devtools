# coding: utf-8

externals = [
    # system
    'cpu_time', 'system_clock', 'get_environment_variable',
    # Compilers
    'tracebackqq', 'backtrace',
    # Intrinsics
    'abort', 'random_number',
    # standard modules
    'c_f_pointer',
]

# PETSc - not found in modules?!
externals.extend([
    # 'build_lmp_context', 'free_augm_lagrangian_context',
    # 'free_elim_lagr_context', 'free_lmp_context', 'free_saddle_point_context',
    'ISCreateGeneral', 'ISCreateStride', 'ISDestroy', 'ISGetIndices', 'ISGetSize',
    'ISRestoreIndices', 'ISSort', 'KSPComputeRitz', 'KSPCreate', 'KSPDestroy',
    'KSPGetConvergedReason', 'KSPGetIterationNumber', 'KSPGetOperators',
    'KSPGetPC', 'KSPGetSolution', 'KSPGetTolerances', 'KSPMonitorSet',
    'KSPSetComputeRitz', 'KSPSetOperators', 'KSPSetPCSide', 'KSPSetTolerances',
    'KSPSetType', 'KSPSetUp', 'KSPSolve', 'KSPView', 'MatAssemblyBegin', 'MatAssemblyEnd',
    'MatAYPX', 'MatConvert', 'MatCreate', 'MatCreateSeqAIJ', 'MatCreateShell',
    'MatCreateVecs', 'MatDestroy', 'MatDuplicate', 'MatGetColumnNorms',
    'MatGetLocalSize', 'MatGetOwnershipRange', 'MatGetRow', 'MatGetSize',
    'MatGetSubMatrix', 'MatGetVecs', 'MatMatMult', 'MatMatMultTranspose',
    'MatMPIAIJSetPreallocation', 'MatMult', 'MatMultTranspose',
    'MatMultTransposeAdd', 'MatMumpsSetCntl', 'MatMumpsSetIcntl',
    'MatNullSpaceCreateRigidBody', 'MatNullSpaceDestroy', 'MatPtAP',
    'MatRestoreRow', 'MatSEQAIJSetPreallocation', 'MatSetBlockSize',
    'MatSetNearNullSpace', 'MatSetOption', 'MatSetSizes', 'MatSetType',
    'MatSetValue', 'MatSetValues', 'MatShellSetOperation', 'MatTranspose',
    'MatTransposeMatMult', 'MatZeroRows', 'PCApply', 'PCApplySymmetricLeft',
    'PCApplySymmetricRight', 'PCBJacobiGetSubKSP', 'PCCreate', 'PCDestroy',
    'PCFactorGetMatrix', 'PCFactorSetFill', 'PCFactorSetLevels',
    'PCFactorSetMatOrderingType', 'PCFactorSetMatSolverPackage',
    'PCFactorSetUpMatSolverPackage', 'PCGAMGSetNSmooths',
    'PCSetFromOptions', 'PCSetOperators', 'PCSetType', 'PCSetUp',
    'PCShellSetApply', 'PCShellSetApplySymmetricLeft',
    'PCShellSetApplySymmetricRight', 'PCShellSetContext', 'PCShellSetDestroy',
    'PCShellSetName', 'PCShellSetSetUp', 'PetscFinalize', 'PetscInitialize',
    'PetscInitializeFortran', 'PetscOptionsSetValue',
    'PetscViewerAndFormatCreate', 'VecAssemblyBegin', 'VecAssemblyEnd',
    'VecAXPY', 'VecAYPX', 'VecCopy', 'VecCreate', 'VecCreateMPI',
    'VecCreateSeq', 'VecDestroy', 'VecDot', 'VecDuplicate',
    'VecDuplicateVecs', 'VecGetArray', 'VecGetOwnershipRange',
    'VecGetOwnerShipRange', 'VecGetSize', 'VecGetSubVector', 'VecMAXPY',
    'VecNorm', 'VecRestoreArray', 'VecScale', 'VecScatterBegin',
    'VecScatterCreate', 'VecScatterCreateToAll', 'VecScatterDestroy',
    'VecScatterEnd', 'VecSet', 'VecSetBlockSize', 'VecSetSizes',
    'VecSetType', 'VecSetValues'
])
