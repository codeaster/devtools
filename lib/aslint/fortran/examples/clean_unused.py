#!/usr/bin/env python3
# coding=utf-8

"""
Example script using the aslint/fortran functions.
"""

import os.path as osp
from optparse import OptionParser
from functools import partial

from aslint.config import ASCFG
from aslint.utils import apply_func_text, apply_in_parallel
from aslint.logger import logger, setlevel
from aslint.check_files import read_waf_parameters
from aslint.fortran.check_source import list_unused
from aslint.fortran.free.change import remove_vars, remove_tabs, remove_labels


def clean_source(txt, uvars, upars, ulabs):
    """Define the successive steps to clean the source text."""
    if uvars:
        txt = remove_vars(txt, uvars)
    # if upars:
        # txt = remove_params(txt, upars)
    if ulabs:
        txt = remove_labels(txt, ulabs)
    txt = remove_tabs(txt, 8)
    return txt


def clean_unused(fname, incdir, flags):
    """Check warnings and remove unused variables and labels.
    """
    ignvars = ['i4zon', 'lszon', 'r8zon']
    unused = list_unused(fname, incdir, flags)
    logger.debug(unused)
    uvars = list(set(unused['vars']).difference(ignvars))
    upars = list(set(unused['params']).difference(ignvars))
    ulabs = unused['labels']
    # define the 'atomic' function: func(txt) => txt
    func = partial(clean_source, uvars=uvars, upars=upars, ulabs=ulabs)
    try:
        apply_func_text(func, fname, backup_ext='.orig')
    except AssertionError as exc:
        logger.error("Error with '%s': %s", fname, exc)


if __name__ == '__main__':
    # command arguments parser
    parser = OptionParser(usage=__doc__)
    parser.add_option('-g', '--debug', action='callback', callback=setlevel,
                      help="add debug informations")
    parser.add_option('--build', action='store', default=None,
                      metavar='DIR',
                      help="waf build directory containing configuration parameters")
    parser.add_option('--flag', action='append',
                      help="options passed to the compiler")
    opts, args = parser.parse_args()
    if len(args) == 0:
        parser.error('files ?')
    builddir = opts.build or ASCFG.get('waf.builddir')
    flags = opts.flag or []
    wafc4che = osp.join(builddir, 'c4che', 'release_cache.py')
    c4che = read_waf_parameters(wafc4che)
    fclean = partial(clean_unused,
                     incdir=c4che['INCLUDES'],
                     flags=flags)
    apply_in_parallel(fclean, args, 1)
