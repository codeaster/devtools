#!/usr/bin/env python3
# coding=utf-8

"""
Insert INCLUDE 'jeveux_private.h'.
"""

import re
from optparse import OptionParser

from aslint.logger import logger, setlevel
from aslint.utils import apply_func_text_on_files, remove_lines
from aslint.fortran.free.change import (
    remove_vars, remove_tabs, remove_common, add_statement
)

# declarations to remove
RM_VARS = [
    'IACCE', 'LTYP', 'LONG', 'DATE', 'IADD', 'IADM', 'LONO', 'HCOD',
    'CARA', 'LUTI', 'IMARQ', 'GENR', 'TYPE', 'DOCU', 'ORIG', 'RNOM', 'INDIR',
    'IUSADI',
    'ISZON', 'K1ZON', 'R8ZON', 'LSZON', 'I4ZON',
    'ISZON(1)', 'K1ZON(1)', 'K1ZON(8)', 'R8ZON(1)', 'LSZON(1)', 'I4ZON(1)',
]

# commons decls to remove
RM_COMMON = ['IACCED', 'IATRJE', 'KATRJE', 'KINDIR', 'KUSADI', 'KZONJE']

# equivalence between the last variables to remove


def is_equivalence_zon(txt):
    """Tell if 'txt' matches an EQUIVALENCE to remove"""
    return re.search('EQUIVALENCE .*ZON', txt, re.M) is not None


def clean_source(txt):
    """Define the successive steps to clean the source text."""
    txt = remove_vars(txt, RM_VARS)
    txt = remove_common(txt, RM_COMMON)
    txt = remove_lines(txt, is_equivalence_zon)
    txt = remove_tabs(txt)
    txt = add_statement(txt, addline="      INCLUDE 'jeveux_private.h'")
    return txt


def remove_old_decls(*args):
    """Remove 'old' declarations (those replaced by the include)"""
    apply_func_text_on_files(clean_source, args)


if __name__ == '__main__':
    # command arguments parser
    parser = OptionParser(usage=__doc__)
    parser.add_option('-g', '--debug', action='callback', callback=setlevel,
                      help="add debug informations")
    opts, l_args = parser.parse_args()
    if len(l_args) == 0:
        parser.error('files ?')
    remove_old_decls(*l_args)
