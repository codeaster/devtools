# coding=utf-8

"""Check source files"""

import os
import os.path as osp
import re
import tempfile
from functools import partial
from glob import glob
from subprocess import PIPE, STDOUT, Popen

import aslint.common_checkers as COMM
import aslint.fortran.gfortran_checkers as GFORT
import aslint.fortran.static_fortran_checkers as STAT
from aslint.base_checkers import (
    CheckList,
    CompilMsg,
    GenericMsg,
    MsgList,
    Report,
    call_checkers,
    check_disabled,
    check_file_content,
    check_filename,
    check_fortran_code,
    checkcontext,
)
from aslint.config import ASCFG
from aslint.decorators import interrupt_decorator
from aslint.dependency import Node, resolve_dependency
from aslint.fortran.free.fortran_code import FortranParserError
from aslint.i18n import _
from aslint.logger import logger
from aslint.string_utils import convert
from aslint.utils import apply_in_sandbox


class ParserMsg(GenericMsg):
    """Fortran parser error
    There is probably an error reported by the compiler. Otherwise, please
    report this error."""

    id = "C1011"

    def search(self, txt):
        """Returns the error message"""
        return [": " + txt]


@interrupt_decorator
def check_fortran_source(fname, flags, incdir, checklist, tmpdir=None):
    """Check a fortran source file."""
    logger.info(fname)
    if tmpdir:
        fobj = osp.join(tmpdir, osp.splitext(osp.basename(fname))[0] + ".o")
    else:
        fobj = None
    msgerr = build_source(fname, incdir, flags, objdest=fobj)
    lmsg = MsgList()
    try:
        msgsplit = GFORT.msg_splitter(osp.abspath(fname), msgerr, extern=False)
        for msg in msgsplit:
            lmsg.extend(call_checkers(msg, checklist.on_compiloutput(), typm=CompilMsg, stopon="0"))
        lmsg.extend(check_file_content(fname, checklist.on_content()))
        lmsg.extend(check_fortran_code(fname, checklist.on_fortran_code()))
        lmsg.extend(check_filename(fname, checklist.on_filename()))
    except FortranParserError as exc:
        checker = ParserMsg(None)
        checker.check(lmsg, exc.msg)
    except Exception as exc:
        logger.error(_("cannot check %r"), fname)
        raise RuntimeError(exc)
    check_disabled(fname, checklist, lmsg)
    report = Report()
    report.set(fname, lmsg)
    return report


def check_fortran_loop(args, numthread, flags, incdir, compiler):
    """Check all the files in 'args'. Return a report of the errors."""
    checklist = CheckList()
    checklist.register(GFORT.CHECK_LIST)
    checklist.register(STAT.CHECK_LIST)
    if ASCFG.get("fortran.form") == "fixed":
        checklist.register([STAT.FixedFormDisable])
    checklist.register(COMM.CHECK_LIST)
    ctg = checkcontext
    use_cached_module = ctg.is_enabled(ctg.UseCachedModule) and compiler == "gfortran"
    report = Report()
    if args:
        # the global directory is given by the first argument to check
        dname = osp.dirname(osp.abspath(args[0])).split(os.sep)
        try:
            idx = dname.index("bibfor")
            path = os.sep.join(dname[:idx])
            if not use_cached_module:
                # prepare module files (only in bibfor)
                build_module(path, incdir=incdir, flags=flags)
            else:
                logger.info(_("modules have been built with gfortran, do not rebuild"))
            logger.info(_("add checkings of the source directory"))
            lmsg = check_filename(path, checklist.on_dirname())
            report.set(path, lmsg)
        except ValueError:
            # bibfor not found
            logger.warn(_("'bibfor' not found in path, duplicated filenames " "not checked"))
    func_one = partial(check_fortran_source, flags=flags, incdir=incdir, checklist=checklist)
    lrep = apply_in_sandbox(func_one, args, numthread, pass_tmpdir=True)
    report.merge(lrep)
    ASCFG.clean("fortran.moddest")
    if report.hasError("E9991", "Can't open module file"):
        logger.error(
            _("Module files missed. code_aster must be built with 'release' mode (aka 'nodebug').")
        )
    return report


def check_header_file(fname, checklist):
    """Check a header file"""
    logger.info(fname)
    report = Report()
    lmsg = check_file_content(fname, checklist.on_content())
    check_disabled(fname, checklist, lmsg)
    report.set(fname, lmsg)
    return report


def check_header_loop(args, numthread):
    """Check all the files in 'args'. Return a report of the errors."""
    checklist = CheckList()
    checklist.register(STAT.CHECK_LIST)
    checklist.register([STAT.ForHeaderDisable])
    checklist.register(COMM.CHECK_LIST)
    func_one = partial(check_header_file, checklist=checklist)
    lrep = apply_in_sandbox(func_one, args, numthread)
    report = Report()
    report.merge(lrep)
    return report


def build_module(path, incdir, flags=None):
    """Build the modules before checking the source files
    because .mod files are compiler dependent."""
    # dependencies: modules name must match '*_module.F90'
    deps = glob("bibfor/*/*_module.F90")
    deps.extend(glob("bibfor/*/*_type.F90"))
    # for compatibility
    deps.extend(glob("bibfor/*/module_*.F90"))
    logger.info(_("preparing module files"))
    order = module_order(deps)
    done = set()
    for modname in deps:
        _build_module_deps(modname, order, incdir, done, flags)


def _build_module_deps(modname, dependencies, incdir, done, flags):
    """Build a module by starting with its dependencies."""
    if modname in done:
        return
    moddest = ASCFG.get("fortran.moddest")
    done.add(modname)
    for fname in dependencies[modname]:
        _build_module_deps(fname, dependencies, incdir, done, flags)
    logger.info(modname)
    fobj = osp.join(moddest, osp.splitext(osp.basename(modname))[0] + ".o")
    build_source(modname, incdir, flags, objdest=fobj, moddest=moddest)


def module_order(deps):
    """Dependency resolution order"""
    # same expression as in waflib.Tools.fc_scan
    use_regex = r"(?:^|;)\s*" r"USE(?:\s+|(?:(?:\s*,\s*(?:NON_)?INTRINSIC)?\s*::))" r"\s*(\w+)"
    re_use = re.compile(use_regex, re.I)
    nodes = {}
    files = {}
    for fname in deps:
        mod = osp.splitext(osp.basename(fname))[0]
        nodes[fname] = Node(mod)
        files[mod] = fname
    # resolve order for each module
    order = {}
    for fname in deps:
        with open(fname, "r") as fobj:
            txt = fobj.read()
        for line in txt.splitlines():
            mat = re_use.search(line)
            if mat:
                mod = mat.group(1)
                other = files.get(mod)
                if not other:
                    logger.info(_("external module: {0}").format(mod))
                else:
                    other_node = nodes[other]
                    nodes[fname].add_dependency(other_node)
        order[fname] = [files[mod.name] for mod in resolve_dependency(nodes[fname])]
    return order


def build_source(fname, incdir, flags=None, objdest=None, moddest=None):
    """Build a source file"""
    objdest = objdest or tempfile.NamedTemporaryFile().name
    moddest = moddest or osp.dirname(objdest)
    if type(incdir) not in (list, tuple):
        incdir = [
            incdir,
        ]
    incdir.insert(0, ASCFG.get("fortran.moddest"))
    all_flags = []
    for inc in incdir:
        flag = "-I" + osp.abspath(inc)
        if flag in all_flags:
            continue
        all_flags.append(flag)
    all_flags.extend(list(flags or []))
    cmd = GFORT.check_command_line(
        {
            "source": osp.abspath(fname),
            "object": osp.abspath(objdest),
            "flags": all_flags,
        }
    )
    logger.debug(cmd)
    saved = os.environ.get("LANGUAGE"), os.environ.get("LC_ALL")
    os.environ["LANGUAGE"] = "en"
    os.environ["LC_ALL"] = "C"
    prev = os.getcwd()
    try:
        os.chdir(moddest)
        prc = Popen(cmd, stdout=PIPE, stderr=STDOUT, close_fds=True)
        msgerr = convert(prc.communicate()[0])
    finally:
        os.chdir(prev)
        for key, value in zip(("LANGUAGE", "LC_ALL"), saved):
            if value:
                os.environ[key] = value
            else:
                del os.environ[key]
    logger.debug(msgerr)
    return msgerr


# special checkings


def list_unused(fname, incdir, flags=None):
    """Return the list of unused variables, parameters, labels, dummy args."""
    messages = {
        "vars": GFORT.UnusedVar,
        "params": GFORT.UnusedParam,
        "labels": GFORT.UnusedLabel,
        "dummy": GFORT.DummyArgument,
    }
    checklist = CheckList(list(messages.values()))
    flags = flags or []
    report = check_fortran_source(fname, flags, incdir, checklist)
    rfn = report[fname]
    unused = {}
    for key, klass in list(messages.items()):
        unused[key] = unused.get(key, []) + [
            msg.result["elt"] for msg in rfn if isinstance(msg, klass)
        ]
    ASCFG.clean("fortran.moddest")
    return unused


def list_uninit(fname, incdir, flags=None):
    """Return the list of uninitialized variables in fname"""
    messages = [GFORT.UninitializedVariable, GFORT.MaybeUninitializedVariable]
    checklist = CheckList(messages)
    flags = flags or []
    report = check_fortran_source(fname, flags, incdir, checklist)
    rfn = report[fname]
    uninit = []
    for klass in messages:
        uninit.extend([msg.result["arg"] for msg in rfn if isinstance(msg, klass)])
    ASCFG.clean("fortran.moddest")
    return uninit
