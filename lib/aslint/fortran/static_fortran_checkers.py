# coding=utf-8

# because of the 'search' method :
# pylint: disable=W0223

"""Check the text of a fortran source file"""
# TODO: import regexp as RX...

import os
import os.path as osp
import re
from glob import glob
from threading import Lock

import aslint.common_checkers as COMM
import aslint.fortran.free.fortran_code as FC
import aslint.fortran.free.fortran_utilities as FUT
import aslint.fortran.free.regexp as RX
from aslint.base_checkers import (
    DiffCat,
    DirnameCat,
    FileContentCat,
    FilenameCat,
    FortranCodeCat,
    GenericMsg,
    HiddenMsg,
    TextMsg,
    TextMsgNotFound,
    checkers_from_context,
    search_msg,
)
from aslint.config import ASCFG
from aslint.fortran.externals import externals

COMMENT_FOR = re.compile("^[!].*?$", re.M)
STATS = "(call|write|read|do|while|if)"
UNAUTH_STATS = "(stop *$|intrinsic|entry|dimension|equivalence" "|include|external)"
AVOID_STATS = "(call +utalrm)"

VARLENGTH = 24
SUBLENGTH = 32

# syntax


class ImplicitNone(FileContentCat, TextMsgNotFound):
    """implicit none"""

    id = "C1001"
    search = search_msg("^ *implicit +none")


class UnauthorizedStatement(FileContentCat, TextMsg):
    """Unauthorized statement
    These statements should not be used: stop, intrinsic, entry,
    dimension, equivalence, include.
    Prefer use '#include' instead of the 'include' fortran statement.
    """

    id = "C1002"
    search = search_msg("^ *(?P<main>%s)" % UNAUTH_STATS)


class UnrecommendedStatement(FileContentCat, TextMsg):
    """Unrecommended statement
    These statements should not be used: utalrm, allocate, deallocate.
    ALLOCATE/DEALLOCATE should be replaced by AS_ALLOCATE/AS_DEALLOCATE.
    """

    id = "W1003"
    search = search_msg("^ *(?P<main>%s.*?)$" % AVOID_STATS)


class LicenseNotFound(FileContentCat, COMM.LicenseNotFound):
    """Summary with GPL is required
    The copyright and GPL summary are required. Even if there is an external
    copyright, it must mention www.code-aster.org
    See `fix_license` tool (use --help option for details)."""

    id = "C1004"


class EDFCopyright(FileContentCat, COMM.EDFCopyright):
    """EDF R&D Copyright not found
    Even if there is a copyright to another company than EDF R&D, the source
    has been changed for Code_Aster conformance and version management.
    Example:

       - Copyright 2008 Company ABC
       - Copyright 2008-2025 EDF R&D www.code-aster.org
    """

    id = "C1005"


class ExternalCopyright(FileContentCat, COMM.ExternalCopyright):
    """Another Copyright than EDF"""

    id = "W1006"


# Checkers on bibfor
_lock_listfiles = Lock()
_cache_listfiles = {}


def _get_list_files(path):
    """Return the list of fortran and interfaces files as tuples:
    (basename, fullname)"""
    _lock_listfiles.acquire()
    cached = _cache_listfiles.get(path)
    if cached is None:

        def _basn(files):
            """Return basenames without extension"""
            return [osp.splitext(osp.basename(fname))[0] for fname in files]

        prev = os.getcwd()
        os.chdir(path)
        lfor = glob("bibfor/*.F90") + glob("bibfor/*/*.F90")
        linc = (
            glob("bibfor/include/asterfort/*.h")
            + glob("bibfor/include/asterfort/*/*.h")
            + glob("bibfor/include/fake/asterfort/*.h")
            + glob("bibfor/include/fake/asterfort/*/*.h")
        )
        os.chdir(prev)
        zfor = list(zip(_basn(lfor), lfor))
        zinc = list(zip(_basn(linc), linc))
        cached = zfor, zinc
        _cache_listfiles[path] = cached
    _lock_listfiles.release()
    assert (
        type(cached) is tuple
        and len(cached) == 2
        and type(cached[0]) is list
        and type(cached[1]) is list
    )
    return cached


class UniqueFilename(DirnameCat, GenericMsg):
    """Filename used more than once
    A filename must not be used several times."""

    id = "E1007"

    def search(self, txt):
        """Check for duplicate filenames"""
        zfor = _get_list_files(txt)[0]
        lfor = [i[0] for i in zfor]
        uniq = set(lfor)
        for fname in uniq:
            lfor.remove(fname)
        dble = [": %s.F90" % path for path in lfor]
        return dble


class InterfaceRequired(DirnameCat, GenericMsg):
    """Interface file is required for each subroutine
    An interface is required for each fortran subroutine or function.
    For a ``xxxx.F90`` file, a file defining the interface must exist in
    ``include/<subdirectory>/xxxx.h``.
    The modules must be named ``xxxx_module.F90`` (or ``xxxx_type.F90`` for
    the definition of types)."""

    id = "E1008"

    def search(self, txt):
        """Check for interfaces files"""
        zfor, zinc = _get_list_files(txt)
        dfor = dict(zfor)
        dinc = dict(zinc)
        mish = set(dfor.keys()).difference(list(dinc.keys()))
        req = []
        for path in mish:
            code = FC.source(filename=dfor[path])
            mod = code.get_type(FC.Module)
            if len(mod) != 1:
                req.append(": %s.F90" % path)
        return req


class UnusedInterface(DirnameCat, GenericMsg):
    """Interface without a fortran subroutine
    An interface (.h) exists for a fortran subroutine that does not exist."""

    id = "E1009"

    def search(self, txt):
        """Check for duplicate filenames"""
        zfor, zinc = _get_list_files(txt)
        dfor = dict(zfor)
        dinc = dict(zinc)
        mish = set(dinc.keys()).difference(list(dfor.keys()))
        req = [": %s.h" % path for path in mish]
        return req


class OutOfRangeSubroutine(FortranCodeCat, GenericMsg):
    """Numbered subroutine out of range
    Developpers must not add 'te', 'op' or 'lc' subroutines out of the
    predefined range: 'te' <= 600, 'op' < 200, 'lc' <= 1e6."""

    id = "C1010"
    _regexp = re.compile("^(?P<cat>(op|te|lc))(?P<num>[0-9]+)$")
    _crit = {"te": 600, "op": 199, "lc": 1000000}

    def search(self, code):
        """Check for file name"""
        lsub = code.get_type((FC.Subroutine, FC.Function))
        if len(lsub) == 0 and not code.get_type((FC.Module)):
            return [": no subroutine/function found!"]
        result = []
        for sub in lsub:
            mat = self._regexp.search(sub.name)
            if mat:
                maxi = self._crit.get(mat.group("cat"), -1)
                num = int(mat.group("num"))
                if num > maxi and num != 9999:
                    result.append(": subroutine '%s' out of range [1, %d]" % (sub.name, maxi))
        return result


# declarations errors


class ExpectedDimension(FileContentCat, TextMsg):
    """Expected another dimension in array declaration
    Size of arrays must be explicitly declared: 'array(0:)' is not tolerated,
    use 'array(0:N)' instead."""

    id = "C1301"
    search = search_msg("^ *(?P<main>" "%s.*?(?P<dim>[0-9]+:[,\)]|[\(,]:[0-9])" ")$" % RX.DECLS)


class DoublePrecision(FileContentCat, TextMsg):
    """Double precision type not allowed
    'double precision' should be replaced by 'real(kind=8)'.
    Ref. http://fortranwiki.org/fortran/show/Real+precision"""

    id = "C1302"
    search = search_msg("^ *(?P<main>double precision.*?)$")


class CharacterTooLong(FileContentCat, TextMsg):
    """Character variable too long
    Character variables size must not exceed 256 characters."""

    id = "W1303"
    search = search_msg("^ *(?P<main>character\s*\( *len *= *" "(?P<len>[0-9]+)\).*?)$")

    def check(self, msglist, txt):
        """Check the strings length"""
        self.match = self.search(txt)
        count = 0
        for match in self.match:
            if int(match.group("len")) <= 256:
                continue
            count += 1
            self.store_msg(msglist, match, txt)
        return count


class AutomaticArray(FortranCodeCat, GenericMsg):
    """Automatic arrays
    Automatic arrays must be used with care of their size."""

    id = "W1306"

    def search(self, code):
        """Search for automatic arrays"""
        blocks = code.get_type((FC.Subroutine, FC.Function))
        exp0 = re.compile("\(.*$")
        larr = []
        for sub in blocks:
            args = sub.attr["arg"]
            if not args:
                return []
            lvar = []
            decl_block = sub.get_type(FC.Decl)
            for decl in decl_block:
                lvar.extend([i for i in decl.attr["arg"] if "(" in i])
            expr = re.compile(
                "\((|.*[ ,]+|)(%s)(|[ ,].*)\)" % "|".join([re.escape(arg) for arg in args])
            )
            for var in lvar:
                raw = exp0.sub("", var)
                if raw in args:
                    continue
                if expr.search(var):
                    larr.append(var)
        result = [": %s" % path for path in larr]
        return result


class VariableLength(FortranCodeCat, GenericMsg):
    """Variable name too long
    The maximum length for a variable name is 24."""

    id = "W1307"

    def search(self, code):
        """Search for variable names"""
        longvar = []
        for decl in code.get_type(FC.Decl):
            if decl.attr["type"] in ("public", "private", "type"):
                # subroutine or type in a module: pass
                continue
            if "private" in decl.attr["attr"]:
                # private parameter, type... in a module: pass
                continue
            var = decl.attr["names"]
            longvar.extend([": " + i for i in var if len(i) > VARLENGTH])
        return longvar


class UninitializedPointer(FortranCodeCat, GenericMsg):
    """Uninitialized pointer
    Pointers must be initialized to 'null()'."""

    id = "C1310"

    def search(self, code):
        """Check declaration of local variables"""
        lsub = code.get_type((FC.Subroutine, FC.Function))
        if len(lsub) == 0 and not code.get_type((FC.Module)):
            return [": no subroutine/function found!"]
        errmsg = ": pointer '%s' not initialized to 'null()' but '%s'"
        result = []
        for sub in lsub:
            decls = sub.get_type(FC.Decl)
            for stmt in decls:
                names = [
                    i
                    for i in stmt.attr["names"]
                    if i not in sub.args and "pointer" in stmt.attr["attr"]
                ]
                for i in names:
                    vini = stmt.attr["init"][i]
                    if not vini or "null()" not in vini:
                        result.append(errmsg % (i, vini))
        return result


class SubroutineNameLength(FortranCodeCat, GenericMsg):
    """Subroutine name too long
    The maximum length for a subroutine name is 32."""

    id = "C1311"

    def search(self, code):
        """Check subroutine name"""
        lsub = code.get_type((FC.Subroutine, FC.Function))
        if len(lsub) == 0 and not code.get_type((FC.Module)):
            return [": no subroutine/function found!"]
        result = []
        for sub in lsub:
            name = sub.name
            if len(name) > SUBLENGTH:
                result.append(": " + name)
        return result


class UnrecommendedIf(FortranCodeCat, GenericMsg):
    """Unrecommended IF statement before macros, use `if (...) then ... end if`
    The syntax without `then` (also called "logical if") is considered archaic
    and may cause unexpected error when a macro is replaced by several
    statements (only the first one is really under the condition).

    Ref.: http://en.wikibooks.org/wiki/Fortran/Fortran_control"""

    id = "C1312"

    def search(self, code):
        """Search for if statements"""
        lst = []
        macro = _macros(os.getcwd())
        for stmt in code.get_type(FC.IfThen):
            # print stmt.dbg()
            if stmt.attr["then"] != "":
                continue
            for tag in macro:
                if stmt.attr["instr"].startswith(tag):
                    lst.append(stmt.code_text())
        return lst


class RealWithoutKind(FortranCodeCat, GenericMsg):
    """Kind value of real/complex must be 4 or 8
    Real and complex variables must be declared with an explicit range and
    in most case, it should be 'real(kind=8)' or 'complex(kind=8)'."""

    id = "C1313"

    def search(self, code):
        """Search for real/complex declaration statements"""
        var = []
        for decl in code.get_type(FC.Decl):
            if decl.attr["type"] in ("real", "complex") and decl.attr["kind"] not in (4, 8):
                var.extend([": " + i for i in decl.attr["names"]])
        return var


class ImplicitSave(FortranCodeCat, GenericMsg):
    """Unauthorized implicit save
    Implicit save is too dangerous. If it's intended, 'save' attribute must be
    explicitly added.
    """

    id = "C1314"

    def search(self, code):
        """Check declaration of local variables"""
        lsub = code.get_type((FC.Subroutine, FC.Function))
        if len(lsub) == 0 and not code.get_type((FC.Module)):
            return [": no subroutine/function found!"]
        errmsg = " of '%s' (type '%s')"
        result = []
        for sub in lsub:
            decls = sub.get_type(FC.Decl)
            for stmt in decls:
                if (
                    "save" in stmt.attr["attr"]
                    or "parameter" in stmt.attr["attr"]
                    or "pointer" in stmt.attr["attr"]
                ):
                    continue
                names = [i for i in stmt.attr["names"] if i not in sub.args]
                for i in names:
                    vini = stmt.attr["init"][i]
                    if vini:
                        result.append(errmsg % (i, stmt.attr["type"]))
        return result


# standard conformance
# http://www.codezone.co.uk/fortran/deprecatedfeatures.shtml


class NameList(FileContentCat, TextMsg):
    """Nonstandard syntax"""

    id = "C1401"
    search = search_msg("^\s*(?P<main>NAMELIST *\/.*?)$")


class NonStdDeclaration(FileContentCat, TextMsg):
    """Nonstandard type declaration
    'real*8' and similar declarations must no longer be used.
    Ref. http://fortranwiki.org/fortran/show/Real+precision"""

    id = "C1402"
    search = search_msg("^ *(?P<main>%s\s*\*.*?)$" % RX.DECLS)


class NoExecutableStatement(FileContentCat, TextMsgNotFound):
    """No executable statement"""

    id = "W1403"
    # assignment or STATS
    search = search_msg("^ *([a-z0-9 \(\)]+ *=|%s)" % STATS)


# coding conventions


class LineNumber(FileContentCat, TextMsg):
    """More than 500 lines or than 200 per subroutines in modules"""

    id = "W1501"
    fmt = "%(id)s: %(label)s"
    _max = 500
    _max_inmod = 200

    def search(self, txt):
        """Return the line numbers"""
        instr = [i for i in txt.splitlines() if not COMMENT_FOR.search(i)]
        return list(range(len(instr)))

    def check(self, msglist, txt):
        """Check the strings length"""
        self.match = self.search(txt)
        count = len(self.match)
        code = FC.source(txt)
        if code.get_type(FC.Module):
            nsub = len(code.get_type((FC.Subroutine, FC.Function)))
            nsub = max(nsub, 1)
            if count > nsub * self._max_inmod:
                self.store_msg_minimal(msglist, ": %d lines for %d subroutines" % (count, nsub))
        else:
            if count > self._max:
                self.store_msg_minimal(msglist, ": %d lines" % count)
        return count


class TooManyContinuation(FileContentCat, TextMsg):
    """More than 19 continuation lines"""

    id = "W1502"
    fmt = "%(id)s: %(label)s"

    def search(self, txt):
        """Return the number of continuation"""
        ltest = [line[-1] for line in txt.splitlines() if line]
        cont = [0]
        for cch in ltest:
            if cch == "&":
                cont[-1] += 1
            else:
                cont.append(0)
        return cont

    def check(self, msglist, txt):
        """Check the strings length"""
        self.match = self.search(txt)
        count = max(self.match)
        if count > 19:
            self.store_msg_minimal(msglist, ": %d continuations" % count)
        return count


class TooManyArgs(FortranCodeCat, GenericMsg):
    """More than 20 arguments"""

    id = "W1504"
    fmt = "%(id)s: %(label)s"

    def search(self, code):
        """Check for number of arguments"""
        lsub = code.get_type((FC.Subroutine, FC.Function))
        res = []
        for sub in lsub:
            nargs = len(sub.args)
            if nargs > 20:
                res.append(": '{0}' has {1} arguments".format(sub.name, nargs))
        return res


class ContinuedDecl(FileContentCat, TextMsg):
    """Declaration with continuation line
    For readibility the declarations must not be continued. This is only allowed
    for the declarations of constant arrays (with ``parameter`` attribute)."""

    id = "C1505"
    _search = search_msg("^ *(?P<main>%s.*?&) *$" % RX.DECLS)

    def search(self, txt):
        """Search for continuated lines but without parameter"""
        lmat = self._search(txt)
        return [mat for mat in lmat if not re.search("parameter", mat.group("main"))]


class InlineComment(FileContentCat, TextMsg):
    """Inline comment"""

    id = "C1506"

    def search(self, txt):
        """Search comment, no ['"] before '!'."""
        lmat = search_msg("^ *(?P<p>[^\s!#]+.*)(?P<main>!.*)$")(txt)
        return [mat for mat in lmat if not re.search("[\"']", mat.group("p"))]


class NotContinueOrFormatLabel(FileContentCat, DiffCat, TextMsg):
    """Label only before continue or format
    For loop, use do/end do."""

    # to simplify the parser
    id = "I1507"
    search = search_msg("^ *(?P<main>[0-9]+ +(?P<stmt>\w+.*))$")

    def check(self, msglist, txt):
        """Check the authorized statement"""
        self.match = self.search(txt)
        count = 0
        for match in self.match:
            stmt = match.group("stmt").lower()
            if stmt.startswith("continue") or stmt.startswith("format"):
                continue
            count += 1
            self.store_msg(msglist, match, txt)
        return count


class LabelSize(FileContentCat, DiffCat, TextMsg):
    """Recommendation: 3 digits max. for labels"""

    id = "I1508"
    search = search_msg("^ *(?P<main>[0-9]{3}[0-9]+ +(?P<stmt>\w+.*))$")


class LineTooLong(FileContentCat, COMM.LineTooLong):
    """Line too long
    Lines must not be too long for readability and convention.
    Maximum line length is 100 columns in code_aster fortran source files."""

    id = "C1509"
    _maxlength = 100


class DebugMode(FilenameCat, GenericMsg):
    """Debug mode in 'calcul' enabled
    This major subroutine must not be committed with the debug mode enabled."""

    id = "C1510"
    _regexp = re.compile(r"^ *(?P<main>dbg *= *" r"(?:\.true(|_[0-9]+)\.|ASTER_TRUE))", re.M)

    def search(self, fname):
        """Check for file name"""
        result = []
        if osp.basename(fname) == "calcul.F90":
            with open(fname, "r") as fobj:
                content = fobj.read()
            if self._regexp.search(content):
                result.append(": set 'dbg' to .false.")
        return result


class InvalidCharacter(FileContentCat, COMM.InvalidCharacter):
    r"""Invalid character, unexpected tabulation '\t'
    Tabulation character is not allowed."""

    # tabs in source code are detected by NoTabs, this avoids them in comments
    id = "C1511"


class MultilineAssert(FileContentCat, TextMsg):
    """ASSERT must be on a single line
    The fortran preprocessor does not always support macros on several lines.
    It depends on the preprocessor version."""

    id = "C1512"
    fmt = "%(id)s: %(label)s: %(main)s"
    search = search_msg("^ *(?P<main>ASSERT *\(.*&) *$")


# fixed-form
if ASCFG.get("fortran.form") == "fixed":

    class FixedFormDisable(HiddenMsg):
        """Messages hidden until source are in fixed form"""

        id = "I8999"
        hidden = "C0404,C0407,C1402,C1506"


class CompilationDirective(FileContentCat, TextMsg):
    """Compilation directives are not recommended
    Compilation directives must only be used for strong reasons."""

    id = "C1513"
    search = search_msg("^ *! *(?P<main>(DIR\$|\$OMP|MIC\$|\$PAR).*) *$")


class ReformatSource(FilenameCat, COMM.ReformatFort):
    """Reformat Fortran source"""

    id = "C1522"


class ForHeaderDisable(HiddenMsg):
    """Messages hidden for header files"""

    id = "I8886"
    hidden = "C1001,C1402,W1403,C1503,C1506"


# vars used before set/set but never used
# --> list of vars...


class ReservedFilename(FilenameCat, GenericMsg):
    """Reserved filename
    These patterns are reserved to identify fortran types and modules
    definition : ``xxxx_type.F90`` for public types, ``xxxx_module.F90``
    for modules.
    """

    id = "C9006"
    fmt = "%(id)s: %(label)s"

    def search(self, fname):
        """Check for file name"""
        result = []
        if fname.endswith("_module.F90") or fname.endswith("_type.F90"):
            code = FC.source(filename=fname)
            mod = code.get_type(FC.Module)
            if len(mod) != 1:
                result.append(": %s" % fname)
        return result


class InvalidFilename(FilenameCat, GenericMsg):
    """Invalid filename for a module
    The module must be names ``xxxx_module.F90`` to allow automatic checkings
    using different compilers between build and checkings.
    """

    id = "C9007"
    fmt = "%(id)s: %(label)s"

    def search(self, fname):
        """Check for file name"""
        result = []
        code = FC.source(filename=fname)
        if code.get_type((FC.Module)):
            if not (fname.endswith("_module.F90") or fname.endswith("_type.F90")):
                result.append(": %s" % fname)
        return result


def remove_continuation(block):
    """Remove continuation and new line characters."""
    expr = re.compile("& *. *&?", flags=re.DOTALL)
    return expr.sub("", block)


_lock_macros = Lock()
_cache_macros = None


def _macros(path_src):
    """Fill defines in cache"""
    global _cache_macros
    re_def = re.compile("^# *define *(?P<macro>\w+)", re.M)
    _lock_macros.acquire()
    if _cache_macros is None:
        _cache_macros = []
        includes = glob("bibfor/include/*.h")
        includes.extend(["bibfor/include/asterfort/assert.h"])
        for inc in includes:
            try:
                with open(osp.join(path_src, inc), "r") as fobj:
                    txt = fobj.read()
            except IOError:
                continue
            defs = [mat.group("macro") for mat in re_def.finditer(txt)]
            _cache_macros.extend(defs)
    _lock_macros.release()
    return _cache_macros


CHECK_LIST = checkers_from_context(globals(), TextMsg)
