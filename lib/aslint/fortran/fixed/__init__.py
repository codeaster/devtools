# coding=utf-8

"""
This package contains the functions to check, parse, change, etc. fortran source
files in fixed form.
"""

from warnings import warn, simplefilter

# DeprecationWarning are ignored in python2.7 by default
simplefilter('default')

# XXX later
# warn("The module works on source files in fixed form and will "
#     "probably not evolve anymore.",
     # DeprecationWarning, stacklevel=2)
