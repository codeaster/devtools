# coding=utf-8

"""Transitional script to change person in charge"""


import os.path as osp
import re

from aslint.logger import logger
from aslint.config import ASCFG
from aslint.utils import apply_func_text, remove_lines


def read_csv(fname):
    """Read two columns csv file"""
    dres = {}
    with open(fname, 'r') as fobj:
        for line in fobj:
            if not line:
                continue
            spl = line.split(',')
            if len(spl) != 2:
                continue
            dres[spl[0].strip()] = spl[1].strip()
    return dres

dresp = read_csv(osp.join(ASCFG.get('aslint.root'),
                          'fortran', 'migration', 'resp_login.csv'))


def change_responsible(fname):
    """Change login of the responsible to its email"""
    logger.info(fname)
    expr = re.compile('^(?P<decl>[!Cc#&%/\*]{1,2}) *RESPONSABLE +'
                      '(?P<login>\w+).*?(?P<close>\*/)? *$',
                      re.MULTILINE)

    def func_text(txt):
        """replace string"""
        mat = expr.search(txt)
        if not mat:
            return txt
        email = dresp.get(mat.group('login'))
        if not email:
            print("#NOT FOUND", mat.group('login'), fname)
            return expr.sub('\g<decl>', txt)
        eol = ''
        if mat.group('close'):
            eol = ' ' + mat.group('close')
        return expr.sub('\g<decl> person_in_charge: %s%s' % (email, eol), txt)
    apply_func_text(func_text, fname)


def change_cmodif(fname):
    """Remove MODIF line"""
    expr_cata = re.compile('^(?P<decl>%&) +MODIF +(?P<subdir>\w+) +.*$', re.M)
    fmodif = re.compile(
        '^(?P<decl>(?:[!Cc#&@]{1,2}|/\*)) +MODIF +', re.M).search

    def func_text(txt):
        """remove the MODIF line"""
        iscata = expr_cata.search(txt)
        if iscata:
            txt = expr_cata.sub('%& LIBRARY \g<subdir>', txt)
        else:
            txt = remove_lines(txt, fmodif)
        return txt
    apply_func_text(func_text, fname)
