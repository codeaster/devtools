# coding=utf-8

"""
Functions to call external tools.
"""

import os
import os.path as osp
import re
import tempfile
from functools import partial
from subprocess import PIPE, Popen

from aslint.decorators import interrupt_decorator
from aslint.fortran.formatting import (MAX_INDENT, STEP_INDENT, is_f90_txt,
                                       reformat_source)
from aslint.i18n import _
from aslint.logger import logger
from aslint.string_utils import convert
from aslint.utils import apply_func_text, apply_in_sandbox

TOOLSDIR = osp.join(osp.abspath(osp.dirname(__file__)), os.pardir, 'tools')
CONVERT = osp.join(TOOLSDIR, 'convert')


def convert_metcalf(txt, tmpdir):
    """Call the external tool 'convert' on the source text."""
    prev = os.getcwd()
    os.chdir(tmpdir)
    fsrc = tempfile.NamedTemporaryFile(
        prefix='f', dir=os.getcwd(), suffix='.f')
    fsrc = osp.basename(fsrc.name)
    with open(fsrc, 'w') as fobj:
        fobj.write(txt)
    prc = Popen([CONVERT], stdin=PIPE, stdout=PIPE)
    args = '%s %d %d F F\n' % (osp.splitext(fsrc)[0], STEP_INDENT, MAX_INDENT)
    logger.debug('tempfile: %s, args:%r', fsrc, args)
    out = convert(prc.communicate(args)[0])
    f90 = osp.splitext(fsrc)[0] + '.f90'
    if prc.returncode != 0 or not osp.isfile(f90):
        logger.error("FAIL: %s", txt.splitlines()[0])
        logger.error(out)
        logger.error(_("check that the source file is in fixed-form"))
        logger.exit()
    else:
        with open(f90, 'r') as fobj:
            txt = fobj.read()
    os.chdir(prev)
    return txt


def convert_to_free_form(txt, ext, tmpdir=None):
    """Convert a fortran source into free format."""
    # detect 'include' or 'fortran90'
    from_inc = ext == '.h'
    from_f90 = is_f90_txt(txt)
    if from_inc:
        txt = '      subroutine fake\n' + txt + '\n      end'
    if from_f90:
        txt = re.compile('^!', flags=re.M).sub('C', txt)
        txt = re.compile('^#', flags=re.M).sub('C%fpp%', txt)
    txt = txt.replace('\t', ' ' * 8)
    txt = convert_metcalf(txt, tmpdir)
    txt = remove_old_style_decl(txt)
    txt = reformat_source(txt, interface=from_inc)
    if from_f90:
        txt = re.compile('^!%fpp%', flags=re.M).sub('#', txt)
    if from_inc:
        txt = os.linesep.join(txt.splitlines()[1:-1])
    txt = _manual_posts(txt)
    return txt


@interrupt_decorator
def _swap_convert(fname, tmpdir=None):
    """Required to swap arguments"""
    logger.info(fname)
    func_text = partial(convert_to_free_form, ext=osp.splitext(fname)[-1],
                        tmpdir=tmpdir)
    return apply_func_text(func_text, fname)


def convert_all(args, numthread):
    """Convert all files in 'args'."""
    assert osp.exists(CONVERT), 'not exists: %s, use the Makefile' % CONVERT
    args = [osp.abspath(fname) for fname in args]
    apply_in_sandbox(_swap_convert, args, numthread, pass_tmpdir=True)


def remove_old_style_decl(txt):
    """Replace REAL*8 by REAL(KIND=8)...
    Be carefull to change COMPLEX*16 into COMPLEX(KIND=8),
    LEN for CHARACTER..."""
    def repl_cmplx(match):
        """to compute complex kind"""
        return '%s%s(kind=%d)' % (match.group('pre'),
                                  match.group('type'),
                                  int(match.group('dim')) // 2)
    # declare functions separately
    decls = r'(integer|real|logical|character)[\* ]*[0-9\(\)]*'
    func = re.compile('^(?P<pre> *)(?P<type>%s) +'
                      'function +(?P<f>\w+) *\(' % decls, re.M | re.I)
    mat = func.search(txt)
    if mat:
        txt = func.sub('\g<pre>FUNCTION \g<f>(', txt, count=1)
        txt = DREG['IMPL'].sub('      IMPLICIT NONE\n      %s %s'
                               % (mat.group('type'), mat.group('f')), txt)
    # add :: (must be run first)
    txt = DREG['::'].sub('\g<pre>\g<type>\g<dim> :: ', txt)
    txt = re.compile('^(?P<pre> *)(?P<type>integer|real) *\* *(?P<dim>[0-9]+)',
                     flags=re.I | re.M).sub('\g<pre>\g<type>(kind=\g<dim>)', txt)
    txt = re.compile('^(?P<pre> *)(?P<type>complex) *\* *(?P<dim>[0-9]+)',
                     flags=re.I | re.M).sub(repl_cmplx, txt)
    txt = re.compile('^(?P<pre> *)(?P<type>character) *\* *(?P<dim>[0-9]+)',
                     flags=re.I | re.M).sub('\g<pre>\g<type>(len=\g<dim>)', txt)
    txt = re.compile('^(?P<pre> *)(?P<type>character) *\* *\( *\* *\)',
                     flags=re.I | re.M).sub('\g<pre>\g<type>(len=*)', txt)
    return txt

_compil = partial(re.compile, flags=re.DOTALL)


def _manual_posts(txt):
    """too specific"""
    txt = _compil('d&(. *)atset').sub('&\g<1>datset', txt)
    txt = _compil('dat&(. *)set').sub('&\g<1>datset', txt)
    txt = _compil('nomas&(. *)u').sub('&\g<1>nomasu', txt)
    txt = _compil('typre&(. *)s').sub('&\g<1>typres', txt)
    txt = _compil('troid&(. *)l').sub('&\g<1>troidl', txt)
    txt = _compil('jmar&(. *)q').sub('&\g<1>jmarq', txt)
    txt = _compil('ntye&(. *)le').sub('&\g<1>ntyele', txt)
    txt = _compil('compt&(. *)e').sub('&\g<1>compte', txt)
    txt = _compil('typm&(. *)av').sub('&\g<1>typmav', txt)
    txt = _compil('zer&(. *)o').sub('&\g<1>zero', txt)
    txt = _compil('11\.&(. *)97d\-12').sub('&\g<1>11.97d-12', txt)
    txt = _compil('11\.97d\-1&(. *)2').sub('&\g<1>11.97d-12', txt)
    txt = _compil('243&(. *)5').sub('&\g<1>2435', txt)
    for key in ('EQ1', 'EQ2', 'EQ3'):
        txt = DREG[key].sub('&\g<1>.eq.', txt)
    for key in ('AND1', 'AND2', 'AND3', 'AND4'):
        txt = DREG[key].sub('&\g<1>.and.', txt)
    for key in ('OR1', 'OR2', 'OR3'):
        txt = DREG[key].sub('&\g<1>.or.', txt)
    # for f90 interface added by patch 2 in f77
    txt = txt.replace(' :: (kind=', '(kind=')
    txt = txt.replace(' :: (len=', '(len=')
    txt = txt.replace(' :: (*)', '(*)')
    txt = txt.replace(':: ::', '::')
    txt = txt.replace(' :: ,', ',')
    txt = txt.replace('real :: ro', 'realro')
    txt = txt.replace('real :: ve', 'realve')
    txt = petsc_decls_fix(txt)
    return txt


def petsc_decls_fix(txt):
    """Change PETSc declarations manually"""
    chg = [('vec', 'Vec'), ('mat', 'Mat'), ('ksp', 'KSP'), ('pc', 'PC'),
           ('PetscInt', 'PetscInt'), ('PetscReal', 'PetscReal'),
           ('PetscScalar', 'PetscScalar'), ('PetscOffset', 'PetscOffset'),
           ('VecScatter', 'VecScatter'),
           ('KSPConvergedReason', 'KSPConvergedReason'),
           ]
    for typ, into in chg:
        expr = re.compile('^( +)%s( +[a-zA-Z])' % typ, flags=re.M)
        txt = expr.sub('\g<1>%s ::\g<2>' % into, txt)
    return txt

# to speed up
DREG = {
    'IMPL': re.compile('^      implicit *none', re.I | re.M),
    '::': re.compile('^(?P<pre> *)(?P<type>integer|real|complex|character) *'
                     '(?P<dim>\*[ \(]*[0-9\*a-z]+[ \)]*|)', re.I | re.M),
    'EQ1': _compil('\.&(. *)eq\.'),
    'EQ2': _compil('\.e&(. *)q\.'),
    'EQ3': _compil('\.eq&(. *)\.'),
    'AND1': _compil('\.&(. *)and\.'),
    'AND2': _compil('\.a&(. *)nd\.'),
    'AND3': _compil('\.an&(. *)d\.'),
    'AND4': _compil('\.and&(. *)\.'),
    'OR1': _compil('\.&(. *)or\.'),
    'OR2': _compil('\.o&(. *)r\.'),
    'OR3': _compil('\.or&(. *)\.'),
}
