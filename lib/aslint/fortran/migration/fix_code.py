# coding=utf-8

"""
Functions to fix the code
"""

import os
import os.path as osp
import re
from functools import partial

from aslint.logger import logger, INFO
from aslint.utils import apply_func_text, join_lines
from aslint.fortran.free.change import remove_vars
from aslint.fortran.free.change_code import remove_dummy_arg
from aslint.fortran.fixed.utils import lines_blocks
from aslint.base_checkers import MsgList
import aslint.fortran.free.fortran_code as FC
import aslint.fortran.gfortran_checkers as GFORT
import aslint.fortran.static_fortran_checkers as STAT


def fix_code(report, fix=True):
    """Try to fix the reported errors."""
    args = set()
    for fname, msglist in report.with_msg_iter():
        bname = osp.basename(fname)
        # fix rank mismatch
        rank_msg = MsgList([msg for msg in msglist
                            if isinstance(msg, GFORT.RankMismatch)])
        if len(rank_msg) > 0:
            args.add(fname)
            if fix:
                rank_msg = rank_msg.sort_by_position(reverse=True)
                for msg in rank_msg:
                    fixit = partial(fix_rank_mismatch, params=msg.result)
                    apply_func_text(fixit, fname, info_level=INFO)
        # remove long declarations
        cont_decl = MsgList([msg for msg in msglist
                             if isinstance(msg, STAT.ContinuedDecl)])
        if len(cont_decl) > 0:
            args.add(fname)
            if fix:
                for msg in cont_decl:
                    apply_func_text(fix_cont_decl, fname, info_level=INFO)
        # remove unused variables
        # TODO use change_code.remove_vars
        uv_msg = MsgList([msg for msg in msglist
                          if isinstance(msg, GFORT.UnusedVar)])
        if len(uv_msg) > 0:
            args.add(fname)
            if fix:
                uvars = [msg.result['elt'] for msg in uv_msg]
                if uvars:
                    fixit = partial(remove_vars, variables=uvars)
                    apply_func_text(fixit, fname, info_level=INFO)
        # remove dummy arguments
        dum_msg = MsgList([msg for msg in msglist
                           if isinstance(msg, GFORT.DummyArgument)])
        dummy = [msg.result['elt'] for msg in dum_msg]
        if dummy and not (bname.startswith('lc0')
                          or bname.startswith('te0')):
            args.add(fname)
            if fix:
                others = []
                try:
                    remove_dummy_arg(fname, dummy, append_to=others)
                    args.update(others)
                except AssertionError:
                    logger.error('FAIL: %s %s', fname, dummy)
    args = list(args)
    args.sort()
    return args


def fix_cont_decl(txt):
    """Fix continued declarations (id=C1505)."""
    src = FC.source(text=txt)
    return src.code_text()

# to refactor


def fix_rank_mismatch(txt, params):
    """Fix the 'rank mismatch' errors (id=E0103)."""
    line = int(params['line'])
    logger.debug('fix_rank_mismatch: %r', params)
    func_block = partial(fix_rank_mismatch_block, params=params)
    txt = change_block_at(txt, line, func_block)
    return txt


def fix_rank_mismatch_block(blck, linec, params):
    """Fix the rank mismatch error in the block"""
    col = int(params['col'])
    if col == 72:
        linec = linec + 1
        col = 6
    lines = blck.splitlines()
    lmod = lines[linec]
    splitted = lmod[col:].split(',', 1)
    var = splitted[0]
    if len(splitted) < 2:
        end = ''
    else:
        end = ',' + splitted[1]
    if var.count(')') > var.count('('):
        var, plus = var.split(')', 1)
        end = ')' + plus + end
    assert var.count('(') == var.count(')'), \
        'split between parenthesis %r\n%s' % (var, blck)
    new = params['repl'].replace(GFORT.VARNAME, var)
    new_line = lmod[:col] + new + end
    lines[linec] = split_long_line(new_line)
    return os.linesep.join(lines)


def fix_bad_space(txt):
    """Replace bad spaces : 'END IF', 'GO TO'..."""
    txt = txt.replace('\t', ' ' * 8)
    txt = re.sub('END +IF', 'ENDIF', txt)
    txt = re.sub('GO +TO', 'GOTO', txt)
    # 1 024 > 1024 - erfcfo
    for dummy in range(3):
        txt = DREG['N N'].sub('\g<1>\g<2>\g<3>', txt)
    # 1. D0
    txt = DREG['1 D0'].sub('\g<pre>\g<n>\g<e>\g<exp>', txt)
    # '. AND .'
    for oper in ('AND', 'OR', 'EQ', 'NE', 'LT', 'LE', 'GT', 'GE',
                 'TRUE', 'FALSE'):
        # up to 3 by line
        for i in range(3):
            txt = re.compile('^([^C].*?)(\. +%s +\.)' % oper,
                             flags=re.I | re.M).sub('\g<1>.%s.' % oper, txt)
            txt = re.compile('^([^C].*?)(\. +%s *\.)' % oper,
                             flags=re.I | re.M).sub('\g<1>.%s.' % oper, txt)
            txt = re.compile('^([^C].*?)(\. *%s +\.)' % oper,
                             flags=re.I | re.M).sub('\g<1>.%s.' % oper, txt)
        # .\n  OR.
        txt = re.compile('(\. *). (    & *)%s *\.' % oper,
                         flags=re.M | re.DOTALL).sub('\n\g<2>.%s.' % oper, txt)
        txt = re.compile('(\.%s *). (    & *)\.' % oper,
                         flags=re.M | re.DOTALL).sub('\n\g<2>.%s.' % oper, txt)
    # CALLSUB : CALL SUB
    txt = DREG['CALLSUB'].sub('\g<1>CALL \g<2>', txt)
    # DO 500I= : DO 500 I=...
    txt = DREG['DO1'].sub('\g<1>DO \g<2> \g<3>', txt)
    # continuation character in column 5
    txt = DREG['CONT5'].sub('     &', txt)
    txt = _manual_changes(txt)
    return txt


def _manual_changes(txt):
    """too specific"""
    # not generic: erfcfo, te0564, slegro#177, jjlchd & at end
    txt = txt.replace('1.0D 0', '1.0D0')
    txt = DREG['CH8,'].sub('\g<1>CHARACTER*8 ', txt)
    txt = txt.replace(',INTF 1,', ',INTF1,')
    txt = txt.replace('10.\n     &D0', '10.D0\n     &')
    txt = txt.replace('IMA 1= ZI(', 'IMA1 = ZI(')
    return txt

# XXX move to utils


def change_block_at(txt, line, func_block):
    """Generic wrapper to change a block at the given line
    by calling 'func_block'."""
    blocks, nlin = lines_blocks(txt)
    i = -1
    for i, index in enumerate(nlin):
        if line - 1 < index:
            i = i - 1
            break
    index = nlin[i]
    blck = blocks[i]
    logger.debug('block #%d found at index %d: %s', i, index, blck)
    assert index > 0
    linec = (line - 1) - index
    logger.debug('block to change starts at index #%d (line=%d, lc=%d):\n%s',
                 index, line, linec, blck)
    new_block = func_block(blck, linec)
    logger.debug("new block:\n%s", new_block)
    blocks[i] = new_block
    return join_lines(blocks)


def split_long_line(line):
    """Split 'line' if it is too long"""
    if len(line) > 72:
        wrap = '\n     &'
        index = line.rfind(',', 7, 72) + 1
        line = line[:index] + wrap + line[index:]
    return line

# to speed-up
DREG = {
    '1 D0': re.compile('^(?P<pre>[^C].*?)(?P<n>[0-9\-\+=] *\.[0-9]*) +'
                       '(?P<e>[ed]) *(?P<exp>[\-\+]?[0-9])', re.I | re.M),
    'N N': re.compile('^(      .*?)([0-9]+) +([0-9])', re.M),
    'CALLSUB': re.compile('^( *)CALL([^ ])', re.M),
    'DO1': re.compile('^( *)DO +([0-9]+)([A-Z])', re.M),
    'CONT5': re.compile('^    [&\+A-Z] ', re.I | re.M),
    'CH8,': re.compile('^(?P<pre>[^C].*?)CHARACTER\*8 *,', re.M),
}
