# coding=utf-8

"""Decorators"""

import os
import os.path as osp
import time
from functools import partial, wraps

from aslint.i18n import _
from aslint.logger import logger


def interrupt_decorator(func):
    """Pass KeyboardInterrupt to be catched by the multiprocessing Pool"""
    @wraps(func)
    def wrapper(*args, **kwds):
        """wrapper"""
        try:
            return func(*args, **kwds)
        except KeyboardInterrupt as exc:
            if logger.debug_enabled():
                raise
            raise RuntimeError(exc)
    return wrapper


def stop_on_failure(method):
    """decorator to highlight the execution of a method and check its error code
    The instance must have a 'check_error' method."""
    @wraps(method)
    def wrapper(inst, *args, **kwds):
        """wrapper"""
        logger.add_cr()
        logger.debug('--- begin %s ---', method.__name__)
        errcode = method(inst, *args, **kwds)
        logger.debug('---  end  %s ---', method.__name__)
        logger.update_errcode(errcode)
        inst.check_error()
        return errcode
    return wrapper


def log_as_important(func):
    """decorator to mark messages as important during execution"""
    @wraps(func)
    def wrapper(*args, **kwds):
        """wrapper"""
        logger.start_important()
        results = func(*args, **kwds)
        logger.add_cr()
        logger.stop_important()
        return results
    return wrapper


def allow_cancel_error(method, testcases=False):
    """decorator to allow to ask confirmation to cancel an error
    The instance must have a 'confirm' method."""
    @wraps(method)
    def wrapper(inst, *args, **kwds):
        """wrapper"""
        errcode = method(inst, *args, **kwds)
        logger.update_errcode(errcode)
        if logger.is_error():
            if not testcases:
                logger.warn(_("Your request should be refused, but if you "
                              "think that it will be accepted anyway, you can "
                              "continue."))
            else:
                logger.warn(_("Please check the report at "
                              "https://aster.retd.edf.fr/. "
                              "If the errors are well known on your platform, "
                              "you can continue. Otherwise, you have to fix "
                              "them."))
            inst.confirm()
            logger.cancel_error()
        return logger.errcode
    return wrapper


allow_cancel_error_testcases = partial(allow_cancel_error, testcases=True)


def locked(method):
    """decorator to lock a feature using a basic lock file.
    The instance must have a 'check_error' method and a 'lockfile' attribute.
    """
    @wraps(method)
    def wrapper(inst, *args, **kwds):
        """wrapper"""
        def _check():
            if osp.exists(inst.lockfile):
                with open(inst.lockfile, 'r') as flock:
                    txt = flock.read()
                logger.error(txt)
                inst.check_error()

        def _lock():
            logger.info("adding {0}".format(inst.lockfile))
            with open(inst.lockfile, 'w') as flock:
                flock.write("locked at {0} by process {1}\n"
                            .format(time.asctime(), os.getpid()))

        def _unlock():
            logger.info("removing {0}".format(inst.lockfile))
            os.remove(inst.lockfile)

        _check()
        _lock()
        try:
            retvalue = method(inst, *args, **kwds)
        finally:
            _unlock()
        return retvalue
    return wrapper
