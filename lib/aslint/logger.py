# coding=utf-8

"""This module defines a logger object and error functions for the package"""

import argparse
import io
import logging
import logging.handlers as HDLR
import os
import os.path as osp
import sys
import tempfile
import time
from functools import partial

# using these values allows to use them as `lvl` in `logger.log(lvl, ...)`
ERROR = logging.ERROR
WARN = logging.WARNING
INFO = logging.INFO
TITLE = INFO + 1
VERBOSE = (WARN + INFO) // 2
OK = INFO
DEBUG = logging.DEBUG

RETURNCODE = {OK: 0, DEBUG: 0, VERBOSE: 0, WARN: 2, ERROR: 4}
assert OK < VERBOSE < WARN < ERROR, (OK, VERBOSE, WARN, ERROR)


class Abort(Exception):
    """Raised when execution is aborted."""


class ErrorCode(object):

    """Error code for several commands and functions"""

    __slots__ = "value"

    def __init__(self, initial_value=OK):
        """Initialization"""
        self.value = initial_value

    def add(self, other):
        """Keep the worse error code"""
        # depend on OK, WARN, ERROR values
        if type(other) is ErrorCode:
            other = other.value
        if other not in list(RETURNCODE.keys()):
            other = ERROR
        if other <= VERBOSE:
            other = OK
        self.value = max(self.value, other)

    def is_value(self, value):
        """Tell if it is equal to `value`"""
        return self.value == value

    def is_ok(self):
        """Tell if it is OK"""
        return self.is_value(OK)

    def is_warn(self):
        """Tell if it is WARN"""
        return self.is_value(WARN)

    def is_error(self):
        """Tell if it is ERROR"""
        return self.is_value(ERROR)

    def numeric(self):
        """Return the numeric value for sys.exit"""
        code = self.value
        if code not in list(RETURNCODE.keys()):
            code = ERROR
        return RETURNCODE[code]

    def from_status(self, status):
        """Set ERROR if status is not null (not the reverse of numeric)"""
        if status == 0:
            value = OK
        else:
            value = ERROR
        self.add(value)

    def cancel_error(self):
        """Give the possibility to deprecate an error into a simple warning"""
        if self.is_error():
            self.value = WARN

    def cancel_warning(self):
        """Give the possibility to cancel a warning"""
        if self.is_warn():
            self.value = OK

    def exit(self):
        """Call sys.exit() with the numeric status"""
        sys.exit(self.numeric())


def timestamp(as_path=False):
    """Return a timestamp with milliseconds.

    Arguments:
        as_path (bool): If True, ensure that the string is a valid path.
    """
    _now = time.time()
    _msec = (_now - int(_now)) * 1000
    fmt = "%a-%d-%H%M%S" if as_path else "%a-%d-%H:%M:%S"
    return time.strftime(fmt) + ".{:03d}".format(int(_msec))


class PerLevelFormatter(logging.Formatter):

    """Formatter for aslint messages"""

    def _adjust_format(self, level):
        """Adjust the format for the given level"""
        if level >= logging.WARNING:
            self._fmt = "%(levelname)-7s %(message)s"
        elif level == VERBOSE:
            self._fmt = "INFO    %(message)s"
        elif level == DEBUG:
            self._fmt = "DEBUG:%s: %%(message)s" % timestamp()
        else:
            self._fmt = "%(message)s"

    def format(self, record):
        """Enhance error and warning messages"""
        lvl = record.levelno
        self._adjust_format(lvl)
        return logging.Formatter.format(self, record)


class PerLevelColorFormatter(PerLevelFormatter):

    """Formatter for aslint messages"""

    def _adjust_color(self, level):
        """Choose a color function according to the level"""
        func = lambda message: message
        if level >= logging.ERROR:
            func = red
        elif level >= logging.WARN:
            func = blue
        elif level == TITLE:
            func = grey
        return func

    def format(self, record):
        """Enhance error and warning messages"""
        lvl = record.levelno
        return self._adjust_color(lvl)(PerLevelFormatter.format(self, record))


class HgStreamHandler(logging.StreamHandler):

    """StreamHandler switching between sys.stdout and sys.stderr
    like the mercurial ui does"""

    def _adjust_stream(self, level):
        """Adjust the stream according to the given level"""
        self.flush()
        if level >= logging.WARNING:
            self.stream = sys.stderr
        else:
            self.stream = sys.stdout

    def emit(self, record):
        """Enhance error and warning messages"""
        self._adjust_stream(record.levelno)
        return logging.StreamHandler.emit(self, record)


class AslintLogger(object):

    """A mixed object defining a Logger with a FileHandler to store all the
    messages and the capabilities of an ErrorCode object to keep a global
    status"""

    # fatal and critical methods have not been defined

    def __init__(self, level=logging.INFO):
        """Initialize the logger with its handlers"""
        logger = logging.getLogger("aslint")
        logger.setLevel(level)
        term = HgStreamHandler(sys.stdout)
        term.setFormatter(PerLevelColorFormatter())
        logger.addHandler(term)
        memh = HDLR.MemoryHandler(capacity=1e6)
        logger.addHandler(memh)
        imph = HDLR.MemoryHandler(capacity=1e6)
        logger.addHandler(imph)
        self._log = logger
        self._mhdlr = memh
        self._important = imph
        self._text = ""
        self._err = ErrorCode()
        self.stop_important()

    # Methods for logging tasks
    def setLevel(self, level):
        """Set the level of the logger"""
        self._log.setLevel(level)

    def debug(self, msg, *args, **kwargs):
        """Delegate a debug call to the underlying logger"""
        self._log.debug(msg, *args, **kwargs)

    def info(self, msg, *args, **kwargs):
        """Delegate a info call to the underlying logger"""
        self._log.info(msg, *args, **kwargs)

    def title(self, msg, *args, **kwargs):
        """Delegate a log call at the TITLE level to the underlying logger
        Same as info but with different formatting"""
        self._log.log(TITLE, msg, *args, **kwargs)

    def verbose(self, msg, *args, **kwargs):
        """Delegate a log call at the VERBOSE level to the underlying logger
        Same as info but with different formatting"""
        self._log.log(VERBOSE, msg, *args, **kwargs)

    def warning(self, msg, *args, **kwargs):
        """Delegate a warn call to the underlying logger"""
        self._err.add(WARN)
        self._log.warning(msg, *args, **kwargs)

    warn = warning

    def error(self, msg, *args, **kwargs):
        """Delegate a error call to the underlying logger"""
        self._err.add(ERROR)
        try:
            stop = kwargs.pop("exit")
        except KeyError:
            stop = False
        self._log.error(msg, *args, **kwargs)
        if stop:
            self.exit()

    def log(self, level, msg, *args, **kwargs):
        """Delegate a log call to the underlying logger"""
        self._err.add(level)
        self._log.log(level, msg, *args, **kwargs)

    def log_current(self, msg, *args, **kwargs):
        """Log the message at the current error code"""
        self.log(self.errcode, msg, *args, **kwargs)

    def getEffectiveLevel(self):
        """Ask the underlying logger"""
        return self._log.getEffectiveLevel()

    def debug_enabled(self):
        """Tell if debugging is enabled"""
        return self.getEffectiveLevel() <= DEBUG

    def add_cr(self):
        """Log a line break"""
        self.info("")

    def start_important(self):
        """Following messages will be important"""
        self._important.setLevel(self.getEffectiveLevel())

    def stop_important(self):
        """Do not log following messages as important"""
        self._important.setLevel(ERROR + 1)

    def _read_text(self):
        """Store the content of all log records"""
        return _empty_handler(self._mhdlr)

    def get_important_text(self):
        """Store the content of all log records"""
        return _empty_handler(self._important)

    def get_text(self):
        """Return the content of all log records"""
        self._text = os.linesep.join([self._text, self._read_text()])
        return self._text

    def write_log(self, rootname, path=None):
        """Write the logger content to `LOGDIR/rootname.log`
        Try LOGDIR = $HOME/log, $PWD or system TMPDIR."""
        dirs = []
        if path:
            dirs.append(osp.join(path, "log"))
        dirs.extend([os.getcwd(), None])
        fobj, logf = None, None
        for dlog in dirs:
            if dlog is None:
                logf = tempfile.NamedTemporaryFile(prefix=rootname + ".").name
            else:
                logf = osp.join(dlog, rootname + ".log")
            try:
                fobj = open(logf, "w")
                break
            except IOError:
                continue
        if fobj is not None:
            self.info("log content written to: %s", logf)
            fobj.write(self.get_text())
            fobj.close()
        else:
            self.warn("can not write the log file")
        return logf

    # Methods on error code
    def update_errcode(self, errcode):
        """Keep the worse error code"""
        if errcode is None:
            errcode = self.errcode
        self._err.add(errcode)

    def update_from_status(self, status):
        """Keep the worse error code using status"""
        self._err.from_status(status)

    @property
    def errcode(self):
        """Return the current error code"""
        return self._err.value

    def is_ok(self):
        """Tell if it is OK"""
        return self._err.is_ok()

    def is_warn(self):
        """Tell if it is WARN"""
        return self._err.is_warn()

    def is_error(self):
        """Tell if it is ERROR"""
        return self._err.is_error()

    def cancel_error(self):
        """Change an ERROR code into a WARN"""
        return self._err.cancel_error()

    def cancel_warning(self):
        """Change an WARN code into a OK"""
        return self._err.cancel_warning()

    def exit(self, ignore_warning=True):
        """Call sys.exit() with the numeric status.

        Arguments:
            ignore_warning (bool): If *True* exists with 0 even if warnings
                have been raised (default).
        """
        if ignore_warning:
            self.cancel_warning()
        self._err.exit()

    def abort(self, msg, *args, **kwargs):
        """Emit an error message and raise an exception."""
        self.error(msg, *args, **kwargs)
        raise Abort()


def _empty_handler(memory_handler):
    """Store the content of all log records"""
    textio = io.StringIO()
    hdlr = logging.StreamHandler(textio)
    hdlr.setFormatter(PerLevelFormatter())
    memory_handler.setTarget(hdlr)
    memory_handler.flush()
    hdlr.close()
    text = textio.getvalue()
    textio.close()
    memory_handler.setTarget(None)
    return text


def setlevel(*dummy, **kwargs):
    """Callback for verbose/debug option"""
    lvl = kwargs.get("level", logging.DEBUG)
    logger.setLevel(lvl)


class SetLevelAction(argparse.Action):
    """Define a custom action for '--debug' argument that changes the level."""

    def __init__(self, option_strings, dest, **kwargs):
        super(SetLevelAction, self).__init__(
            option_strings, dest, nargs=0, const=True, default=False, help="add debug informations"
        )

    def __call__(self, *args, **kwargs):
        setlevel()


logger = AslintLogger()
if os.getenv("DEBUG", "0") == "1":
    setlevel()

COLOR = {
    "red": "\033[1;31m",
    "green": "\033[1;32m",
    "blue": "\033[1;34m",
    "grey": "\033[1;30m",
    "magenta": "\033[1;35m",
    "cyan": "\033[1;36m",
    "yellow": "\033[1;33m",
    "endc": "\033[1;m",
}

_colored = sys.stdout.isatty()


def _colorize(color, string):
    """Return the colored `string`"""
    if not _colored or not string.strip():
        return string
    return COLOR[color] + string + COLOR["endc"]


red = partial(_colorize, "red")
green = partial(_colorize, "green")
blue = partial(_colorize, "blue")
magenta = partial(_colorize, "magenta")
cyan = partial(_colorize, "cyan")
yellow = partial(_colorize, "yellow")
grey = partial(_colorize, "grey")

if sys.getfilesystemencoding().lower() != "utf-8":
    logger.warn(
        "System encoding is not 'utf-8' but '{0}'.\n"
        "If an error occurs please check the LANG environment variable "
        "(current value '{1}', try: LANG=fr_FR.utf8).".format(
            sys.getfilesystemencoding(), os.environ.get("LANG", "")
        )
    )
