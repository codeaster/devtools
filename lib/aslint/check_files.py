# coding=utf-8

"""Function to check source files"""

import os
import os.path as osp
import pickle
import re
import shutil
import tempfile
from subprocess import PIPE, STDOUT, Popen, call

from .api_asrun import check_asrun, check_asrun_bin, get_asrun_config
from .baseutils import force_list
from .check_diff import check_diff_loop
from .check_global import check_global
from .config import ASCFG
from .decorators import log_as_important
from .filetype import filter_names, typelist
from .fortran.check_source import check_fortran_loop, check_header_loop
from .i18n import _
from .logger import ErrorCode, logger
from .other.check_other import (
    check_c_loop,
    check_capy_loop,
    check_cata_loop,
    check_cxx_loop,
    check_datg_loop,
    check_mater_loop,
    check_other_loop,
)
from .python.check_python import check_python_loop
from .test.check_test import check_test_loop
from .test_result import Diag, TestResult
from .utils import get_proc_numb, read_testlist


def check_files(
    report,
    filtered_changes,
    c4che=None,
    srcdir=None,
    dry_run=False,
    skip_global=False,
    numthread=None,
):
    """Check the files already filtered by type
    If given, `srcdir` is the directory name the filenames are relative to.

    Arguments:
        report (Report): Report object to be completed.
        filtered_changes (dict): List of files to be checked.
        c4che (dict): Parameters of the build.
        srcdir (str): Source path.
        dry_run (bool): Do not execute any command if *True*.
        numthread (int): Number of threads run in parallel.
        skip_global (bool): Allow to ignore global checkings.
    """
    logger.title(_("checking the changed files"))
    numthread = numthread or get_proc_numb()
    prev = os.getcwd()
    if srcdir:
        os.chdir(srcdir)
    for typ in typelist(with_none=True):
        flist = filtered_changes.get(typ)
        if not flist:
            continue
        args = filter_names(flist, "AMR")
        logger.info(_("checking {0:4d} files of type '{1}'").format(len(args), typ))
        if dry_run:
            logger.info(_("(DRY RUN) list of files:"))
            logger.info(os.linesep.join(args))
            continue
        irep = None
        if typ == "for":
            try:
                incdir = []
                for key in c4che:
                    if key.startswith("INCLUDES"):
                        path = c4che[key]
                        if not isinstance(path, list):
                            path = [path]
                        incdir.extend(path)
                flags = ASCFG.get("fortran.fcflags").split()
            except (TypeError, KeyError):
                logger.error(
                    _("waf parameters are required, run 'waf configure' to fill the parameters")
                )
                continue
            logger.debug("filenames :%s", args)
            logger.debug("incdir: %s", incdir)
            logger.debug("fcflags: %s", flags)
            compiler = c4che.get("FC_NAME", "").lower()
            irep = check_fortran_loop(args, numthread, flags, incdir, compiler)
        elif typ == "hf":
            irep = check_header_loop(args, numthread)
        elif typ == "test":
            irep = check_test_loop(args, numthread)
        elif typ in ("c", "h"):
            irep = check_c_loop(args, numthread)
        elif typ in ("cxx", "hxx"):
            irep = check_cxx_loop(args, numthread)
        elif typ in ("py", "catalo"):
            irep = check_python_loop(args, numthread)
        elif typ == "cata":
            irep = check_cata_loop(args, numthread)
        elif typ == "capy":
            irep = check_capy_loop(args, numthread)
        elif typ == "datg":
            irep = check_datg_loop(args, numthread)
        elif typ == "mater":
            irep = check_mater_loop(args, numthread)
        elif typ in ("build", None):
            irep = check_other_loop(args, numthread)
        else:
            # uncomment to force error
            # irep = check_other_loop(args, numthread)
            logger.info(_("this type is not yet checked: '%s'"), typ)
        report.merge(irep)
    if not dry_run and not skip_global:
        report.merge(check_global(srcdir or prev))
    os.chdir(prev)


def check_diff(report, diffs):
    """Check the diff of files given by type"""
    numthread = get_proc_numb()
    for typ in typelist(with_none=True):
        dict_diff = diffs.get(typ)
        if not dict_diff:
            continue
        args = list(dict_diff.keys())
        logger.info(_("checking diff of {0:4d} files of type '{1}'").format(len(args), typ))
        irep = check_diff_loop(typ, dict_diff, numthread)
        report.merge(irep)


def run_waf_command(
    repodir, wafcmd, command, options=[], env=None, dry_run=False, return_output=False
):
    """Check the build with waf"""
    errcode = ErrorCode()
    command = force_list(command)
    options = force_list(options)
    logger.title(
        _("run the waf command: {0} {1} {2}").format(wafcmd, " ".join(command), " ".join(options))
    )
    prev = os.getcwd()
    os.chdir(repodir)
    waf = osp.abspath(wafcmd)
    logger.debug("waf script: %s", waf)
    cmd = (
        [
            waf,
        ]
        + command
        + options
    )
    if env:
        env = " && ".join([". " + i for i in force_list(env)])
        cmd = " ".join(["(", env, "&&"] + cmd + [")"])
        cmd = ["/bin/bash", "-c", cmd]
    logger.debug("command: %s", cmd)
    try:
        if not dry_run:
            proc = Popen(cmd, stdout=PIPE, stderr=STDOUT, bufsize=1, universal_newlines=True)
            output = proc.communicate()[0]
            returncode = proc.wait()
            errcode.from_status(returncode)
        else:
            logger.info(_("(DRY RUN) %s"), cmd)
    except OSError as exc:
        logger.error(_("waf failed: %s"), exc.strerror)
    finally:
        os.chdir(prev)
    if errcode.is_ok():
        logger.info(_("waf ended successfully"))
    else:
        logger.log(errcode.value, _("waf exited with code %d"), returncode)
        logger.error(output)
    res = errcode
    if return_output:
        res = errcode, output
    return res


def read_waf_parameters(cache_file, prefer_sequential=False):
    """Read waf parameters from its c4che file"""
    if not osp.isfile(cache_file):
        return None
    # from waflib/ConfigSet.py
    re_imp = re.compile(r"^(#)*?([^#=]*?)\ =\ (.*?)$", re.M)
    with open(cache_file, "r") as fobj:
        txt = fobj.read()
    dcach = {}
    for mat in re_imp.finditer(txt):
        grp = mat.group
        if not grp(1):
            dcach[grp(2)] = eval(grp(3))
    # for backward compatibility - see BB0006
    if type(dcach.get("ASTERDATADIR")) is list:
        dcach["ASTERDATADIR"] = dcach["ASTERDATADIR"][0]

    if c4che_ismpi(dcach) and prefer_sequential:
        # check if 'std' configuration exists
        wafc4che = osp.join("build", "std", "c4che", "release_cache.py")
        waf2 = read_waf_parameters(wafc4che)
        if waf2 and not c4che_ismpi(waf2):
            dcach = waf2
    return dcach


@log_as_important
def run_testcase(
    params_,
    dry_run=False,
    is_success=None,
    add_seq=False,
    add_restart=False,
    no_retry=False,
    resu_init=None,
):
    """Run a list of testcases.

    If batch mode is expected, it separates testcases that must be run in
    interactive mode in a dedicated execution.

    Arguments:
        params_ (dict): Parameters.
        dry_run (bool): Do not execute the testcases, only print command line.
        is_success (Diag): Execution status that is considered as a success.
        add_seq (bool): Also run sequential testcases with MPI version.
        add_restart (bool): Automatically add restart at the end of testcases.
        no_retry (bool): By default (*False*) give a second chance to the
            testcases that fails. If *True*, it disables the second run.
        resu_init (*TestResult*): *TestResult* object of a previous run.

    Returns:
        ErrorCode, TestResult: List of errors and object containing the results.
    """
    params = params_.copy()
    values = {}
    if add_restart:
        values["add_restart"] = True
    if is_success is None:
        is_success = Diag.Success

    flist = params["list"]
    flist_b = tempfile.NamedTemporaryFile(prefix="list", suffix=".nointeract").name
    flist_i = tempfile.NamedTemporaryFile(prefix="list", suffix=".interact").name
    if resu_init:
        resu_init.report(_("Report of the previously executed testcases"), show_longest=False)
        already_run = resu_init.get_diag().keys()
        fprev = tempfile.NamedTemporaryFile(prefix="list", suffix=".not_rerun").name
        with open(fprev, "w") as fobj:
            fobj.write(os.linesep.join(already_run))
        exclude_test(flist, fprev)

    shutil.copyfile(flist, flist_b)
    shutil.copyfile(flist, flist_i)

    logger.title(_("running the testcases without those with " "'verif_interact' tag"))
    filter_list(
        flist_b,
        params,
        params["have_mpi"],
        add_seq,
        filters=['--filter="verif_interact" not in testlist'],
    )
    params["list"] = flist_b
    change_export_files(params, values)
    err_b, res_b = run_testcase_2(params, dry_run, is_success, no_retry)

    logger.title(_("running the testcases with 'verif_interact' tag"))
    filter_list(
        flist_i,
        params,
        params["have_mpi"],
        add_seq,
        filters=['--filter="verif_interact" in testlist'],
    )
    params["list"] = flist_i
    params["mode"] = "interactif"
    change_export_files(params, values)
    err_i, res_i = run_testcase_2(params, dry_run, is_success, no_retry)

    result = TestResult(resudir=None, is_success=is_success)
    dict_resu = {}
    if resu_init:
        dict_resu.update(resu_init.get_diag())
    dict_resu.update(res_b.get_diag())
    dict_resu.update(res_i.get_diag())
    result.read_diag(dict_resu)
    title = _("final results of all the testcases")
    result = TestResult(params["resu_test"], is_success)
    result.read_diag(dict_resu)
    result.report(title)
    result.report_xunit("testcases from {0}".format(params_["list"]))

    err_b.add(err_i)
    return err_b, result


def run_testcase_1(params, dry_run, is_success):
    """Run a list of testcases.

    Arguments:
        See :py:func:`run_testcase`.
    """
    check_asrun_bin()
    errcode = ErrorCode()
    clean = True
    dtmp = tempfile.mkdtemp(prefix="test_")
    foutput = osp.join(dtmp, "output")
    ferror = osp.join(dtmp, "error")
    returncode = "?"
    result = []
    try:
        params = _check_runtest_params(params, dtmp)
        result = TestResult(params["resu_test"], is_success)
        with open(params["list"], "r") as fobj:
            tlist = fobj.read().splitlines()
        tlist = [line for line in tlist if not re.search("^[%#]", line) and line.strip()]
        if len(tlist) == 0:
            logger.info(_("no testcases to execute"))
            return errcode, result
        export = export_from_parameters(params)
        logger.debug(_("calling as_run with the export file:\n%s"), export)
        fexport = osp.join(dtmp, params["nomjob"] + ".export")
        with open(fexport, "w") as fobj:
            fobj.write(export)
        cmd = [ASCFG.get("asrun.bin"), fexport]
        logger.info(_("directory of the results of the testcases: %s"), params["resu_test"])
        if dry_run:
            logger.info(_("(DRY RUN) execute: %s"), cmd)
            logger.warn(_("(DRY RUN) testcases not run"))
        else:
            logger.info(_("waiting for the end of the %d testcases..."), len(tlist))
            logger.info(_("output is buffered in %s"), foutput)
            with open(foutput, "w") as fout, open(ferror, "w") as ferr:
                proc = Popen(cmd, stdout=fout, stderr=ferr, bufsize=1, universal_newlines=True)
            returncode = proc.wait()
            errcode.from_status(returncode)
            clean = not errcode.is_error()
            if osp.isfile(params["diag"]):
                with open(params["diag"], "rb") as fpick:
                    dict_resu = pickle.load(fpick)
            else:
                logger.error(_("no such file: %s"), params["diag"])
                dict_resu = {}
            result.read_diag(dict_resu)
            nberr = result.report(_("results of the testcases run"))
            if nberr == 0:
                errcode.cancel_error()
                errcode.cancel_warning()
            else:
                logger.error(_("%6d testcases failed"), nberr)
    except AssertionError as exc:
        logger.error(_("can not run testcases: %s"), str(exc))
    finally:
        if clean and not logger.debug_enabled():
            shutil.rmtree(dtmp)
            logger.info(_("working directory cleaned"))
    if errcode.is_ok():
        logger.info(_("testcases run successfully"))
    else:
        logger.log(errcode.value, _("as_run exited with code %s"), returncode)
        logger.error(
            _("detailed output and results of the testcases " "execution can be found in: %s"), dtmp
        )
    return errcode, result


def run_testcase_2(params, dry_run, is_success, no_retry):
    """Run a list of testcase (same as `run_testcase_1`), but give a second
    chance for the testcases that end with an unknown status (supposed to be
    a server problem)"""
    errcode, result = run_testcase_1(params, dry_run, is_success)
    unk = result.get_result(Diag.Unknown | Diag.NotRun)
    if not no_retry and unk:
        logger.info(_("giving a second chance to %d testcases"), len(unk))
        tmpf = tempfile.NamedTemporaryFile()
        tmpf.close()
        with open(tmpf.name, "w") as fobj:
            fobj.write(os.linesep.join(unk))
        par2 = params.copy()
        par2["list"] = tmpf.name
        err2, res2 = run_testcase_1(par2, dry_run, is_success)
        if err2.is_ok() and not set(result.get_error()).difference(unk):
            logger.cancel_error()
            errcode = err2
        # global diagnostic
        dict_resu = result.get_diag()
        dict_resu.update(res2.get_diag())
        title = _("results of the testcases run after the second run")
        result = TestResult(params["resu_test"], is_success)
        result.read_diag(dict_resu)
        nberr = result.report(title)
        logger.cancel_error()
        if nberr:
            logger.error(_("%6d testcases failed"), nberr)
    return errcode, result


def _check_runtest_params(params, dtmp):
    """Add default values for undefined parameters to run the testcases"""
    assert "version" in params
    params["mode"] = params.get("mode", "batch")
    params["nomjob"] = params.get("nomjob", "run_testcase")
    params["nbmaxnook"] = params.get("nbmaxnook", ASCFG.get("test.nbmaxnook"))
    params["facmtps"] = params.get("facmtps", ASCFG.get("test.facmtps"))
    params["tpsjob"] = params.get("tpsjob", 3600)
    params["resu_test"] = params.get("resu_test", osp.join(dtmp, "results"))
    params["cpresok"] = params.get("cpresok", "RESNOOK")
    params["diag"] = params.get("diag", osp.join(dtmp, "diag.pick"))
    params["debug"] = params.get("debug", "nodebug")
    logger.debug(_("export parameters: %s"), params)
    return params


def build_testcase_list(changes, testdir, repo=None):
    """Return the list of testcase to run: the official list + the changed ones.
    Only used for the legacy version.

    Arguments:
        changes: dict of changes between two revisions
        repo: name of the checked repository
    """
    changed_tests = set()
    deps_built = False
    logger.title(_("build the testcases dependencies"))
    for fname, status in changes["test"]:
        logger.debug("%s %s", status, fname)
        if not deps_built:
            deps = test_dependency(testdir)
            deps_built = True
        if status in ("M", "A"):
            tests = deps.get(osp.basename(fname))
            if not tests or None in tests:
                logger.warn(_("the file seems not used by any testcase: %s"), fname)
            else:
                changed_tests.update(tests)
    tlist = set(changed_tests)
    if repo in ("src", "data"):
        flist = osp.join(ASCFG.get("aslint_root"), "test", "list.submit")
        if ASCFG.get("admin._test"):
            flist += "._test"
        tests = read_testlist(flist)
        if not tests:
            logger.error(_("can not build the list of testcases"))
            return None
        tlist.update(tests)
    dtmp = tempfile.mkdtemp(prefix="test_")
    flist = osp.join(dtmp, "list")
    with open(flist, "w") as fobj:
        fobj.write(os.linesep.join(tlist))
    return flist


def filter_list(flist, params, have_mpi, add_seq, filters=None):
    """Filter a list of testcases"""
    logger.title(_("build the list of testcases"))
    check_asrun_bin()
    tmpf = tempfile.NamedTemporaryFile()
    tmpf.close()
    with open(tmpf.name, "w") as fobj, open(flist, "r") as fobj2:
        fobj.write(fobj2.read())
    versdir = params["version"]
    cmd = [
        ASCFG.get("asrun.bin"),
        "--list",
        "--test_list=%s" % tmpf.name,
        "--nodebug_stderr",
        "--vers=%s" % versdir,
    ]
    filt = []
    if have_mpi:
        if not add_seq:
            filt.append('--filter="parallel" in testlist')
    else:
        filt.append('--filter="parallel" not in testlist')
    if filters:
        filt.extend(filters)
    logger.info(_("filters used: {0}").format(filt))
    cmd.extend(filt)
    cmd.extend(["-o", flist])
    logger.update_from_status(call(cmd))
    os.remove(tmpf.name)


def exclude_test(flist, fexcl):
    """Exclude some testcases, overwrite 'flist' in place"""
    fexcl = osp.join(ASCFG.get("aslint_root"), "test", fexcl)
    exclude = read_testlist(fexcl)
    lines = read_testlist(flist)
    lines = [line for line in lines if line.strip() and line.strip() not in exclude]
    with open(flist, "w") as fobj:
        fobj.write(os.linesep.join(lines))
    return len(lines)


def test_dependency(testdir):
    """Build a dict: filename : [exports that are using it]"""
    check_asrun()
    from asrun.profil import AsterProfil

    files = set(os.listdir(testdir))
    files.discard("wscript")
    lexp = [fname for fname in files if fname.endswith(".export")]
    files.difference_update(lexp)
    deps = {}
    for path in lexp:
        pexp = AsterProfil(osp.join(testdir, path))
        testname = osp.splitext(path)[0]
        deps[osp.basename(path)] = [testname]
        for entry in pexp.get_data():
            if osp.basename(entry.path) != entry.path:
                logger.warn(_("unexpected filenames for a testcase: %s"), entry.path)
            deps[entry.path] = deps.get(entry.path, []) + [testname]
    return deps


ASTOUT_EXPORT = """
P actions astout
P nomjob %(nomjob)s
P mode %(mode)s
P version %(version)s
P debug %(debug)s

P ncpus 1
P nbmaxnook %(nbmaxnook)s
P cpresok %(cpresok)s
P facmtps %(facmtps)s
P tpsjob %(tpsjob)s

F list %(list)s D 0
R resu_test %(resu_test)s R 0
P diag_pickled %(diag)s
"""


def export_from_parameters(params):
    """Return the content of the export file.

    Arguments:
        params (dict): Dict of parameters to run testcases.
            For the needed keys see ASTOUT_EXPORT template.
    Returns:
        str: Text of the export file.
    """
    lines = [ASTOUT_EXPORT % params]
    if params.get("args"):
        args = force_list(params["args"])
        lines.extend(["A args %s" % i for i in args])
    if params.get("parameters"):
        args = force_list(params["parameters"])

    lines.extend(params.get("lines", []))
    export = os.linesep.join(lines)
    return export


def change_export_files(params, new_values):
    """Change export files by setting `new_values`.
    Add necessary parameters to `params` to take these export files into
    account to execute the testcases.
    Add a 'lines' value in `params`.

    Arguments:
        params (dict): Run parameters (required: 'list', 'version').
            'rep_test' will be added.
        new_values (dict): New values.
    """
    if not new_values:
        return
    config = get_asrun_config(params["version"])
    dirtest = config.get_with_absolute_path("SRCTEST")
    if not dirtest:
        logger.warn(_("No testcases directory found!"))
        return
    # make directory for export files
    expdir = osp.join(params["resu_test"], "export_changed")
    if not osp.exists(expdir):
        os.makedirs(expdir)
    # list of additions
    ladd = []
    restart = new_values.pop("add_restart", False)
    if restart:
        fcomm = tempfile.NamedTemporaryFile().name
        with open(fcomm, "w") as fobj:
            fobj.write("POURSUITE()\nFIN()\n")
        ladd.append("F comm {0} D 1\n".format(fcomm))
    # list of substitutions
    lsub = []
    for key, value in list(new_values.items()):
        lsub.append([re.compile(r"\b{0}\b.*$".format(key), re.M), "{0} {1}".format(key, value)])
    # loop on testcases
    tlist = read_testlist(params["list"])
    for test in tlist:
        lexp = [
            osp.join(tdir, test + ".export")
            for tdir in dirtest
            if osp.exists(osp.join(tdir, test + ".export"))
        ]
        if len(lexp) < 1:
            # will be reported at execution
            continue
        with open(lexp[0], "r") as fexport:
            content = fexport.read()
        for expr, repl in lsub:
            content = expr.sub(repl, content)
        content = content.rstrip() + "\n"
        for line in ladd:
            content += line
        with open(osp.join(expdir, test + ".export"), "w") as fexport:
            fexport.write(content)
    # add directory as a directory containing testcases
    params["lines"] = params.get("lines", [])
    exp_line = "R rep_test {0} D 0".format(expdir)
    if exp_line not in params["lines"]:
        params["lines"].append(exp_line)
    logger.info(_("Changed export files written into {0}").format(expdir))


def c4che_ismpi(c4che):
    """Tell if waf c4che dict has MPI support enabled.

    Arguments:
        c4che (dict): waf parameters dict.

    Returns:
        bool: *True* for a MPI version, *False* otherwise.
    """
    # HAVE_MPI for version < 15.3.3
    return c4che.get("ASTER_HAVE_MPI") or c4che.get("HAVE_MPI")
