# coding=utf-8

"""Some utilities"""

import difflib
import multiprocessing as MPR
import os
import os.path as osp
import re
import shutil
import tempfile
from functools import lru_cache, partial
from glob import glob
from hashlib import sha1
from subprocess import PIPE, CalledProcessError, Popen, check_call

from aslint.baseutils import force_list
from aslint.i18n import _
from aslint.logger import logger
from aslint.string_utils import convert

MAX_NUMTHREADS = 8


def remove_lines(txt, func):
    """Remove (block of) lines of 'txt' that match 'func(block)'"""
    lines = [blck for blck in txt.splitlines() if not func(blck)]
    return join_lines(lines)


def end_with_newline(txt):
    """Add a new line at the end if there is not one."""
    return join_lines(txt.splitlines())


def join_lines(lines, end_cr=True):
    """Join the lines and add a new line if necessary
    and if 'end_cr' is True."""
    if not lines:
        return os.linesep
    if end_cr and lines[-1].strip() != "":
        # do not change input lines
        lines = lines + [""]
    return os.linesep.join(lines)


def re_all_sub(
    list_exp, repl, txt, nmin=1, nmax=999999, count=999999, ignore_comment=True, flags=None
):
    """For each regular expression of 'list_exp', replace first 'count'
    occurrences by 'repl' if found at least 'nmin' times and no more than
    'nmax' times."""
    found = 0
    fhead = rhead = ""
    flags = flags or (re.I | re.M)
    if ignore_comment:
        fhead = "(?P<head>^ .*)"
        rhead = "\g<head>"
    if type(list_exp) not in (list, tuple):
        list_exp = [
            list_exp,
        ]
    for exp in list_exp:
        exp = fhead + exp
        expc = re.compile(exp, flags)
        lval = expc.findall(txt)
        if nmin <= len(lval) + found <= nmax:
            new = expc.sub(rhead + repl, txt)
            logger.debug(difftxt(txt, new))
            txt = new
            found += len(lval)
            if found >= count:
                break
        logger.debug("no match: expr=%r, lval=%r", exp, lval)
    return found, txt


def all_none(args):
    """Return True if all 'args' are None."""
    sargs = set(args)
    return len(sargs) == 1 and args[0] is None


def no_none(args):
    """Return True if no one of 'args' is None."""
    return None not in args


def difftxt(old, new, fromfile="old", tofile="new"):
    """Return the differences between two files as text."""
    return "".join(
        difflib.unified_diff(old.splitlines(1), new.splitlines(1), fromfile=fromfile, tofile=tofile)
    )


def save_auto(content, lsrc=None, root="file", ext=""):
    """Write content to a file automatically named."""
    if lsrc:
        common = osp.basename(osp.dirname(osp.commonprefix(lsrc)))
    if common:
        root = common
    i = 1
    fname = "%s.%d%s" % (root, i, ext)
    while osp.exists(fname) and i < 99:
        i += 1
        fname = "%s.%d%s" % (root, i, ext)
    assert i < 99, "too many files"
    with open(fname, "w") as fobj:
        fobj.write(content)
    return fname


def clean_dir(dirname, error=True, silent=False):
    """Remove a directory"""
    if not silent:
        logger.info(_("removing '{0}'...").format(dirname))
    to_remove = glob(dirname)
    for dirn in to_remove:
        try:
            if osp.isdir(dirn):
                shutil.rmtree(dirn)
            else:
                os.remove(dirn)
        except (IOError, OSError) as exc:
            if error:
                logger.warn(_("can not remove: '%s'"), dirn)
                logger.warn(_("Error:\n%s"), str(exc))


def apply_func_text(func_text, fname, backup_ext=None, info_level=None):
    """Run 'func_text' to change/check one file.
    The signature of 'func_text' is :
        `def func_text(original_text): return new_text`
    'backup_ext' is used to backup the original file if not null."""
    if info_level is not None:
        logger.log(info_level, " %s", fname)
    with open(fname, "r") as fobj:
        txt = fobj.read()
    txt = func_text(txt)
    if backup_ext:
        os.rename(fname, fname + backup_ext)
    with open(fname, "w") as fobj:
        fobj.write(end_with_newline(txt))


def apply_func_text_on_files(func_text, files, backup_ext=None, info_level=None):
    """Run 'func_text' on 'args'."""
    for fname in files:
        apply_func_text(func_text, fname, backup_ext, info_level)


def apply_func_text_on_dirs(func_text, ldirs, pattern="*.f", backup_ext=None):
    """Run 'func_text' on files from 'ldirs' matching the given 'pattern'."""
    for dname in ldirs:
        logger.info(_("Enter in directory %s..."), dname)
        lsrc = glob(osp.join(dname, pattern))
        apply_func_text_on_files(func_text, lsrc, backup_ext)


def apply_in_parallel(func, args, numthread):
    """Run `func` on each argument of `args` in parallel"""
    # if there is only one argument or if run in debug mode, it does not call
    # multiprocessing to ease debugging and give full traceback
    values = []
    if len(args) == 0:
        return values
    elif len(args) == 1:
        return func(args[0])
    if logger.debug_enabled():
        numthread = 1
    if numthread == 1:
        return [func(i) for i in args]
    pool = MPR.Pool(processes=numthread)
    try:
        result = pool.map_async(func, args)
        values = result.get()
        pool.close()
        pool.join()
    except (KeyboardInterrupt, RuntimeError):
        pool.terminate()
    return values


def apply_in_sandbox(func, args, numthread=None, pass_tmpdir=False):
    """Call `func` in a sandbox, loop on 'args'.
    pass_tmpdir/pass_id must be set to True if such arguments are
    required by 'func'."""
    if numthread is None:
        numthread = get_proc_numb()
    tmpdir = tempfile.mkdtemp(prefix="aslint.")
    kwargs = {}
    if pass_tmpdir:
        kwargs["tmpdir"] = tmpdir
    func_one = partial(func, **kwargs)
    result = None
    try:
        result = apply_in_parallel(func_one, args, numthread)
    finally:
        clean_dir(tmpdir, silent=True)
    return result


_numthread_cache = None


def get_proc_numb():
    """Return the number of core"""
    global _numthread_cache
    if _numthread_cache is not None:
        return _numthread_cache
    out = Popen(["cat", "/proc/cpuinfo"], stdout=PIPE).communicate()[0]
    out = convert(out)
    exp = re.compile(r"^processor\s+:\s+([0-9]+)", re.MULTILINE)
    l_ids = exp.findall(out)
    num = 1
    if len(l_ids) >= 1:  # else: it should not !
        num = max([int(i) for i in l_ids]) + 1
    _numthread_cache = min(num, MAX_NUMTHREADS)
    # check for installation problem: http://bugs.python.org/issue3770
    try:
        MPR.Queue()
    except ImportError as exc:
        if "sem_open implementation" in str(exc):
            _numthread_cache = 1
            logger.warn(
                _(
                    "Problem detected with your Python interpreter "
                    "(see http://bugs.python.org/issue3770): "
                    "only one thread will be used"
                )
            )
        # else: it should fail later
    return _numthread_cache


def indent(text, prefix):
    """Add `prefix` before each line of `text`."""
    l_s = [prefix + line for line in text.split(os.linesep)]
    return os.linesep.join(l_s)


def convert_addr(list_pic):
    """Convert a list of person in charge fields into email addresses
    Return the list of valid and invalid addresses"""
    re_at = re.compile(" +at +")
    error = []
    addr = []
    list_pic = force_list(list_pic)
    for string in list_pic:
        spl = [re_at.sub("@", add.strip()) for add in string.split(",")]
        for add in spl:
            if "@" not in add or add == "" or " " in add:
                error.append(add)
            else:
                addr.append(add)
    return addr, error


def convert_to_addr(list_pic):
    """Convert a list of person in charge fields into email addresses
    Just warn in case of invalid address."""
    addr, error = convert_addr(list_pic)
    for add in error:
        logger.warn(_("invalid email address: '%s'"), add)
    return addr


def prefix_from_options(opts):
    """Read --prefix value from options"""
    from optparse import OptionParser

    pars = OptionParser()
    # disable error
    pars.error = lambda err: False
    pars.add_option("--prefix")
    try:
        opts = pars.parse_args([i for i in opts if "--prefix" in i])[0]
        prefix = opts.prefix
    except IOError:
        prefix = None
    return prefix


# XXX split_diff sometimes splits badly


def split_diff(diffstring):
    """Split a `diffstring` and return a dict of the diffs by file"""
    splitter = re.compile(
        "^diff .* a/(?P<f1>\S*) b/(?P=f1)\n"
        "(|old mode .*\n)"
        "(|new mode .*\n)"
        "\-\-\- a/(?P=f1)\n"
        "\+\+\+ b/(?P=f1)\n",
        re.M,
    )
    hunk = splitter.split(diffstring)
    # expect an empty string (before the first 'diff', then filename + its
    # diff)
    empty = hunk.pop(0)
    if empty.strip() != "":
        logger.warn(_("unexpected diff format:\n%s"), diffstring)
        logger.error(_("unexpected diff format:\n%s"), empty)
    if len(hunk) % 2 != 0:
        logger.error(_("unexpected split of diff:\n%s"), hunk)
    dict_diff = {}
    while hunk:
        fname = hunk.pop(0)
        diff = hunk.pop(0)
        dict_diff[fname] = diff
        logger.debug("diff of '%s':\n%s", fname, diff)
    return dict_diff


def in_src(fname):
    """Tell the file is located in the 'src' repository"""
    return "src" in fname.split(os.sep)


def read_testlist(flist):
    """Return the list of testcases read from 'fname'"""
    tlist = []
    try:
        with open(flist, "r") as fobj:
            lines = fobj.read().splitlines()
        tests = [ctest for ctest in lines if not re.search("^[!#]", ctest)]
        tlist = set(tests)
    except IOError:
        pass
    return tlist


def checksum(fname):
    """Return the checksum of a file"""
    with open(fname, "rb") as fobj:
        fsum = sha1()
        for line in fobj:
            fsum.update(line)
    return fsum.hexdigest()


def open_editor(text="", editor=None):
    """Open an external editor with the given text.

    Arguments:
        text (str): Initial text.
        editor (list[str]): Command line for the editor (defaults: nano).

    Returns:
        str: Text entered in the editor.
    """
    ftxt = tempfile.NamedTemporaryFile().name
    with open(ftxt, "w") as fobj:
        fobj.write(text)

    input_text = ""
    try:
        check_call((editor or ["nano"]) + [ftxt])
    except Exception:
        pass
    else:
        if osp.exists(ftxt):
            with open(ftxt, "r") as fobj:
                input_text = fobj.read()
    finally:
        if osp.exists(ftxt):
            os.remove(ftxt)
    return input_text


@lru_cache(maxsize=10)
def have_program(program):
    """Tell if an executable is available.

    Arguments:
        program (str): Executable to be checked.

    Returns:
        bool: *True* if the program is responding, *False* otherwise.
    """
    try:
        check_call([program, "--version"], stdout=PIPE)
    except (CalledProcessError, FileNotFoundError):
        return False
    return True
