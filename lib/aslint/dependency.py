# coding=utf-8

"""Manipulate a graph dependencies"""


class Node:
    """A node a the graph"""

    def __init__(self, name):
        """Create a node with its name"""
        self.name = name
        self.edges = []

    def __repr__(self):
        """Represent a Node by its name"""
        return self.name

    def add_dependency(self, node):
        """Declare a new dependency"""
        self.edges.append(node)


def resolve_dependency(node, return_unresolved=False,
                       resolved=None, unresolved=None):
    """Resolve the dependencies"""
    resolved = resolved or []
    unresolved = unresolved or []
    unresolved.append(node)
    for edge in node.edges:
        if edge not in resolved:
            if edge in unresolved:
                raise ValueError('Circular reference detected: {0} -> {1}'
                                 .format(node.name, edge.name))
            resolved, unresolved = resolve_dependency(edge,
                                                      return_unresolved=True,
                                                      resolved=resolved,
                                                      unresolved=unresolved)
    resolved.append(node)
    unresolved.remove(node)
    if return_unresolved:
        return resolved, unresolved
    else:
        return resolved
