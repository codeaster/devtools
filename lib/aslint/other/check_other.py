# coding=utf-8

"""This module gives convenient functions to check files of types:
- C
- cata/capy
- datg
- mater
- other (untyped)
"""

from functools import partial

import aslint.common_checkers as COMM
import aslint.other.c_checkers as C
import aslint.other.capy_checkers as CAPY
import aslint.other.cata_checkers as CATA
import aslint.other.cxx_checkers as CXX
import aslint.other.datg_checkers as DATG
import aslint.other.mater_checkers as MATER
import aslint.other.other_checkers as OTHER
from aslint.base_checkers import (CheckList, MsgList, Report, check_disabled,
                                  check_file_content, check_filename)
from aslint.logger import logger
from aslint.utils import apply_in_sandbox


def check_loop(checklist, args, numthread):
    """Check all the files in 'args'. Return a report of the errors."""
    func_one = partial(check_one_file, checklist=checklist)
    lrep = apply_in_sandbox(func_one, args, numthread)
    report = Report()
    report.merge(lrep)
    return report


def check_one_file(fname, checklist):
    """Check one file"""
    logger.info(fname)
    lmsg = MsgList()
    lmsg.extend(check_filename(fname, checklist.on_filename()))
    lmsg.extend(check_file_content(fname, checklist.on_content()))
    check_disabled(fname, checklist, lmsg)
    report = Report()
    report.set(fname, lmsg)
    return report


def check_c_loop(args, numthread):
    """Check all the files in 'args'. Return a report of the errors."""
    checklist = CheckList()
    checklist.register(C.CHECK_LIST)
    checklist.register(COMM.CHECK_LIST)
    return check_loop(checklist, args, numthread)


def check_cxx_loop(args, numthread):
    """Check all the files in 'args'. Return a report of the errors."""
    checklist = CheckList()
    checklist.register(C.CHECK_LIST)
    checklist.register(CXX.CHECK_LIST)
    checklist.register(COMM.CHECK_LIST)
    return check_loop(checklist, args, numthread)


def check_capy_loop(args, numthread):
    """Check all the files in 'args'. Return a report of the errors."""
    checklist = CheckList()
    checklist.register(CAPY.CHECK_LIST)
    checklist.register(COMM.CHECK_LIST)
    return check_loop(checklist, args, numthread)


def check_cata_loop(args, numthread):
    """Check all the files in 'args'. Return a report of the errors."""
    checklist = CheckList()
    checklist.register(CATA.CHECK_LIST)
    checklist.register(COMM.CHECK_LIST)
    return check_loop(checklist, args, numthread)


def check_datg_loop(args, numthread):
    """Check all the files in 'args'. Return a report of the errors."""
    checklist = CheckList()
    checklist.register(DATG.CHECK_LIST)
    checklist.register(COMM.CHECK_LIST)
    return check_loop(checklist, args, numthread)


def check_mater_loop(args, numthread):
    """Check all the files in 'args'. Return a report of the errors."""
    checklist = CheckList()
    checklist.register(MATER.CHECK_LIST)
    checklist.register(COMM.CHECK_LIST)
    return check_loop(checklist, args, numthread)


def check_other_loop(args, numthread):
    """Check all the files in 'args'. Return a report of the errors."""
    checklist = CheckList()
    checklist.register(OTHER.CHECK_LIST)
    checklist.register(COMM.CHECK_LIST)
    return check_loop(checklist, args, numthread)
