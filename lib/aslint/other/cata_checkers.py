# coding=utf-8

"""Checkers for catalo files"""

import aslint.common_checkers as COMM
from aslint.base_checkers import (
    FileContentCat,
    FilenameCat,
    GenericMsg,
    TextMsg,
    TextMsgNotFound,
    checkers_from_context,
    search_msg,
)
from aslint.common_checkers import check_encoding


class AsciiEncoding(FileContentCat, GenericMsg):

    """Invalid encoding, expecting ascii
    'cata' files must be encoded in ascii."""

    id = "C6001"
    fmt = "%(id)s: %(label)s"

    def search(self, txt):
        """Check encoding of txt"""
        result = []
        if not check_encoding(txt, "ascii"):
            result.append("")
        return result


class LicenseNotFound(FileContentCat, COMM.LicenseNotFound):

    """Summary with GPL is required
    The copyright and GPL summary are required. Even if there is an external
    copyright, it must mention www.code-aster.org"""

    id = "C6002"


class EDFCopyright(FileContentCat, COMM.EDFCopyright):

    """EDF R&D Copyright not found
    Even if there is a copyright to another company than EDF R&D, the source
    has been changed for Code_Aster conformance and/or version management.
    Example:

       - Copyright 2008 Company ABC
       - Copyright 2008-2013 EDF R&D www.code-aster.org
    """

    id = "C6003"


class ExternalCopyright(FileContentCat, COMM.ExternalCopyright):

    """Another Copyright than EDF"""

    id = "W6009"


class InvalidCharacter(FileContentCat, COMM.InvalidCharacter):

    r"""Invalid character, unexpected tabulation '\t'
    Tabulation character is not allowed."""
    id = "C6004"


class UnsupportedExtension(FilenameCat, TextMsgNotFound):

    """Unsupported extension
    '.cata' files are not accepted anymore."""

    id = "C6011"
    fmt = "%(id)s: %(label)s"
    search = search_msg("(?P<ext>\.py)$")


CHECK_LIST = checkers_from_context(globals(), TextMsg)
