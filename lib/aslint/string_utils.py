# coding=utf-8

"""Some string utilities"""

import re

from aslint.baseutils import get_encoding


def convert(content, encoding='utf-8', errors='replace'):
    """Convert content using encoding or default encoding if None."""
    if isinstance(content, bytes):
        content = str(content, encoding, errors)
    return content


# Levenshtein algorithm implementation
# original author: Magnus Lie Hetland
# License <http://creativecommons.org/publicdomain/zero/1.0>
def levenshtein(a, b):
    """Calculate the Levenshtein distance between a and b."""
    n, m = len(a), len(b)
    if n > m:
        # Make sure n <= m, to use O(min(n,m)) space
        a, b = b, a
        n, m = m, n
    current = list(range(n + 1))
    for i in range(1, m + 1):
        previous, current = current, [i] + [0] * n
        for j in range(1, n + 1):
            add, delete = previous[j] + 1, current[j - 1] + 1
            change = previous[j - 1]
            if a[j - 1] != b[i - 1]:
                change = change + 1
            current[j] = min(add, delete, change)
    return current[n]


def relative_levenshtein(a, b):
    """Calculate a relative Levenshtein distance between a and b."""
    a = remove_accent(a)
    b = remove_accent(b)
    norm = max((len(a) + len(b)) / 2., 1.)
    return levenshtein(a, b) / norm


def remove_accent(text):
    """Remove accentuated characters"""
    for pattern, repl in (['[àâä]', 'a'], ['[éèêë]', 'e'], ['[îï]', 'i'],
                          ['[ôö]', 'o'], ['[ùûü]', 'u'],
                          ['[ÀÂÄ]', 'A'], ['[ÉÈÊË]', 'E'], ['[ÎÏ]', 'I'],
                          ['[ÔÖ]', 'O'], ['[ÙÛÜ]', 'U']):
        text = re.sub(pattern, repl, text)
    return text


def search_similar(text, values, maxdist=1):
    """Search similar text in values, return the nearest value with relative Levenshtein distance less than 'maxdist'."""
    assert len(values) > 0
    scores = sorted([(relative_levenshtein(text, other), other) for other in values])
    score, found = scores[0]
    if score <= maxdist:
        return found
    return None
