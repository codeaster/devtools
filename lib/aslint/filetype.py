# coding=utf-8

"""This module allows to identify the type of source files"""

import os.path as osp
import re


# the order is important
_types = [
    (
        "build",
        re.compile(
            r"^(?:|\./)(waf|waf_.*|waf\.(engine|main)|waftools/.*"
            r"|wafcfg/.*|.*wscript|configure|makefile|check_.*|\.jenkinsedf/.*"
            r"|\.fprettify\.rc|pyproject\.toml|\.clang-format|gcovr\.cfg|.*\.md"
            r"|\.readthedocs\.yaml"
            r"|\.hg.*|\.git.*"
            r"|extern/.*"
            r"|env\.d/.*)$"
        ),
    ),
    ("test", re.compile(r"^(?:|\./)(astest|tests_data)/")),
    ("hxx", re.compile(r"^(?:|\./)bibcxx/.*\.h$")),
    ("cxx", re.compile(r"\.cxx$")),
    ("h", re.compile(r"^(?:|\./)bibc/.*\.h$")),
    ("c", re.compile(r"\.c$")),
    ("hf", re.compile(r"^(?:|\./)bibfor/.*\.h$")),
    ("for", re.compile(r"\.F90$")),
    ("py", re.compile(r"^(?:|\./)(bibpyt|code_aster|run_aster)/.*\.py(?:|tmpl)$")),
    ("cython", re.compile(r"^(?:|\./)code_aster/.*\.(?:pyx|pxd)$")),
    ("cata", re.compile(r"\.cata$")),  # old catalo/*/*.cata
    ("catalo", re.compile(r"^(?:|\./)catalo/.*\.py$")),  # new catalo/*/*.py
    ("capy", re.compile(r"\.capy$")),
    ("mfront", re.compile(r"\.mfront$")),
    ("datg", re.compile(r"^(?:|\./)datg/")),
    ("mater", re.compile(r"^(?:|\./)materiau/")),
    ("i18n", re.compile(r"^(?:|\./)i18n/")),
    ("data", re.compile(r"^(?:|\./)data/")),
    ("doc", re.compile(r"^(?:|\./)doc/")),
]


def typelist(with_none=False):
    """Return the supported filetypes.
    None means 'unknown type'."""
    return [i for i, dummy in _types] + [None] if with_none else []


def get_file_type(path):
    """Return the type of file using its path in the repository"""
    for typ, reg in _types:
        if reg.search(path):
            return typ
    return None


def filter_dict(dict_changes):
    """Build the list of changes by type from the dict of the status.
    filtered_dict[type] = [(filename, status), ...]"""
    fdict = {}
    for key in typelist(with_none=True):
        fdict[key] = []
    for status, flist in list(dict_changes.items()):
        if status == "msg":
            continue
        for fname in flist:
            typ = get_file_type(fname)
            fdict[typ].append([fname, status])
    return fdict


def filter_names(flist, status=None):
    """Extract the filenames of `flist` of the status or all if it is None."""
    return [fname for fname, sts in flist if status is None or sts in status]


def filter_arguments(args, status="?"):
    """Return the filenames in arguments filtered by type and mark them as
    if they were in the given status."""
    fdict = {}
    for key in typelist(with_none=True):
        fdict[key] = []
    for fname in args:
        typ = get_file_type(fname)
        if not typ:
            ext = osp.splitext(fname)[-1]
            if ext in (".f", ".F", ".F90"):
                typ = "for"
            elif ext == ".c":
                typ = "c"
            elif ext == ".py":
                typ = "py"
            elif ext == ".capy":
                typ = "capy"
            elif ext == ".cata":
                typ = "cata"
            elif ext == ".datg":
                typ = "datg"
            elif ext == ".NOMI":
                typ = "mater"
            elif ext in (".comm", ".mail", ".mmed", ".msup", ".mgib", ".export"):
                typ = "test"
        fdict[typ].append([fname, status])
    return fdict
