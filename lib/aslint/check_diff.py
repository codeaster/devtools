# coding=utf-8

"""This module allows to check the diff of files"""

from functools import partial

from aslint.logger import logger
from aslint.utils import apply_in_parallel
from aslint.base_checkers import (
    CheckList, MsgList, Report,
    check_diff_content,
)
import aslint.common_checkers as COMM
import aslint.other.c_checkers as C
import aslint.other.capy_checkers as CAPY
import aslint.other.cata_checkers as CATA
import aslint.other.datg_checkers as DATG
import aslint.other.mater_checkers as MATER
import aslint.other.other_checkers as OTHER
import aslint.python.python_checkers as PY
import aslint.fortran.static_fortran_checkers as STAT
import aslint.test.test_checkers as TEST


def check_diff_loop(typ, dict_diff, numthread):
    """Loop on the hunk"""
    func_one = partial(check_one_file, checklist=get_checklist(typ))
    items = list(dict_diff.items())
    lrep = apply_in_parallel(func_one, items, numthread)
    report = Report()
    report.merge(lrep)
    return report


def check_one_file(item, checklist):
    """Check one file"""
    fname, diff = item
    fname = 'DIFF@' + fname
    logger.info(fname)
    lmsg = MsgList()
    lmsg.extend(check_diff_content(diff, checklist.on_diff()))
    report = Report()
    report.set(fname, lmsg)
    return report


def get_checklist(typ):
    """Return the checkers for the given 'typ'"""
    checklist = CheckList()
    if typ in ('for', 'hf'):
        checklist.register(STAT.CHECK_LIST)
    elif typ == 'test':
        checklist.register(TEST.CHECK_LIST)
    elif typ in ('c', 'h'):
        checklist.register(C.CHECK_LIST)
    elif typ == 'py':
        checklist.register(PY.CHECK_LIST)
    elif typ == 'cata':
        checklist.register(CATA.CHECK_LIST)
    elif typ == 'capy':
        checklist.register(CAPY.CHECK_LIST)
    elif typ == 'datg':
        checklist.register(DATG.CHECK_LIST)
    elif typ == 'mater':
        checklist.register(MATER.CHECK_LIST)
    elif typ in ('build', None):
        checklist.register(OTHER.CHECK_LIST)
    else:
        pass
    checklist.register(COMM.CHECK_LIST)
    return checklist
