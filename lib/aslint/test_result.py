# coding=utf-8

"""This module allows to report the results of a testcases run"""

import os
import os.path as osp
import re

import junit_xml_output as JUNIT
from aslint.i18n import _
from aslint.logger import logger
from aslint.string_utils import convert


class Diag(object):
    """Enumerator for testcases diagnostic."""
    Ok = 0x01
    Warn = 0x02
    NotTested = 0x04
    NotRun = 0x08
    Nook = 0x10
    Unknown = 0x20
    Error = 0x40

    Success = Ok | Warn
    Failure = Error | Nook | NotTested | Unknown | NotRun

    @classmethod
    def from_text(cls, expression):
        """Evaluate the value from a text"""
        ctxt = cls.__dict__
        try:
            value = eval(expression.strip(), {}, ctxt)
        except Exception:
            raise ValueError("Can not evaluate {0!r}".format(expression))
        return value

    @classmethod
    def use(cls, value):
        """Check that the given value is valid.

        The value is automatically converted from text expression if needed.
        """
        if type(value) is str:
            value = cls.from_text(value)
        if value & (cls.Success | cls.Failure) == 0:
            raise ValueError("Invalid diagnostic value: {0}".format(value))
        return value


class TestResult(object):
    """Store the results of the testcases"""

    fmt_lign = '{test:12s} {diag:21s} {tcpu:10.2f} {tsys:10.2f} {ttot:10.2f}'
    fmt_tot = '-' * 12 + ' ' + '-' * 21 + \
        ' ' + '-' * 10 + ' ' + '-' * 10 + ' '
    default = {'diag': '<F>_ERROR', 'tcpu': 0., 'tsys': 0., 'ttot': 0.}

    def __init__(self, resudir=None, is_success=Diag.Success):
        """Initialization"""
        self._states = [Diag.Ok, Diag.Nook, Diag.Warn, Diag.NotTested,
                        Diag.NotRun, Diag.Error, Diag.Unknown]
        self.is_success = 0
        self.is_error = 0
        self.set_success(is_success)
        self.lists = {}
        for state in self._states:
            self.lists[state] = []
        self.count = None
        self.total = None
        self.diag = {}
        self.resudir = resudir
        self.junit_test = []

    def set_success(self, is_success):
        """Set the states considered as a successfull execution."""
        self.is_success = is_success
        self.is_error = (Diag.Success | Diag.Failure) ^ is_success

    def repr(self):
        """Raw representation for debugging"""
        txt = ["total: {0}".format(self.total)]
        txt.append("count: {0}".format(self.count))
        txt.append("diag: {0}".format(self.diag))
        return os.linesep.join(txt)

    def _add(self, state, ctest, dtime):
        """Add a testcase in the relevant list"""
        self.lists[state].append(
            (ctest, dtime['ttot'], self.fmt_lign.format(**dtime)))

    def _get(self, state, detail):
        """Return the testcases in the given list
        :param detail: tell if it returns all the details or the testcase name
        :type detail: bool
        """
        if not detail:
            out = [i[0] for i in self.lists[state]]
        else:
            out = [i for i in self.lists[state]]
        return out

    def get_diag(self):
        """Return the dict of status"""
        return self.diag and self.diag.copy()

    def get_error(self, detail=False):
        """Return the error list"""
        return self.get_result(self.is_error, detail)

    def get_success(self, detail=False):
        """Return the success list"""
        return self.get_result(self.is_success, detail)

    def get_result(self, state, detail=False):
        """Return the list of tests that match the given state."""
        res = []
        for stl in self._states:
            if state & stl:
                res.extend(self._get(stl, detail))
        return res

    def read_all(self):
        """Read states from the RESULTAT file."""
        diag = {}
        if self.resudir:
            fname = osp.join(self.resudir, "RESULTAT")
            with open(fname, 'r') as fresult:
                for line in fresult:
                    line = line.strip()
                    spl = line.split()
                    if len(spl) < 2:
                        continue
                    diag[spl[0]] = {"diag": spl[1]}
            self.read_diag(diag)

    def read_diag(self, diag):
        """Make the summary of the execution of testcases
        and return the number of errors, the list of testcases that failed
        and a printable summary"""
        self.diag = diag
        self.count = 0
        self.total = total = {'tcpu': 0., 'tsys': 0., 'ttot': 0.}
        if not self.diag:
            return
        sorted_values = sorted(self.diag.items())
        for ctest, res in sorted_values:
            if ctest.startswith('__'):
                continue
            self.count += 1
            diag = res['diag']
            dres = self.default.copy()
            dres.update(res)
            dres['test'] = ctest
            for key in total:
                total[key] += dres[key]

            state = self._convert_diag(diag)
            self._add(state, ctest, dres)

            jstate = "" if state & (Diag.Ok | Diag.Warn) else "failure"
            if jstate and self.resudir:
                if state & Diag.Nook:
                    filename = osp.join(self.resudir, ctest + ".mess")
                    errmsg = get_nook(filename)
                else:
                    filename = osp.join(self.resudir, ctest + ".mess")
                    errmsg = get_err_msg(filename)
                content = diag + os.linesep + errmsg
            else:
                content = diag
            content = convert(content, 'ascii')
            self.junit_test.append(JUNIT.TestCase(ctest, content, jstate))

    @staticmethod
    def _convert_diag(diag):
        if diag == 'OK':
            return Diag.Ok
        elif 'NOOK' in diag:
            return Diag.Nook
        elif 'NO_TEST_RESU' in diag:
            return Diag.NotTested
        elif '<A>' in diag:
            return Diag.Warn
        elif 'NOT_RUN' in diag:
            return Diag.NotRun
        elif diag == '_' or 'ABNORMAL' in diag:
            return Diag.Unknown
        return Diag.Error

    def _write_file(self, basename, lines):
        """Write the results into a file
        Does nothing if the attribute 'resudir' has not been set."""
        if self.resudir:
            fname = osp.join(self.resudir, basename)
            lines.append('')
            try:
                with open(fname, 'w') as fobj:
                    fobj.write(os.linesep.join(lines))
            except IOError:
                logger.warn(_("can not create file {}").format(fname))

    def write_all(self):
        """Write all results into the RESULTAT file"""
        lall = []
        lall.extend(self.get_error(detail=True))
        lall.extend(self.get_success(detail=True))
        lines = [i[2] for i in sorted(lall)]
        self._write_file('RESULTAT', lines)

    def write_error(self):
        """Write the errors into the NOOK file"""
        lerr = []
        lerr.extend(self.get_error(detail=True))
        lines = [i[2] for i in sorted(lerr)]
        self._write_file('NOOK', lines)

    def report_longest(self, nmax=15):
        """Report the longest testcases"""
        lall = []
        lall.extend(self.get_error(detail=True))
        lall.extend(self.get_success(detail=True))
        longest = sorted([i[1:] for i in lall], reverse=True)
        longest = longest[:nmax]
        logger.add_cr()
        logger.title(_("The %d longest testcases are:"), len(longest))
        txt = [i[1] for i in longest]
        logger.info(os.linesep.join(txt))

    def report_xunit(self, testsuite):
        """Write results in xUnit format."""
        if self.resudir:
            junit = JUNIT.JunitXml(testsuite, self.junit_test)
            self._write_file("run_testcases.xml", [junit.dump()])

    def report(self, title, show_longest=True):
        """Report the results"""
        self.write_all()
        self.write_error()
        if show_longest:
            self.report_longest()
        lerr = []
        lerr.extend(self.get_error(detail=True))
        nberr = len(lerr)
        lines = [i[2] for i in sorted(lerr)]
        if lines:
            lines.append(self.fmt_tot)
        self.total['test'] = _("{:4d} tests").format(self.count)
        self.total['diag'] = _("{:4d} errors").format(nberr)
        lines.append(self.fmt_lign.format(**self.total))
        logger.add_cr()
        logger.title(title)
        logger.info(os.linesep.join(lines))
        return nberr


REG_ERRMSG = re.compile(r'(<[ESF]>.*?)!\-\-\-', re.M | re.DOTALL)
REG_EXCEP = re.compile(r'(<EXCEPTION>.*?)!\-\-\-', re.M | re.DOTALL)
REG_SUPERV = re.compile('DEBUT RAPPORT(.*?)FIN RAPPORT', re.M | re.DOTALL)

def get_err_msg(fname):
    """Extract the error message for a 'message' file."""
    if not osp.isfile(fname):
        return 'not found: {0}'.format(fname)
    msg = []
    with open(fname, 'rb') as fobj:
        txt = fobj.read().decode(errors='replace')
    found = REG_ERRMSG.findall(txt) \
        or REG_EXCEP.findall(txt) \
        or REG_SUPERV.findall(txt)
    msg.extend(_clean_msg(found))
    return os.linesep.join(msg)

def _clean_msg(lmsg):
    """Some cleanups in the found messages."""
    out = []
    redeb = re.compile(r'^\s*!\s*', re.M)
    refin = re.compile(r'\s*!\s*$', re.M)
    reefs = re.compile('Cette erreur sera suivie .*', re.M | re.DOTALL)
    reefa = re.compile('Cette erreur est fatale.*', re.M | re.DOTALL)
    for msg in lmsg:
        msg = redeb.sub('', msg)
        msg = refin.sub('', msg)
        msg = reefs.sub('', msg)
        msg = reefa.sub('', msg)
        msg = [line for line in msg.splitlines() if line.strip() != '']
        out.append(os.linesep.join(msg))
    return out

def get_nook(fname):
    """Extract NOOK values from a 'resu' file."""
    if not osp.isfile(fname):
        return 'not found: {0}'.format(fname)
    with open(fname, 'rb') as fobj:
        txt = fobj.read().decode(errors='replace')
    reg_resu = re.compile("^( *(?:REFERENCE|OK|NOOK) .*$)", re.M)
    lines = reg_resu.findall(txt)
    return os.linesep.join(lines)
