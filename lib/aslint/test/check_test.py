# coding=utf-8

"""This module gives convenient functions to check untyped files"""
# not a package because it defines very few checkers

import os
import os.path as osp
from functools import partial

from aslint.i18n import _
from aslint.logger import logger
from aslint.decorators import interrupt_decorator
from aslint.utils import apply_in_sandbox
from aslint.base_checkers import (
    CheckList, Report, MsgList,
    check_filename, check_file_content, check_disabled,
)
from aslint.test.test_checkers import CHECK_LIST
import aslint.common_checkers as COMM


def check_test_loop(args, numthread):
    """Check all the files in 'args'. Return a report of the errors."""
    checklist = CheckList(CHECK_LIST)
    checklist.register(COMM.CHECK_LIST)
    report = Report()
    for filename in args:
        # the global directory is given by the first 'astest' directory found
        dname = osp.dirname(osp.abspath(filename)).split(os.sep)
        if 'astest' not in dname:
            continue
        idx = dname.index('astest')
        path = os.sep.join(dname[:idx])
        logger.info(_("add checking of testcases filenames"))
        lmsg = check_filename(path, checklist.on_dirname())
        report.set(path, lmsg)
        break

    func_one = partial(check_test_files, checklist=checklist)
    lrep = apply_in_sandbox(func_one, args, numthread)
    report.merge(lrep)
    return report


@interrupt_decorator
def check_test_files(fname, checklist):
    """Check a file of a testcase"""
    logger.info(fname)
    lmsg = MsgList()
    lmsg.extend(check_filename(fname, checklist.on_filename()))
    lmsg.extend(check_file_content(fname, checklist.on_content()))
    # filter checkings using the extension
    ext = osp.splitext(fname)[1]
    lmsg.extend(check_filename(fname, checklist.on_filename(ext)))
    lmsg.extend(check_file_content(fname, checklist.on_content(ext)))
    check_disabled(fname, checklist, lmsg)
    report = Report()
    report.set(fname, lmsg)
    return report
