# coding=utf-8

# because of the 'search' method :
# pylint: disable=W0223

"""
This module defines the generic objects to check source files.

There are several classes for each type of checking and derivated classes
for each type of error or warning.
There is a convenient class CheckList to register the messages to check for
a type of checking and source type (it simply avoids ``import *`` or to forget
to register a check, use checkers_from_context too).

A warning or error message is an instance of one of these classes.
All the messages for a source file are stored in a MsgList object (if the
same error is raised twice, there will be two objects of the same class
in the MsgList store).

An instance of the Report class allows to store the MsgList instances for
all the checked source files.

Each message has a unique identifier. This is compulsory to be registered in
a CheckList object and a good practice at all.

The docstring of each message contains a short description on the first line,
and then a longer description that gives the developer some possibilities
to suppress it.

The messages identifiers are as 'Xijnn' and follow the following rules.

The 'X' letter means:
    - 'E' as 'Error': this is an error, we cannot pass over (for example because
      the compiler stops).
    - 'C' as 'Convention': this could be a warning but we consider that it must
      not occur in the source files. So it is as worse as 'E' and can not be
      tolerated.
    - 'W' as 'Warning': this is a warning. Good practices should fix it but in
      some cases, it cannot be removed and has to be tolerated.
    - 'R' as 'Refactoring': this could be use in future to detect or mark code
      that needs refactoring.
    - 'I' as 'Information': just an information, a recommendation (disabled
      warnings are reported as 'I').

The first digit 'i' defines the type of checking:
    - 0: check of fortran source files using a compiler
    - 1: check of fortran source files using textual analysis
    - 2: check of testcase files
    - 3: check of C/C++ source files
    - 4: check of python source files
    - 5: check of data files (material and geometrical data)
    - 6: check of catalog files (command and elements)
    - 9: check of filenames

The second digit 'j' defines the type of error/warning:
    - 0: language/syntax errors
    - 1: arguments passing
    - 2: types
    - 3: declarations
    - 4: standard conformance
    - 5: coding conventions

First two digits for special categories:
    - 88: disabled, hidden messages
    - 99: sentinels messages

The last two digits 'nn' are the number of the message in this subtype.

Example: 'E0102' is an error (E) reported by the compiler (0) concerning the
argument passing (1) and this is a message (02) is this category.

To list all the registered messages (and check the unicity of the ids), run
the unittest `build_messages_doc`:

..  code-block:: sh

    cd $HOME/dev/codeaster/devtools/share/test/test_aslint
    python3 test_aslint_messages.py -doc TestAslint.build_messages_doc
"""

import os
import os.path as osp
import re

from aslint.i18n import _
from aslint.decorators import interrupt_decorator, log_as_important
from aslint.logger import logger, ErrorCode, ERROR, WARN, VERBOSE
from aslint.baseutils import force_list, is_binary


class BaseMsg(object):

    """Class to search warning/error messages."""

    # 'apply_to' can be used to filter messages
    apply_to = None
    # only check if contexts match
    apply_ctxt = None
    id = None
    fmt = "%(id)s: %(label)s"
    # string that tell how to fix a message
    fixme = ""

    def __init__(self, ctxt):
        """Initialization
        ctxt: context of the checking (repository informations)
        match: iterator yielding MatchObjects
        result: result of the analyze stored as a dict.
        The location in the source file of the error can be stored in
        result['line'] and result['col']."""
        self.result = {}
        self.match = None
        self._at_pos = 0
        self.ctxt = ctxt

    @classmethod
    def doc(cls):
        """Return the first line of the docstring"""
        return cls.__doc__.splitlines()[0]

    @classmethod
    def descr(cls, indent=0, init_cr=True):
        """Return the long description of the message"""
        lines = [line.rstrip() for line in cls.__doc__.splitlines()[1:]]
        descr = os.linesep.join(lines).strip()
        if descr:
            descr = [" " * indent + re.sub("^    ", "", line) for line in descr.splitlines()]
            if init_cr:
                descr.insert(0, "")
            descr = os.linesep.join(descr)
        return descr

    def __lt__(self, other):
        return self.id < other.id

    def at_same_position(self):
        """Mark that another message has been raised at the same position
        in the source file.
        This is used to limit the message list to one message per position."""
        self._at_pos += 1

    def search(self, txt):
        """Search for the error/warning message in 'txt' and
        return an interator on MatchObjects"""
        raise NotImplementedError("must be defined in subclass %r" % self)

    def check(self, msglist, txt):
        """Check the given output"""
        raise NotImplementedError("must be defined in subclass %r" % self)

    def analyze(self):
        """Additionnal analysis of the result"""

    def copy(self):
        """Return a copy of the message.
        Useful to report a message several times"""
        new = self.__class__(self.ctxt)
        new.result = self.result.copy()
        new.match = self.match
        return new

    def to_text(self):
        """Format the message"""
        if not self.result:
            return ""
        txt = self.fmt % self.result
        if self._at_pos > 0:
            txt += " (+%d)" % self._at_pos
        return txt

    def __getstate__(self):
        """Remove unpicklable objects (SRE objects)"""
        cnt = self.__dict__.copy()
        try:
            del cnt["match"]
        except KeyError:
            pass
        return cnt

    def has_fixer(self):
        """Tell if a message provides a fixer"""
        return getattr(self, "fixme", None)

    def fixer(self, filenames):
        """Return a string (a command line) that should fix the message
        Arguments: filenames"""
        if type(filenames) in (list, tuple, set):
            filenames = " ".join(filenames)
        args = {"filenames": filenames}
        return self.fixme % args


class CompilMsg(BaseMsg):

    """Class to search warning/error messages in the compiler output.
    The compiler messages are splitted and checked one by one.

    Reserved keywords in regular expressions are:
    - id: message identifier, for example: E0102
        <E>rror/<W>arning/<R>efactoring/<C>onvention
    - label: short label of the message,
    - errw: Error or Warning.
    The first two are attributes of the message too. 'errw' is used
    to mark a message as an error even if was expected as warning.
    """

    def check(self, msglist, txt):
        """Check the given output."""
        self.match = self.search(txt)
        count = 0
        for mdict in self.match:
            if type(mdict) is not dict:
                mdict = mdict.groupdict()
            count += 1
            if mdict.get("id", "?")[0] == "H":
                # hidden message
                continue
            self.result["id"] = self.id
            self.result["label"] = self.doc()
            # if id defines a warning but an error is raised, change to E
            if mdict.get("errw", "").startswith("Err"):
                self.result["id"] = "E" + self.id[1:]
            # warning reported as error, change to C
            elif self.id.startswith("E"):
                self.result["id"] = "C" + self.id[1:]
            self.result.update(mdict)
            self.analyze()
            msglist.append(self.copy())
        return count


class TextMsg(BaseMsg):

    """Class to raise warning or error messages by reading the source code.

    Reserved keywords in regular expressions are:
    - id: message identifier, for example: E0102
        <E>rror/<W>arning/<R>efactoring/<C>onvention
    - label: short label of the message,
    - main: MUST BE used in expression to report the error.
    """

    fmt = "%(id)s: %(label)s at %(line)s,%(col)s: %(main)s"

    def store_msg(self, msglist, match, txt):
        """Store the result with the values of 'match'."""
        self.result["id"] = self.id
        self.result["label"] = self.doc()
        assert hasattr(match, "groupdict"), f"{self.id}: invalid returned type: {match}"
        mdict = match.groupdict()
        start = match.start("main")
        before = txt[:start]
        self.result["line"] = before.count(os.linesep) + 1
        self.result["col"] = len(before) - before.rfind(os.linesep)
        self.result.update(mdict)
        self.analyze()
        msglist.append(self.copy())

    def store_msg_minimal(self, msglist, add_label=""):
        """Store only the id and label of the message."""
        self.result["id"] = self.id
        self.result["label"] = self.doc() + add_label
        self.analyze()
        msglist.append(self.copy())

    def check(self, msglist, txt):
        """Check the given output."""
        self.match = self.search(txt)
        count = 0
        for match in self.match:
            count += 1
            self.store_msg(msglist, match, txt)
        return count


class TextMsgNotFound(TextMsg):

    """Same as TextMsg but message is raised if the regular expression
    is not found."""

    fmt = "%(id)s: %(label)s not found"

    def check(self, msglist, txt):
        """Check the given output."""
        self.match = self.search(txt)
        list_match = [mat for mat in self.match]
        count = len(list_match)
        if count > 0:
            return count
        self.result["id"] = self.id
        self.result["label"] = self.doc()
        self.analyze()
        msglist.append(self.copy())
        return count


class GenericMsg(TextMsg):

    """Very similar to TextMsg but the `search` method returns a list of text
    of errors that are stored using `store_msg_minimal`"""

    fmt = "%(id)s: %(label)s"

    def check(self, msglist, txt):
        """Check the export file"""
        self.match = self.search(txt)
        count = 0
        for match in self.match:
            count += 1
            self.store_msg_minimal(msglist, match)
        return count


class DisableMsg(BaseMsg):

    """This kind of message allows to disable an error or warning message."""

    fmt = "%(id)s: %(label)s"

    def check(self, msglist, txt):
        """Check the given output."""
        self.match = self.search(txt)
        count = 0
        for match in self.match:
            count += 1
            for idm in match.group("disable").split(","):
                idm = idm.strip()
                self.result["id"] = self.id
                obj = msglist.disable(idm)
                if not obj:
                    continue
                descr = obj.doc()
                self.result["label"] = "%s: %s %s" % (self.doc(), idm, descr)
                msglist.append(self.copy())
        return count


class DisableMsgError(DisableMsg):

    """This message raises an error if a disabled message is not raised."""

    def check(self, msglist, txt):
        """Check the given output."""
        self.match = self.search(txt)
        count = 0
        for match in self.match:
            count += 1
            for idm in match.group("disable").split(","):
                idm = idm.strip()
                self.result["id"] = self.id
                obj = msglist.disable(idm)
                if obj:
                    continue
                # message not raised
                self.result["id"] = self.id
                self.result["label"] = "%s: %s" % (self.doc(), idm)
                msglist.append(self.copy())
        return count


class HiddenMsg(DisableMsg):

    """Hide entirely some messages"""

    hidden = ""

    def search(self, dummy):
        """Disable some messages"""
        return self.hidden.split(",")

    def check(self, msglist, txt):
        """Check the given output."""
        self.match = self.search(txt)
        count = 1
        for idm in self.match:
            msglist.disable(idm, remove=True)
        return count


class CheckContext(object):

    """Container to store contextual informations

    Arguments:
        reponame: repository name
        branch: current branch name
        opts: allow to disable some message
    """

    FormatOpt = 1
    UseCachedModule = 2

    __slots__ = ("reponame", "branch", "brchk", "opts")

    def __init__(self, reponame=None, branch=None, opts=0):
        """Initialization"""
        self.branch = None
        self.reponame = reponame
        self.opts = opts
        if branch is None:
            self.brchk = None
        elif isinstance(branch, (list, tuple)):
            self.brchk = lambda br: br in branch
        else:
            try:
                expr = re.compile(branch)
            except Exception as exc:
                raise ValueError("invalid branch attribute: {0}".format(branch))
            self.brchk = lambda br: expr.search(br) is not None

    def init_global(self):
        """Initialize the global context to use all checkings.

        Returns:
            CheckContext: the object itself.
        """
        self.enable(self.FormatOpt)
        self.enable(self.UseCachedModule)
        return self

    def enable(self, option):
        """Enable an option."""
        self.opts = self.opts | option

    def disable(self, option):
        """Disable an option."""
        self.opts = (self.opts | option) ^ option

    def match_global(self):
        """Tell if the values of the global context are supported
        Used for `BaseMsg.ctxt`. Attributes must be of type <list>."""
        ctg = checkcontext
        if self.reponame and ctg.reponame and ctg.reponame not in self.reponame:
            return False
        if self.opts and not self.opts & ctg.opts:
            return False
        if not self.brchk or not ctg.branch:
            return True
        if self.brchk(ctg.branch):
            return True
        # 'main' and 'default'
        if ctg.branch in ("main", "default") and (self.brchk("main") or self.brchk("default")):
            return True
        return False

    def is_enabled(self, option):
        """bool: Tell if an option is enabled"""
        return self.opts & option != 0

    def __repr__(self):
        """Simple representation"""
        return ", ".join(["%s=%s" % (i, getattr(self, i)) for i in self.__slots__])


# global instance
checkcontext = CheckContext().init_global()


def check_ids(msg):
    """Check that all ids are different and return the message in the
    order of checkings"""
    id_msg = [int(obj.id[1:]) for obj in msg]
    uniq = set(id_msg)
    if len(id_msg) != len(uniq):
        for i in uniq:
            id_msg.remove(i)
        assert len(id_msg) == 0, "ID of message must be unique: %s" % id_msg


def check_categories(msglist):
    """Check that all messages inherit of a Category"""
    nocat = [msg for msg in msglist if not issubclass(msg, (Category, HiddenMsg))]
    assert len(nocat) == 0, "messages must belong to (be a subclass of) a category: %s" % nocat


def checkers_from_context(dict_objects, typm):
    """Build the list of all known messages"""
    msg = [
        obj
        for obj in list(dict_objects.values())
        if type(obj) is type and issubclass(obj, typm) and obj.id
    ]
    return msg


def search_msg(expr, ignore_case=True):
    """Convenient wrapper to search 'expr' with relevant re flags.

    Arguments:
        expr (str): Regular expression to search.
        ignore_case (bool, optional): If *True* (the default) a case-insensitive
            search is performed.
    """
    flag = re.M
    if ignore_case:
        flag |= re.I
    return re.compile(expr, flag).finditer


# categories of checkers to filter checklist


class Category(object):

    """Base class for categories of checkers"""


class FileContentCat(Category):

    """Define a type of messages that check content of a file"""


class DiffCat(Category):

    """Define a type of messages that check content of a diff of a file"""


class FilenameCat(Category):

    """Define a type of messages that check a filename"""


class DirnameCat(Category):

    """Define a type of messages that check a directory"""


class CompilOutputCat(Category):

    """Define a type of messages that check content of a compilation output"""


class FortranCodeCat(Category):

    """Define a type of messages that check a fortran code object"""


class CheckList(object):

    """List of message checkers"""

    def __init__(self, linit=None):
        """Initialization"""
        self.values = []
        if linit:
            self.register(linit)

    def __iter__(self):
        """Iterate on message"""
        for item in self.values:
            yield item

    def _sort(self):
        """Sort messages by id"""
        id_msg = [(int(obj.id[1:]), obj) for obj in self.values]
        id_msg.sort()
        self.values = [val[1] for val in id_msg]

    def register(self, checklist):
        """Register a list of checkers"""
        self.values.extend(checklist)
        check_ids(self.values)
        check_categories(self.values)
        self._sort()

    def get_type(self, types):
        """Return a sub-Checklist of the given types"""
        types = tuple(force_list(types) + [HiddenMsg])
        return CheckList([msg for msg in self if issubclass(msg, types)])

    def on_content(self, apply_to=None):
        """Return the messages that check content of files"""
        return _filter(self.get_type(FileContentCat), apply_to)

    def on_compiloutput(self, apply_to=None):
        """Return the messages that check ouput of compilation of files"""
        return _filter(self.get_type(CompilOutputCat), apply_to)

    def on_filename(self, apply_to=None):
        """Return the messages that check filenames"""
        return _filter(self.get_type(FilenameCat), apply_to)

    def on_dirname(self, apply_to=None):
        """Return the messages that check directories names"""
        return _filter(self.get_type(DirnameCat), apply_to)

    def on_fortran_code(self, apply_to=None):
        """Return the messages that check fortran code objects"""
        return _filter(self.get_type(FortranCodeCat), apply_to)

    def on_diff(self, apply_to=None):
        """Return the messages that check diff content"""
        lst = [
            msg
            for msg in _filter(self.get_type(DiffCat), apply_to)
            if not issubclass(msg, TextMsgNotFound)
        ]
        return CheckList(lst)


def _filter(checklist, apply_to):
    """Keep only the messages with no 'apply_to' attr (if apply_to=None) or
    if 'apply_to' is in their 'apply_to' attr"""
    lst = []
    for msg in checklist:
        ctxt = msg.apply_ctxt
        if ctxt and not ctxt.match_global():
            continue
        if apply_to == msg.apply_to is None or (msg.apply_to and apply_to in msg.apply_to):
            lst.append(msg)
    return CheckList(lst)


class MsgList(list):

    """Class to store the report of all the checkers applied
    to a source file."""

    def disable(self, msgid, remove=False):
        """Disable a 'msgid': change to 'I'. Remove it if 'remove' is True"""
        found = None
        for i in range(len(self) - 1, -1, -1):
            # same ids or (not error and same numbers)
            if self[i].id == msgid or (self[i].result["id"] != "E" and self[i].id[1:] == msgid[1:]):
                found = self[i]
                self[i].result["id"] = "I" + self[i].result["id"][1:]
                if remove:
                    del self[i]
        return found

    def to_text(self, prefix="", reverse=False, unique=False):
        """Return the message formatted."""
        msort = self.sort_by_position(reverse, unique)
        txt = [(prefix + msg.to_text()).strip() for msg in msort]
        return os.linesep.join(txt)

    def sort_by_position(self, reverse=False, unique=False):
        """Sort by position. Return a new MsgList."""

        def _get_pos(msg, default):
            """Return a couple (#line, #col)"""
            line = msg.result.get("line") or default
            col = msg.result.get("col") or default
            return int(line), int(col), msg

        default = 0 if reverse else 99999
        tmp = [_get_pos(msg, default) for msg in self]
        tmp.sort(reverse=reverse)
        if unique:
            inter = []
            prev = -1, -1
            for item in tmp:
                pos = item[:2]
                if pos != prev:
                    inter.append(item)
                else:
                    last = inter[-1][-1]
                    last.at_same_position()
                prev = pos
            tmp = inter
        return MsgList([val[-1] for val in tmp])


class Report(dict):

    """Store all the MsgList objects"""

    def __init__(self, context=None):
        """Initialize additionnal attributes"""
        dict.__init__(self)
        self.context = context

    def set(self, key, msglist):
        """Store the `msglist` result under the given `key`"""
        self[key] = msglist

    def merge(self, other):
        """Merge one or more other Report objects to the current one"""
        if type(other) not in (list, tuple):
            other = [other]
        for irep in other:
            if not irep:
                continue
            for key, msglist in list(irep.items()):
                msg = self.get(key, MsgList())
                msg.extend(msglist)
                self[key] = msg

    def remove(self, msgid):
        """Remove messages having the given id
        NB: Change the MsgList objects in place"""
        for dummy, msglist in list(self.items()):
            msglist.disable(msgid, remove=True)

    def with_msg_iter(self, not_empty=False):
        """Iterator on items containing at least one message"""
        for fname, msglist in list(self.items()):
            if not_empty and len(msglist) == 0:
                continue
            yield fname, msglist

    def with_msg_iter_values(self, not_empty=False):
        """Iterator on values containing at least one message"""
        for item in self.with_msg_iter(not_empty):
            yield item[1]

    def to_text(self):
        """Report all errors"""
        values = []
        for fname, msglist in self.with_msg_iter(not_empty=True):
            values.append([fname, msglist.to_text(fname + ":", unique=False)])
        values.sort()
        txt = []
        if self.context:
            txt.append(self.context.to_text())
        txt.extend([fmt for fname, fmt in values])
        return os.linesep.join(txt)

    def report_by_type(self):
        """Group message by id
        Return dicts of number of occurrences and short description."""
        bytyp = {}
        descr = {}
        count = 0
        for msglist in self.with_msg_iter_values():
            count += 1
            for msg in msglist:
                # may have been reported as E or C (see CompilMsg.check)
                eid = msg.result.get("id", msg.id)
                bytyp[eid] = bytyp.get(eid, 0) + 1
                descr[eid] = msg.doc()
        return count, bytyp, descr

    def hasError(self, errorid=None, regexp=None):
        """Tell if the report contains an error message.
        If `errorid` is provided, tell if an error with this id was raised.
        If `regexp` is provided, tell if an error with this id and with a
        message that matches this regular expression was raised.

        Arguments:
            errorid (str, optional): Id of the expected message.
            regexp (str|re.Pattern, optional): Regular expression that
                should be checked by the message.
        """
        if regexp and isinstance(regexp, str):
            regexp = re.compile(regexp)
        for msglist in self.with_msg_iter_values():
            for msg in msglist:
                # may have been reported as E or C (see CompilMsg.check)
                eid = msg.result.get("id", msg.id)
                if not errorid:
                    if eid[0] in ("E", "C"):
                        return True
                elif eid == errorid:
                    if not regexp or regexp.search(msg.result.get("msg", "")):
                        return True
        return False

    @log_as_important
    def report_summary(self):
        """Report statistics by type of error"""
        logger.title(_("report summary:"))
        count, bytyp, descr = self.report_by_type()
        ltyp = list(bytyp.items())
        ltyp.sort()
        ctyp = {}.fromkeys([WARN, ERROR, VERBOSE], 0)
        errcode = ErrorCode()
        for eid, nberr in ltyp:
            args = "%s: %5d %s", eid, nberr, descr[eid]
            if eid[0] in ("E", "C"):
                err_i = ERROR
            elif eid[0] in ("I", "R"):
                err_i = VERBOSE
            else:
                err_i = WARN
            errcode.add(err_i)
            ctyp[err_i] += nberr
            logger.log(err_i, *args)
        args = _("summary: {0:d} errors, {1:d} warnings in {2:d} " "files/directories").format(
            ctyp[ERROR], ctyp[WARN], count
        )
        if sum([ctyp[ERROR], ctyp[WARN]]) == 0:
            logger.verbose(args)
        else:
            logger.log_current(args)
        if ctyp[ERROR] > 0:
            self.show_fixer()
        return errcode

    def getFixerCommands(self):
        """Return a dict of the commands fixer"""
        dcmd = {}
        dmsg = {}
        darg = {}
        for fname, msglist in self.with_msg_iter(not_empty=True):
            for msg in msglist:
                # may have been reported as E or C (see CompilMsg.check)
                eid = msg.id
                if msg.has_fixer():
                    dmsg[eid] = msg
                    darg[eid] = darg.get(eid, set())
                    darg[eid].add(fname)
        for eid in list(dmsg.keys()):
            dcmd[eid] = dmsg[eid].fixer(darg[eid])
        return dcmd

    def show_fixer(self):
        """Show commands to fix some errors if available"""
        title = _(
            "following commands should help you to fix some errors "
            "(try to copy+paste these commands in your terminal):"
        )
        shown = False
        # search messages with fixer
        dcmd = self.getFixerCommands()
        for eid in sorted(dcmd.keys()):
            if not shown:
                logger.title(title)
                shown = True
            logger.info(_("# for {0}:").format(eid))
            logger.info(dcmd[eid])
        if shown:
            logger.title(_("end of commands"))


class Context(object):

    """Store contextual values attached to a Report"""

    __slots__ = ("repository", "submission", "username")

    def __init__(self):
        """Initialization"""
        self.repository = None
        self.submission = None
        self.username = None

    def to_text(self):
        """Representation of the context"""
        txt = ""
        if getattr(self, "submission", None):
            txt += _("for integration")
        if getattr(self, "repository", None):
            if txt:
                txt += " "
            txt += _("in repository '%s'") % self.repository
        if getattr(self, "username", None):
            if txt:
                txt += " "
            txt += _("for '%s'") % self.username
        if txt:
            txt = _("audit report") + " " + txt + ":"
        return txt


GRAV = {"0": -99, "I": 0, "R": 1, "W": 2, "C": 3, "E": 4, "Z": 99}


@interrupt_decorator
def check_disabled(fname, checklist, msglist):
    """Apply 'disabling' messages
    Change msglist in place."""
    if not osp.isfile(fname) or is_binary(fname):
        return
    with open(fname, "r") as fobj:
        txt = fobj.read()
    disabling = checklist.on_content().get_type(DisableMsg)
    for checker in disabling:
        logger.debug("check %s", checker)
        chk = checker(checkcontext)
        chk.check(msglist, txt)


@interrupt_decorator
def call_checkers(obj, checklist, typm=TextMsg, stopon="Z"):
    """Check the given object by calling a list of checkers
    `obj` may be a filename, a file content, the output of the compilation...
    Only messages of type `typm` are checked from `checklist`.
    The loop on `typm` is interrupted on the first message found that has a
    gravity greater than `stopon`.
    '0': stops on first message, 'Z' never stops.
    Return the list of messages raised."""
    msglist = MsgList()
    logger.debug("check object:\n%s", obj)
    for ordered_type in (typm, HiddenMsg):
        for checker in checklist.get_type(ordered_type):
            logger.debug("check %s", checker)
            chk = checker(checkcontext)
            grv = GRAV[chk.result.get("id", chk.id)[0]]
            if chk.check(msglist, obj) and grv >= GRAV[stopon]:
                break
    return msglist


def check_filename(fname, checklist, typm=TextMsg, stopon="Z"):
    """Check the name of a source file"""
    # used to check directories names too
    if not osp.exists(fname):
        return []
    return call_checkers(fname, checklist, typm, stopon)


def check_file_content(fname, checklist, typm=TextMsg, stopon="Z"):
    """Check the content of a source file"""
    if not osp.isfile(fname) or is_binary(fname):
        return []
    with open(fname, "r") as fobj:
        txt = fobj.read()
    return call_checkers(txt, checklist, typm, stopon)


def check_fortran_code(fname, checklist, typm=TextMsg, stopon="Z"):
    """Check the 'fortran_code' object of a source file"""
    import aslint.fortran.free.fortran_code as FC

    if not osp.isfile(fname) or is_binary(fname):
        return []
    with open(fname, "r") as fobj:
        txt = fobj.read()
    obj = FC.source(txt)
    return call_checkers(obj, checklist, typm, stopon)


def check_diff_content(diff, checklist, typm=TextMsg, stopon="Z"):
    """Check the content of a diff file"""
    # keep only new lines
    lines = [i[1:] for i in diff.splitlines() if i.startswith("+") and not i.startswith("+++")]
    txt = os.linesep.join(lines)
    return call_checkers(txt, checklist, typm, stopon)


# wrappers for multiprocessing + info


def check_filename_verbose(fname, *args, **kwargs):
    """wrapper on check_filename"""
    logger.info(fname)
    return check_filename(fname, *args, **kwargs)


def check_file_content_verbose(fname, *args, **kwargs):
    """wrapper on check_file_content"""
    logger.info(fname)
    return check_file_content(fname, *args, **kwargs)
