# coding=utf-8

"""Some basic utilities"""
# no import of aslint here

import locale
import os
import os.path as osp
from collections import UserDict


def force_list(obj):
    """Force to return a list"""
    if type(obj) not in (list, tuple):
        obj = [obj, ]
    return obj


def get_absolute_path(path):
    """Return the absolute path of `path`, eventually by following the
    symbolic links."""
    if osp.islink(path):
        path = osp.realpath(path)
    res = osp.normpath(osp.abspath(path))
    return res


def get_absolute_dirname(path):
    """Return the absolute directory name of `path`, eventually by following
    the symbolic links."""
    res = osp.normpath(osp.join(get_absolute_path(path), os.pardir))
    return res

_encoding = None


def get_encoding():
    """Return local encoding"""
    global _encoding
    if _encoding is None:
        try:
            _encoding = locale.getpreferredencoding() or 'ascii'
        except locale.Error:
            _encoding = 'ascii'
    return _encoding


def is_binary(fname):
    """Tell if a file appears to be binary.
    All non UTF-8 files are considered binary.
    """
    with open(fname, 'rb') as fobj:
        try:
            fobj.read().decode('utf-8')
        except UnicodeDecodeError:
            return True
    return False


class NeverFailedDict(UserDict):
    """A dict that never raise KeyError
    Return a new NeverFailedDict if the key does not exist."""

    def __getitem__(self, key):
        """x.__getitem__(y) <==> x[y]"""
        try:
            return UserDict.__getitem__(self, key)
        except KeyError:
            return NeverFailedDict()


def on_calibre(vers=""):
    """Tell if the OS has a Calibre installation"""
    name = "Calibre {0}".format(vers)
    with open('/etc/issue', 'r') as fobj:
        return name.strip() in fobj.read()
