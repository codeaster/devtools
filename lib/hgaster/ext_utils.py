# coding=utf-8

"""Utilities for Code_Aster hooks and extension."""

import base64
import getpass
import os
import os.path as osp
import re
import smtplib
import tempfile
import time
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from subprocess import Popen, call

from aslint.config import ASCFG
from aslint.i18n import _
from aslint.logger import blue, cyan, green, grey, logger, magenta, red
from aslint.string_utils import convert
from repository_api import UI, get_description, get_list_of_changed_files, get_rev_log, hgcmd

RE_ISSUE = r"(?P<issue>(?P<type>\#|issue|Issue|bb|BB)(?: ?)(?P<number>[0-9]{4,6}))"
BUGTRACKERS = (None, "rex", "bitbucket")
# closed issues must be enclosed by '[]': [#1234]


def is_admin(user):
    """Tell if 'user' is admin."""
    return re.search(r'^["\']?(code_aster|mergerobot)', user, re.I) is not None


def shortrev_s(rev):
    """Return a short representation of a revision passed as string"""
    return rev[:16]


def shortrev(rev):
    """Return a short representation of a revision"""
    return shortrev_s(rev.hex())


def newui():
    """Return a ui instance that returns plain output
    (without color for example)"""
    return UI.ui()


def check_for_updates(path, **opts):
    """Check for available updates for the given repository
    Returns 0 if there are incoming changes, 1 otherwise, 4 in case of error."""
    logger.debug(_("checking for updates for repository '%s'"), path)
    opts["bundle"] = False
    opts["force"] = False
    try:
        source = opts.pop("source")
    except KeyError:
        source = "default"
    try:
        retcode = hgcmd("incoming", path, source, **opts)[0]
    except:
        retcode = 4
    return retcode


def get_changed_files(repo, revs=None, files=(), cached=True):
    """Return the list of files changed between two revisions"""
    # FIXME: choose common chars for AMDR
    output = get_list_of_changed_files(repo, revs, files, cached=True)
    lines = output.splitlines()
    dchg = {}
    add, mod, rmv, mis, unk = [], [], [], [], []
    for occ in lines:
        try:
            fields = occ.split()
            assert len(fields) == 2, occ
            chg, fname = fields
            if chg in "DR":
                rmv.append(fname)
            elif chg == "A":
                add.append(fname)
            elif chg == "M":
                mod.append(fname)
            elif chg == "!":
                mis.append(fname)
            elif chg == "?":
                unk.append(fname)
            else:
                # ignore clean & ignored files
                pass
        except IndexError:
            raise ValueError("line: %r" % occ)
    dchg["A"] = add
    dchg["M"] = mod
    dchg["R"] = rmv
    dchg["!"] = mis
    dchg["?"] = unk
    msg = []
    if len(add):
        msg.append(_("%d files added") % len(add))
        logger.debug(_("added files:"))
        logger.debug(green(os.linesep.join(add)))
    if len(mod):
        msg.append(_("%d files changed") % len(mod))
        logger.debug(_("changed files:"))
        logger.debug(magenta(os.linesep.join(mod)))
    if len(rmv):
        msg.append(_("%d files removed") % len(rmv))
        logger.debug(_("removed files:"))
        logger.debug(red(os.linesep.join(rmv)))
    if len(mis):
        msg.append(_("%d files missing") % len(mis))
        logger.debug(_("missing files:"))
        logger.debug(red(os.linesep.join(mis)))
    if len(unk):
        msg.append(_("%d files not tracked") % len(unk))
        logger.debug(_("unknown files:"))
        logger.debug(red(os.linesep.join(unk)))
    dchg["msg"] = ", ".join(msg)
    return dchg


def get_diff(repo, rev, files):
    """Return the diff of files between two revisions"""
    args = list(files)
    args.insert(0, "--rev={0}".format(rev[0]))
    args.insert(1, "--rev={0}".format(rev[1]))
    args.insert(0, "-b")
    args.insert(1, "--exclude=waf.engine")  # because not reported as binary
    iret, output = hgcmd("diff", repo, *args)
    if iret not in (0, None):
        logger.info(_("'hg diff' exists with code %s"), iret)
    return output


def get_hgpath(repo, dest):
    """Return the full url of a destination"""
    return hgcmd("paths", repo, dest)[1]


def get_reference_ancestor(repopath, child, branch, source):
    """Return the node of the last reference ancestor of a revision in a
    branch.

    Arguments:
        child (str): Revision of which the ancestor is searched.
        branch (str): Branch name in which the ancestor is searched.
        source (str): Repository path that publish reference revisions.

    Returns:
        str: Revision identifier.
    """
    rset = (
        "last(branch('{1}') and not outgoing('{2}') and not secret() " "and ancestors({0}))"
    ).format(child, branch, source)
    return get_rev_log(repopath, rset)


def get_first_new_rev(repopath, child, source):
    """Return the first ancestor of *child* that isn't available on *source*.

    Arguments:
        child (str): Revision of which the ancestor is searched.
        source (str): Repository path that publish reference revisions.

    Returns:
        str: Revision identifier.
    """
    # FIXME remaining log with hg revsets
    rset = "first(ancestors('{0}') and outgoing('{1}'))".format(child, source)
    return get_rev_log(repopath, rset)


def get_solved_issues(repo, rev1, rev2, with_output=False):
    """Return 3 lists: issues solved between the two revisions in all the
    bugtrackers, in rex only, in bitbucket only."""
    output = "\n".join(get_description(repo, rev1, rev2))
    ret = []
    for track in BUGTRACKERS:
        ret.append(get_issues_from_descr(output, filter=track))
    if with_output:
        ret.append(output)
    return ret


def get_issues_from_descr(text, filter=None, only=False, bracket=True):
    """Extract solved issues numbers from a text.

    Arguments:
        text (str): Text of the issues description.
        filter (str): One of 'rex', 'bitbucket' or None. Limits the
            list of issues to the given bugtracker (default: None).
        only (bool): If *True*, 'text' must only contain issue numbers,
            otherwise it raises *ValueError* (default: False).
        bracket (bool): If *True*, only issues numbers found between brackets
            ('[]') are returned, otherwise they are ignored (default: True).

    Returns;
        list[int]: List of issues numbers in the same order as they are found.
    """
    if bracket:
        brack = re.compile(r"\[(.*?)\]", re.M)
        search_in = brack.findall(text)
    else:
        search_in = [text]
    expr = re.compile(RE_ISSUE, re.M)
    clean = re.compile("[, ]+")
    issues = []
    for enclosed in search_in:
        buff = enclosed
        for mat in expr.finditer(enclosed):
            if (
                filter is None
                or (filter == "rex" and mat.group("type") in ("#", "issue"))
                or (filter == "bitbucket" and mat.group("type") in ("bb", "BB"))
            ):
                num = int(mat.group("number"))
                if num not in issues:
                    issues.append(num)
                buff = buff.replace(mat.group("issue"), "")
        buff = clean.sub("", buff)
        if only and buff:
            raise ValueError("unexpected issue number: {0}".format(buff))
    return issues


def split_strip(string, sep):
    """Split a string and strip each element"""
    if not string:
        return []
    return [elt.strip() for elt in string.split(sep)]


HTML = """
<html>
<head></head>
<body style="font-size:10pt;color:#000000;background-color:#FFFFFF;font-family:Verdana,Geneva,sans-serif;">
{text}
<p><br></p>
<div id="Signature">
<div name="divtagdefaultwrapper" style="font-family:Calibri,Arial,Helvetica,sans-serif; margin:0">
<div style="margin:0 0 0 0.75pt">
<font face="Verdana, Geneva, sans-serif" color="#ED7D31">&nbsp; <b>Equipe code_aster</b></font><br>
<font face="Verdana, Geneva, sans-serif" color="#0070C0">&nbsp; EDF &#8211; R&amp;D / &Eacute;lectrotechnique et M&eacute;canique des Structures<br>
&nbsp; Outils de simulation en m&eacute;canique et dynamique rapide<br>
&nbsp; EDF Lab Paris-Saclay<br>
</font><br>
</div>
</div>
</div>
<p></p>

</body>
</html>
"""

NOTIF = "ZWFhZTQzZTguZWRmb25saW5lLm9ubWljcm9zb2Z0LmNvbUBlbWVhLnRlYW1zLm1z"
ADMIN = "MmI3NmFmN2IuZWRmb25saW5lLm9ubWljcm9zb2Z0LmNvbUBlbWVhLnRlYW1zLm1z"


def sendmail(
    recipients,
    subject,
    text=None,
    fromaddr=None,
    footer=True,
    bcc=False,
    add_html=False,
    attach=None,
    dry_run=False,
):
    """Send an email.

    Arguments:
        recipients (list[str]): List of the recipients as a valid list of email
            addresses or a role of the [aster]/notify section.
        text (str, optional): Text of the message. If it is None, the content of
            the logger is sent.
        fromaddr (str, optional): Use the address as sender.
        footer (bool, optional): If *True* (default), add a mention
            "from host...".
        bcc (False, optional): If *True*, hide the destination addresses.
        attach (list[str], optional): List of files to be attached.
        dry_run (bool, optional): If *True*, do not send the message.
    """
    if logger.debug_enabled():
        dry_run = True
    if type(recipients) not in (list, tuple):
        recipients = recipients.split(",")
    logger.debug(_("mail recipients: %r"), recipients)
    to_teams = []
    toaddr = []
    for addr in recipients:
        if not addr:
            continue
        elif "@" in addr:
            addr = [addr]
        elif addr == "user":
            addr = split_strip(ASCFG.get("notify." + addr, ""), ",")
        else:
            if addr in ("admins", "resutest"):
                # does not work, to be tested!
                # toaddr.append(base64.b64decode(ADMIN).decode("utf-8"))
                toaddr.append(ASCFG.get("notify." + addr))
                to_teams.append("--admin")
            elif addr in ("codeaster",):
                # toaddr.append(base64.b64decode(NOTIF).decode("utf-8"))
                to_teams.append("--notifications")
            continue
        for new in addr:
            if not new in toaddr:
                toaddr.append(new)
    if not (toaddr or to_teams):
        if recipients:
            logger.warn(
                _(
                    "no destination address, %s is neither a valid address "
                    "nor defined in the [aster] section"
                ),
                recipients,
            )
        return toaddr
    else:
        admsg = toaddr
        if len(toaddr) == 1:
            admsg = toaddr[0]
        logger.info(_("sending email to %s"), admsg)
    if dry_run:
        logger.info(_("(DRY RUN) not sent"))
        return toaddr
    host = os.uname()[1]
    # shown 'from' address
    fromaddr = fromaddr or "%s@%s" % (getpass.getuser(), host)
    # user connected to the mta (may be filtered, localhost should be authorized)
    mailhost = ASCFG.get("notify.mailhost")
    port = ASCFG.get("notify.port")
    kwargs = {}
    if mailhost:
        kwargs["host"] = mailhost
    if port:
        kwargs["port"] = port
    msg = MIMEMultipart("alternative")
    msg["Subject"] = subject.format(host=host, date=time.strftime("%Y-%m-%d"))
    msg["From"] = fromaddr
    if not bcc:
        msg["To"] = ", ".join(toaddr)
    else:
        msg["To"] = fromaddr

    if text is None:
        text = logger.get_text()
    if footer:
        text = text + os.linesep * 2 + (_("sent from %s") % host)
    part1 = MIMEText(text, "plain", "utf-8")
    msg.attach(part1)
    if add_html:
        html = HTML.format(text="<p>" + text.replace("\n\n", "</p>\n<p>") + "</p>")
        part2 = MIMEText(html, "html", "utf-8")
        msg.attach(part2)

    if attach:
        attach = attach if isinstance(attach, list) else [attach]
        for fname in attach:
            with open(fname, "rb") as fobj:
                content = fobj.read()
            attfile = MIMEApplication(content)
            attfile.add_header("content-disposition", "attach", filename=osp.basename(fname))
            msg.attach(attfile)

    if toaddr:
        if ASCFG.get("notify.method") == "curl":
            send_with_curl(msg, toaddr)
        elif ASCFG.get("notify.method") == "smtplib":
            smtp = smtplib.SMTP(**kwargs)
            smtp.connect()
            smtp.sendmail("no-reply@edf.fr", toaddr, msg.as_string())
            smtp.quit()
        elif ASCFG.get("notify.method") == "mailutils":
            sendmail_shell(msg, text)

    to_teams = set(to_teams)
    for option in to_teams:
        send_teams(msg["From"], msg["Subject"], text, option)
    return toaddr


def send_teams(fromaddr, subject, text, option=None):
    """Send 'text' to a Teams channel"""
    tmpname = tempfile.NamedTemporaryFile().name
    with open(tmpname, "w") as tmpf:
        tmpf.write(convert(text))
    cmd = [
        osp.join(ASCFG.get("devtools_root"), "bin", "send_msteams"),
        "--from",
        fromaddr,
        "--subject",
        subject,
    ]
    if option:
        cmd.append(option)
    cmd.append(tmpname)
    try:
        call(cmd)
    finally:
        os.remove(tmpname)


def send_with_curl(msg, toaddr):
    """Send a message using curl.

    Arguments:
        msg (MIMEMultipart): message
        toaddr (list[str]): list of email addresses
    """
    tmpname = tempfile.NamedTemporaryFile().name
    with open(tmpname, "w") as tmpf:
        tmpf.write(msg.as_string())
    cmd = [
        "curl",
        "--url",
        "smtp://" + ASCFG.get("notify.mailhost"),
        "--mail-from",
        '"no-reply@edf.fr"',
    ]
    cmd.extend(['--mail-rcpt "{}"'.format(i) for i in toaddr])
    cmd.extend(["--upload-file", tmpname])
    try:
        # print(" ".join(cmd))
        # os.system("cat " + tmpname)
        call(" ".join(cmd), shell=True)
    finally:
        os.remove(tmpname)


def sendmail_shell(msg, text):
    """Send a mail using mailutils shell commands"""
    try:
        tmpname = tempfile.NamedTemporaryFile().name
        with open(tmpname, "w") as tmpf:
            tmpf.write(convert(text))
        with open(tmpname, "r") as pipe:
            cmd = ["mail", "-s", convert(msg["Subject"]), msg["To"]]
            logger.debug("trying: %s", cmd)
            pmail = Popen(cmd, stdin=pipe)
        pmail.wait()
        logger.debug(_("mail sent using the shell command"))
    except Exception as exc:
        logger.warn(_("can not send the email using mailutils:\nException: %s"), str(exc))


def colorize_status(status):
    """Colorize the status"""
    ident = lambda x: x
    dfunc = {"M": magenta, "?": blue, "!": cyan, "A": green, "R": red, "I": grey}
    new = []
    for line in status.splitlines():
        mat = re.search(r"^([MAR\?!]) ", line)
        if mat:
            line = dfunc.get(mat.group(1), ident)(line)
        new.append(line)
    return os.linesep.join(new)


_author_regexp = re.compile(" *(.*?) *< *(.*?) *> *")


def decode_author(author):
    """Extract author name and email"""
    mat = _author_regexp.search(author)
    if not mat:
        logger.warn(_("invalid author field: {}").format(author))
        return author, ""
    return mat.groups()
