#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""This module defines the engine to use ``hg bisect`` to check
for a testcase or another checking tool.
"""

import os
import re
import subprocess as SPR
from functools import partial
from optparse import OptionParser
from subprocess import PIPE, STDOUT, Popen

from aslint.i18n import _
from aslint.logger import logger
from aslint.string_utils import convert

RE_OUT = re.compile('- output in (.*)$', re.M)


def call(silent):
    """Return a call function"""
    call = SPR.call
    if silent:
        call = partial(SPR.call, stdout=PIPE, stderr=STDOUT)
    return call


def run_configure(waf, opts, silent=True):
    """Configure the version at the current revision"""
    logger.title(_("run configure at the revision"))
    cmd = [waf, "configure"] + opts
    logger.debug("command: %s", cmd)
    ok = call(silent)(cmd) == 0
    return ok


def run_install(waf, vdbg, opts=None, configure=None, silent=True):
    """Build and install the version at the current revision"""
    ok = True
    if not opts:
        opts = []
    if configure:
        ok = run_configure(waf, opts, silent)
    if ok:
        logger.title(_("build at the commit"))
        dbg = vdbg and "_debug" or ""
        cmd = [waf, "install" + dbg] + opts
        # because it blocks with silent=True ?!! with or without '-p' !
        silent = False
        if not silent:
            cmd.append("-p")
        logger.debug("command: %s", cmd)
        ok = call(silent)(cmd) == 0
    return ok


def run_testcase1(testcase, waf, vdbg, opts=None, configure=None,
                  silent=True, hook=None, patch=None):
    """Check the testcase at the current commit

    Arguments:
        testcase (str): the testcase to check.
        waf (str): the waf script to be used.
        vdbg (bool): if True, use the debug version.
        opts (list): aditional options passed to the waf command.
        configure (bool): if True, run configure at each tested commit.
        silent (bool): if False, print the execution trace.
        hook (callable): function called after each execution.
        patch (filename): patch applied before each build.
    """
    if not opts:
        opts = []
    if patch:
        logger.info(_("applying the patch..."))
        SPR.check_call(["patch", "-p1", "-i", patch])
    ok = run_install(waf, vdbg, opts, configure, silent)
    output = None
    if ok and testcase:
        logger.title(_("run the testcase"))
        dbg = vdbg and "_debug" or ""
        cmd = [waf, "test" + dbg, "-n", testcase] + opts
        logger.debug("command: %s", cmd)
        proc = Popen(cmd, stdout=PIPE, stderr=STDOUT)
        err = convert(proc.communicate()[0])
        logger.debug(err)
        mat = RE_OUT.search(err)
        if mat:
            output = mat.group(1)
            logger.title(_("testcase output: %s"), output)
        ok = proc.returncode == 0 and '- exit 0' in err
    if hook:
        logger.info(_("calling hook..."))
        ok = hook(ok, output)
    # revert the patch
    if patch:
        SPR.check_call(["git", "checkout", "."])
        logger.info(_("patch reverted."))
    return ok


def run_aslint(*args):
    """run aslint"""
    return call(silent)(['aslint'] + list(args)) == 0


def bisect_main(testfunc, good, bad):
    """Main loop:
    testfunc() returns True if it is good, False if bad"""
    env = os.environ.copy()
    env['LANGUAGE'] = 'en'
    try:
        SPR.check_call(["git", "bisect", 'start'])
        SPR.check_call(["git", "bisect", 'good', good])
        SPR.check_call(["git", "bisect", 'bad', bad])
    except SPR.CalledProcessError:
        logger.error(_("`git bisect` command failed. Usually this means that "
                       "there are uncommitted changes. If you have to patch "
                       "the source code, consider the `--patch` option."))
        raise
    ended = False
    while not ended:
        ok = testfunc()
        if ok:
            result = "good"
        else:
            result = "bad"
        logger.title(_("this revision is {0}").format(result))
        proc = Popen(["git", "bisect", result],
                     env=env, stdout=PIPE, stderr=STDOUT)
        err = convert(proc.communicate()[0])
        logger.info(err)
        assert proc.returncode == 0
        ended = 'first bad commit' in err
    SPR.run(["git", "bisect", "reset"], env=env)
    cleanup()


def runall_main(testfunc, good, bad):
    """Main loop on all commits
    testfunc() returns True if it is good, False if bad"""

    proc = Popen(["git", "rev-list", "--ancestry-path", "{0}..{1}".format(good, bad)],
                                            stdout=PIPE, stderr=STDOUT)
    out = convert(proc.communicate()[0])
    lrev = out.split()
    lrev.reverse()
    nbr = len(lrev)
    for i, rev in enumerate(lrev):
        proc = Popen(
            ["git", "checkout", rev], stdout=PIPE, stderr=STDOUT)
        err = convert(proc.communicate()[0])
        logger.title(_("run at commit {0} ({1:d}/{2:d})")
                     .format(getparent(), i + 1, nbr))
        logger.info(err)
        assert proc.returncode == 0
        ok = testfunc()
        logger.info(_("testcase ended %s"),
                    _("successfully") if ok else _("with errors"))
    cleanup()


def cleanup():
    """Warn to clean the temporary files"""
    logger.info(_("you should remove the temporary directories if "
                  "you do not need them: rm -rf /tmp/runtest_*"))


def getparent():
    """Return an identifier of the parent revision"""
    cmd = ['git', 'rev-list', 'HEAD', '-1']
    return convert(Popen(cmd, stdout=PIPE).communicate()[0])


def import_hook(hookname):
    """Import a hook"""
    import sys
    if not hookname:
        return None
    modname, func = hookname.split('.')
    logger.debug('hook %r from module %r', func, modname)
    sys.path.insert(0, os.getcwd())
    mod = __import__(modname, globals(), locals(), [modname])
    sys.path.pop(0)
    hook = getattr(mod, func)
    assert hasattr(hook, "__call__"), type(hook)
    return hook
