# coding=utf-8

"""
Define hooks for devtools repository.
"""

from ..ext_utils import is_admin
from .generic import OK, branch_user_commit_hook, branch_user_push_hook

# pylint: disable=invalid-name
repo_branch_checker = lambda _: OK

pretxncommit = branch_user_commit_hook(repo_branch_checker, is_admin, check_msg=True)

pretxnchangegroup = branch_user_push_hook(repo_branch_checker, is_admin, check_msg=True)
