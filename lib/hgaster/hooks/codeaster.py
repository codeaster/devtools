# coding=utf-8

"""Define hooks for code_aster repositories."""

import os
import os.path as osp
from pathlib import Path

from aslint.logger import logger
from repository_api import get_branch, get_main_branch, get_repo_name, repository_type

from ..ext_utils import get_changed_files, is_admin
from .generic import NOOK, OK, branch_checker, branch_user_commit_hook, branch_user_push_hook

# authorized branches names (+ vNN)
AUTH_BRANCH = ("^default$", "^edf/", "^asterxx/", "^v([0-9]+(\.[0-9]+)?(_smeca)?)$")

UNAUTH_BRANCH_GIT = ("^main", "^v([0-9]+(\.[0-9]+)?)")

# pylint: disable=invalid-name
repo_branch_checker = branch_checker(authorized=AUTH_BRANCH)

pretxncommit = branch_user_commit_hook(repo_branch_checker, is_admin, check_msg=True)

pretxnchangegroup = branch_user_push_hook(repo_branch_checker, is_admin, check_msg=True)

git_branch_checker = branch_checker(authorized=".", unauthorized=UNAUTH_BRANCH_GIT)


def precommit_git(repopath, amend):
    """precommit hook for code_aster Git repository."""
    # do pretxncommit: check branch, message
    branch = get_branch(repopath)
    if git_branch_checker(branch) is NOOK:
        logger.error("You are not allowed to commit in " "branch {0!r}.".format(branch), exit=True)
    return aslint(repopath, amend=amend)


def aslint(repopath, paths=(), amend=False):
    """Call aslint before a commit: in case of known error/warning, try to
    fix it. The commit command is not cancelled.
    """
    from subprocess import call

    from aslint.base_checkers import Report, checkcontext
    from aslint.check_files import check_files, read_waf_parameters
    from aslint.config import ASCFG
    from aslint.filetype import filter_dict
    from aslint.logger import logger

    # global informations
    checkcontext.reponame = get_repo_name(repopath)
    checkcontext.branch = get_main_branch(repopath)
    revs = ("HEAD~1",) if amend else None

    # logger.title("running aslint...")
    if _check_preference("pre-commit") == OK:
        return OK

    dchg = get_changed_files(repopath, revs=revs, files=paths)
    changes = filter_dict(dchg)
    report = Report()
    builddir = ASCFG.get("waf.builddir")
    target = "release"
    if os.environ.get("BUILD") == "debug":
        builddir = "build/mpidebug" if osp.isdir("build/mpidebug") else "build/mpi"
        target = "debug"
    wafc4che = osp.join(builddir, "c4che", target + "_cache.py")

    c4che = read_waf_parameters(wafc4che)

    check_files(report, changes, c4che)
    fixed = False
    if report.hasError():
        # run the fixers and remove the messages from the report
        dcmd = report.getFixerCommands()
        for eid, cmd in list(dcmd.items()):
            fixed = True
            logger.warn("fixing error {0}...".format(eid))
            call(cmd, shell=True)
            report.remove(eid)
    # check for remaining errors
    if not report.hasError():
        nbchg = sum([len(i) for i in changes.values()])
        if nbchg > 0:
            logger.info("pre-commit: aslint hook passed successfully")
        else:
            logger.info("pre-commit: no files to be checked")
        if fixed and repository_type(repopath) == "git":
            logger.warn("pre-commit: some files were automatically fixed and added by running:")
            toadd = dchg.get("A", []) + dchg.get("M", [])
            cmd = ["git", "add"] + toadd
            logger.info(" ".join(cmd))
            call(cmd)
    else:
        logger.info(report.to_text())
        report.report_summary()
        answ = input(
            "The commit will be not accepted because of remaining "
            "errors.\nDo you want to continue (y/n) ? "
        )
        if answ.lower() not in ("y", "o"):
            logger.info("pre-commit: aslint ended with errors.")
            return NOOK
        else:
            logger.cancel_error()
    return OK


def run_ctest_minimal(repopath):
    """Run a minimal list of testcases.

    Arguments:
        repopath (str): Repository path.
    """
    from subprocess import call

    from aslint.base_checkers import checkcontext
    from aslint.check_files import read_waf_parameters
    from aslint.config import ASCFG

    checkcontext.reponame = get_repo_name(repopath)
    if checkcontext.reponame != "src":
        return OK
    if _check_preference("pre-push") == OK:
        return OK

    logger.warning("Run a minimal list of testcases (for about 30 seconds).")

    builddir = ASCFG.get("waf.builddir")
    target = "release"
    if os.environ.get("BUILD") == "debug":
        builddir = "build/mpidebug" if osp.isdir("build/mpidebug") else "build/mpi"
        target = "debug"
    wafc4che = osp.join(builddir, "c4che", target + "_cache.py")
    c4che = read_waf_parameters(wafc4che)
    if not c4che:
        logger.warning("Can not find installation directory. Cancelled.")
        return OK
    run_ctest = Path(c4che["BINDIR"]) / "run_ctest"

    logger.info("NB: This version will be used: %s", run_ctest)
    try:
        answ = input("Do you want to continue ([y]/n, 'Ctrl+C' to abort push) ? ")
    except KeyboardInterrupt:
        return NOOK
    if answ.lower() == "n":
        logger.info("run_ctest skipped.")
        return OK

    cmd = [run_ctest, "-R", "(asrun0|mumps02b|supv002|vocab0|zzzz509j)", "--no-resutest"]
    if call(cmd):
        return NOOK
    return OK


def _check_preference(hook):
    """Check if a checking is enabled or disabled in '~/.gitconfig'.

    Arguments:
        hook (str): Hook name.

    Returns:
        bool: OK if the hook should be skipped, NOOK otherwise.
    """
    from aslint.config import ASCFG

    assert hook in ("pre-commit", "pre-push"), hook
    option = f"check.{hook.replace('-', '')}"
    gitopt = option.replace(".", "-")

    choice = ASCFG.get(option)
    if choice == "no":
        logger.warn(
            "%s checkings skipped because of '%s'" " value in your ~/.gitconfig file", hook, gitopt
        )
        return OK
    elif choice in (None, "undef"):
        logger.info(
            "%s: If you want to disable these checkings, just add "
            "'%s = no' in the [aster] section in your ~/.gitconfig file.",
            hook,
            gitopt,
        )
        logger.info("%s: To hide this message you can set the value '%s = yes'.", hook, gitopt)
    elif choice != "yes":
        logger.warn(
            "unknown choice %r for '%s' (see ~/.gitconfig) must be 'yes' or 'no'.", choice, option
        )
    return NOOK
