# coding=utf-8

"""
Define hooks for AsterStudy repository.
"""

import re

from ..ext_utils import is_admin
from .generic import (branch_checker, branch_user_commit_hook,
                      branch_user_push_hook)

# pylint: disable=invalid-name
repo_branch_checker = branch_checker(authorized='^edf/',
                                     unauthorized=('^edf/default$',
                                                   '^edf/build$'))

pretxncommit = branch_user_commit_hook(repo_branch_checker, is_admin,
                                       check_msg=True)


def admin_or_contributors(author):
    """Tell if the author of a changeset is an administrator or an external
    contributor of AsterStudy.

    Arguments:
        author (str): Author field of the changeset.

    Returns:
        bool: *True* if the author is an admin or a known contributor.
    """
    return bool(is_admin(author) or re.search('@.*opencascade.com', author))


pretxnchangegroup = branch_user_push_hook(repo_branch_checker,
                                          admin_or_contributors,
                                          check_msg=True)
