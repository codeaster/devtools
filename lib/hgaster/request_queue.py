# coding=utf-8

"""Manage the queue of pull-requests for integration"""

import os
import os.path as osp
import time

from aslint.config import ASCFG
from aslint.i18n import _
from aslint.logger import logger
from repository_api import get_rev_log, hgcmd

from .ext_utils import sendmail, shortrev_s


class QueueError(Exception):

    """All exceptions raised by the RequestQueue instances"""

    def __init__(self, message):
        """Register the error message"""
        self.message = message

    def __str__(self):
        """Simple representation"""
        return repr(self.message)


def requestkey(repo, branch, username):
    """Make a valid key to be used in the queue"""
    username = username.replace(' ', '_')
    return repo, branch, username


class Request(object):

    """A request for integration

    Attributes:
    - repo: repository name
    - branch: branch name
    - username: username of the developer
    - status: status of the request: PEND, RUN, TEST
    - node: the changeset id [= ctxt.hex()]
    - email: email of the developer
    - finalrev: the id of the changeset really integrated [= ctxt.hex()]

    - key: (repo, branch, username)
    - shortnode: node[:16]
    """
    __slots__ = ('repo', 'branch', 'username', 'status',
                 'node', 'email', 'finalrev',
                 'key', 'shortnode')

    def __init__(self, repo, branch, username,
                 status='', node='', finalrev='_', email=''):
        """Fill the request"""
        self.repo = repo
        self.branch = branch
        self.username = username.replace(' ', '_')
        self.status = status or 'PEND'
        self.node = node
        self.email = email
        self.finalrev = finalrev
        self.key = (repo, branch, self.username)
        self.shortnode = shortrev_s(self.node)
        assert self.node and self.email, "node and email are required, got: " \
            "'%s' and '%s'" % (self.node, self.email)
        assert ' ' not in email, 'email address must contain no space'

    def __repr__(self):
        """Return a line representing a request"""
        return "%-11s %-7s %-25s %-7s %-40s %-40s %s" % (
            self.repo, self.branch, self.username, self.status,
            self.node, self.finalrev, self.email)


class RequestQueue(object):

    """Session to clone the queue and update it

    The queue is a dict:
        { (repository name, branch, username) : [order, Request object] }
    """

    def __init__(self, uri=None, delay=1, notify=False, repodir=None):
        """Create a new RequestQueue session
        :param uri: repository URI
        :param delay: delay used in case of conflict
        :param notify: enable/disable email notification
        :param repodir: clone directory (for test purpose)
        """
        self._nbfield = 7
        self.uri = uri or ASCFG.get('admin.queue_uri')
        self.delay = delay
        self.repodir = repodir or \
            osp.join(ASCFG.get('devtools_cache'), 'requestqueue')
        self.queuefile = osp.join(self.repodir, 'queue')
        self.queue = None
        self.tip = None
        self.notify = notify

    def init(self):
        """Clone the RequestQueue repo"""
        # use Popen to suppress output
        logger.debug('hg.bin: %s', ASCFG.get('hg_bin'))
        logger.debug('queue uri: %s', self.uri)
        need_update = osp.isdir(self.repodir)
        if not osp.isdir(self.repodir):
            hgcmd("clone", "", self.uri, self.repodir)
        repoid = get_rev_log(self.repodir)
        if repoid != ASCFG.get('admin.queue_repoid'):
            raise QueueError(_("this is not the right repository"))
        if need_update:
            self._update(clean=True)
        return self

    def _update(self, clean=False):
        """Get the lastest queue"""
        out = hgcmd("pull", self.repodir)[1]
        logger.debug('hg pull output:\n%s', out)
        out = hgcmd("update", self.repodir, clean=clean)[1]
        logger.debug('hg update output:\n%s', out)
        self._tip()
        self._read()

    def _tip(self):
        """Set tip attribute to the tip of the local repository"""
        self.tip = get_rev_log(self.repodir, "tip", fmt='{rev}')

    def _read(self):
        """Read the queue"""
        self.queue = {}
        idx = 0
        with open(self.queuefile, 'r') as fobj:
            for line in fobj.read().splitlines():
                line = line.strip()
                if not line or line.startswith('#'):
                    continue
                logger.debug('line: %s', line)
                spl = line.split()
                if len(spl) == self._nbfield - 1:
                    spl.insert(5, '_')
                if len(spl) != self._nbfield:
                    raise QueueError(_("invalid line: '%s'") % line)
                self.set(idx, Request(*spl))
                idx += 1
        logger.debug('queue content: %s', self.queue)

    def __iter__(self):
        """Iterate on the queue"""
        if self.queue is None:
            self._update()
        for request in self.sorted():
            yield request

    def sorted(self):
        """Return the requests sorted by their index"""
        if self.queue is None:
            self._update()
        values = sorted(self.queue.values(), key=lambda x: x[0])
        return [request for dummy, request in values]

    def keys(self):
        """Return the keys of the requests"""
        if self.queue is None:
            self._update()
        return list(self.queue.keys())

    def __len__(self):
        """Return the number of request"""
        if self.queue is None:
            self._update()
        return len(self.queue)

    def _write(self):
        """Write the queue file"""
        lines = [
            "# Requests for integration - DO NOT EDIT",
            "#repository branch  username                  status"
            "  revision.node                            finalrev.node"
            "                            email",
        ]
        for request in self:
            lines.append(repr(request))
        lines.append('')
        txt = os.linesep.join(lines)
        with open(self.queuefile, 'w') as fobj:
            fobj.write(txt)

    def _exec_push(self, request, func, args, message, details=None, cc=None):
        """Call a function and push changes immediately"""
        ok, ntry = False, 0
        while not ok and ntry < 10:
            ntry += 1
            if ntry > 1:
                logger.debug(_("try #%d..."), ntry)
                time.sleep(self.delay)
            ok = func(*args) == 0
            if not ok:
                break
            if self._remote_changed():
                self._update(clean=True)
                ok = False
                continue
            else:
                iret, out = hgcmd("commit", self.repodir, message=message)
                if iret != 0:
                    logger.error(_("commit in the pull-request repository "
                                   "failed with message:\n{0}: {1}")
                                 .format(iret, out))
                    return 1
                ok = self._push()
        if not ok:
            first_line = message.strip().splitlines()[0]
            logger.debug(_("failed after {0:d} attempts: {1}")
                         .format(ntry, first_line))
            logger.error(_("failed: {0}").format(first_line))
            return 1
        elif self.notify:
            text = message
            if details:
                text += os.linesep + details
            dest = [request.email]
            if cc:
                dest.append(cc)
            sendmail(dest, subject=message, text=text,
                     fromaddr=ASCFG.get('notify.codeaster'),)
        return 0

    def _remote_changed(self):
        """Tell if the remote repository has changed"""
        iret = hgcmd("incoming", self.repodir)[0]
        logger.debug(_("are there new changesets: %s"), iret == 0)
        return iret == 0

    def _push(self):
        """Push changeset to main queue"""
        self._tip()
        # use Popen to suppress output
        prev = os.getcwd()
        try:
            os.chdir(self.repodir)
            iret = hgcmd("push", self.repodir,
                            self.uri, rev=self.tip)[0]
            if iret == 1: # nothing to push
                iret = 0
        finally:
            os.chdir(prev)
        logger.debug('hg push exits with code %s', iret)
        return iret == 0

    def set(self, idx, request):
        """Set a value in the queue"""
        if self.queue is None:
            self._update()
        key = request.key
        assert self.get(key) is None, "already exists: %r" % request
        assert type(request) is Request, "unexpected request: %r" % request
        match = self.get_by_node(request.node)
        assert not match or match.node != request.node, \
            "already submitted: %r\nexisting: %r" \
            % (request, self.get_by_node(request.node))
        self.queue[key] = [idx, request]

    def _get(self, key, idx):
        """Return the couple (idx, request) registered under key or None"""
        if self.queue is None:
            self._update()
        value = self.queue.get(key)
        if value is not None:
            value = value[idx]
        return value

    def get(self, key):
        """Return the request currently registered, or None"""
        return self._get(key, 1)

    def index(self, key):
        """Return the request currently registered, or None"""
        idx = self._get(key, 0)
        if idx is None:
            idx = -1
        return idx

    def get_by_node(self, node):
        """Return the request that match 'node', or None"""
        if self.queue is None:
            self._update()
        found = None
        approx = []
        for dummy, request in list(self.queue.values()):
            if node in (request.node, request.finalrev):
                found = request
                break
            if request.node.startswith(node):
                approx.append( request )
        if not found and len( approx ) == 1:
            found = approx[0]
        return found

    def add(self, request):
        """Insert the pull-request in the queue"""
        iret = 0
        try:
            self.set(len(self), request)
            self._write()
        except AssertionError as exc:
            logger.debug(_("can not add request: {0}").format(exc))
            iret = 1
        return iret

    def remove(self, key, status):
        """Remove a pull-request that must be in `status`"""
        request = self.get(key)
        if type(status) not in (list, tuple):
            status = [status]
        if request is None or request.status not in status:
            logger.warn(_("status {0!r} is expected").format(status))
            return 1
        del self.queue[key]
        self._write()
        return 0

    def modify(self, key, attr, new, old=None):
        """Modify an attribute of a request that must be equal to old"""
        req = self.get(key)
        assert type(attr) is str and hasattr(req, attr), \
            'not a request attribute: %r' % attr
        if req is None or (old is not None and getattr(req, attr) != old):
            return 1
        setattr(req, attr, new)
        self._write()
        return 0

    def submit(self, req, details=None):
        """Add a pull-request, status is set to PEND"""
        msgval = req.username, req.repo, req.branch, req.shortnode
        message = _("submit request for {0} ({1}/{2}) {3}").format(*msgval)
        iret = self._exec_push(req, self.add, (req, ),
                               message=message, details=details)
        if iret == 0:
            logger.info(_("request submitted for {0} in '{1}', branch {2}")
                        .format(*msgval[:3]))
        return iret

    def start(self, key, details=None):
        """Mark a pull-request as started, status is set to RUN"""
        req = self.get(key)
        msgval = req.username, req.repo, req.branch, req.shortnode
        if req.status == 'RUN':
            logger.warn(_("integration already started for %s"), req.node)
            return 0
        iret = self._exec_push(
            req, self.modify, (key, 'status', 'RUN', 'PEND'),
            message=_("start integration for {0} ({1}/{2}) {3}").format(*msgval),
            details=details)
        if iret == 0:
            logger.info(_("integration started for {0} in '{1}', branch {2}")
                        .format(*msgval[:3]))
        return iret

    def stop(self, key, details=None):
        """Mark a pull-request as stopped/differed, status is reset to PEND"""
        req = self.get(key)
        msgval = req.username, req.repo, req.branch, req.shortnode
        if req.status == 'PEND':
            logger.warn(_("integration already stopped for %s"), req.node)
            return 0
        iret = self._exec_push(req, self.modify, (key, 'status', 'PEND'),
                               message=_("stop integration for "
                                         "{0} ({1}/{2}) {3}").format(*msgval),
                               details=details)
        if iret == 0:
            logger.info(_("integration stopped for {0} in '{1}', branch {2}")
                        .format(*msgval[:3]))
        return iret

    def cancel(self, key, status='PEND', details=None):
        """Cancel a pull-request"""
        req = self.get(key)
        msgval = req.username, req.repo, req.branch, req.shortnode
        iret = self._exec_push(req, self.remove, (key, status),
                               message=_("cancel request for "
                                         "{0} ({1}/{2}) {3}").format(*msgval),
                               details=details,
                               cc='admins')
        if iret == 0:
            logger.info(_("request cancelled for {0} ({1}/{2}) {3}")
                        .format(*msgval))
        return iret

    def test(self, key, finalrev=None, details=None):
        """Marks a pull-request as 'to be tested', status is set to TEST"""
        def _test(key, asrev):
            """Change finalrev and status"""
            iret = self.modify(key, 'status', 'TEST', 'RUN')
            if iret == 0:
                iret = self.modify(key, 'finalrev', asrev)
            return iret
        req = self.get(key)
        if req is None:
            return 1
        if finalrev:
            cmt = _("as %s") % shortrev_s(finalrev)
        else:
            cmt = _("of %s") % req.shortnode
            finalrev = req.node
        msgval = req.username, req.repo, req.branch, cmt
        iret = self._exec_push(req, _test, (key, finalrev),
                               message=_("mark revision to test for "
                                         "{0} ({1}/{2}) {3}").format(*msgval),
                               details=details)
        if iret == 0:
            logger.info(_("mark revision to test for {0} ({1}/{2}) {3}")
                        .format(*msgval))
        return iret

    def close(self, key, details=None):
        """The revision has been integrated"""
        req = self.get(key)
        msgval = req.username, req.repo, req.branch, req.shortnode
        iret = self._exec_push(req, self.remove, (key, 'TEST'),
                               message=_("revision integrated for "
                                         "{0} ({1}/{2}) {3}").format(*msgval),
                               details=details)
        if iret == 0:
            logger.info(_("revision integrated for {0} ({1}/{2}) {3}")
                        .format(*msgval))
        return iret

    def refuse(self, key, details=None):
        """The revision has been refused"""
        req = self.get(key)
        msgval = req.username, req.repo, req.branch, req.shortnode
        iret = self._exec_push(req, self.remove, (key, 'TEST'),
                               message=_("revision refused for "
                                         "{0} ({1}/{2}) {3}").format(*msgval),
                               details=details,
                               cc='admins')
        if iret == 0:
            logger.info(_("revision refused for {0} ({1}/{2}) {3}")
                        .format(*msgval))
        return iret
