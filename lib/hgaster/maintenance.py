# coding=utf-8

"""Module to update an installation, validate the proposed changesets
"""

import json
import os
import os.path as osp
import re
import shutil
import sys
import tempfile
from glob import glob
from optparse import SUPPRESS_HELP, OptionGroup, OptionParser
from subprocess import call

from aslint.api_asrun import check_asrun_bin, get_asrun_config
from aslint.check_files import (
    Diag,
    c4che_ismpi,
    exclude_test,
    read_waf_parameters,
    run_testcase,
    run_waf_command,
)
from aslint.config import ASCFG
from aslint.decorators import locked, log_as_important, stop_on_failure
from aslint.i18n import _
from aslint.logger import OK, ErrorCode, logger, setlevel
from aslint.test_result import TestResult
from aslint.utils import clean_dir, prefix_from_options
from repository_api import get_parent_desc, get_reference_head, get_rev_log

from hgaster.ext_utils import hgcmd, is_admin, newui, sendmail, shortrev_s
from hgaster.request_queue import RequestQueue, requestkey

try:
    import yaml
except ImportError:
    yaml = None

REFDIR = osp.dirname(ASCFG.get("devtools_root"))

EMAIL_TEMPLATE = _(
    """
Important messages:

%(msg)s

Full log content was written to: %(logf)s
"""
)


class BaseMaintenance(object):
    """Base class for maintenance operations"""

    _command = ""
    _title = ""
    _interrupt_message = _("command interrupted because of errors")
    _all = ("data", "src", "validation")

    def __init__(self, docstr):
        """Initialization - see `_set_parser`"""
        self.opts = None
        self._rootdir = None
        # internal variables
        self._docstr = docstr
        self._parser = None
        self._notify_on_error = True
        self.lockfile = None
        self.c4che = None
        self._list = None
        self._username = None

    def _set_parser(self, exclude=None):
        """set the optparser object for `action`"""
        exclude = exclude or []
        parser = self._parser = OptionParser(usage=self._docstr)
        parser.add_option(
            "-g", "--debug", action="callback", callback=setlevel, help="add debug informations"
        )

        if "repo" not in exclude:
            group = OptionGroup(parser, "Options for the repositories")
            group.add_option(
                "--root",
                action="store",
                default=REFDIR,
                help="directory containing the repositories "
                "(default: {0})".format(ASCFG.get("devtools_root")),
            )
            group.add_option(
                "-b",
                "--branch",
                action="store",
                default="default",
                help="branch to use (default: default)",
            )
            group.add_option(
                "-r",
                "--rev",
                action="store",
                default="reference",
                help="the revision to update the repository 'src' " "(default: reference)",
            )
            group.add_option(
                "--data",
                action="store",
                default="reference",
                metavar="REV",
                help="the revision to update the repository  'data' " "(default: reference)",
            )
            group.add_option(
                "--validation",
                action="store",
                default="reference",
                metavar="REV",
                help="the revision to update the repository  'validation' " "(default: reference)",
            )
            parser.add_option_group(group)

        if "build" not in exclude:
            group = OptionGroup(parser, "Options for the build")
            group.add_option(
                "--env",
                action="append",
                default=[],
                help="environment script to source before running waf " "(default: []",
            )
            group.add_option(
                "--waf",
                action="store",
                default="waf",
                help="alternative waf binary to use (default: waf)",
            )
            group.add_option(
                "--command", action="append", default=[], help="waf command to run (default: [])"
            )
            group.add_option(
                "--options",
                action="append",
                default=[],
                help="options passed to the waf command (default: [])",
            )
            group.add_option(
                "--builddir",
                action="store",
                default=ASCFG.get("waf.builddir"),
                metavar="DIR",
                help="alternative waf build directory "
                "(default: {0})".format(ASCFG.get("waf.builddir")),
            )
            parser.add_option_group(group)

        if "test" not in exclude:
            group = OptionGroup(parser, "Options to run testcases")
            group.add_option(
                "--testlist",
                action="store",
                default="verification",
                metavar="FILE|verification|verification_intra|verification_all|validation|interact|submit",
                help="list of testcases to run. "
                "May be a FILE containing a list of testcases or a special "
                "value 'verification', 'verification_intra', "
                "'verification_all', 'validation', 'interact' or "
                "'submit' meaning respectively "
                "those of src/astest with tag 'verification', "
                "those of validation/astest with tag 'verification', "
                "those of the both repositories with tag 'verification', "
                "those of validation/astest with tag 'validation', "
                "those of src/astest with tag 'interact', "
                "those of src/astest with tag 'submit' "
                "(default: verification). "
                "Some are automatically excluded from the list "
                "(see lib/aslint/test/list.exclude)",
            )
            group.add_option(
                "--resutest",
                action="store",
                metavar="DIR",
                help="directory to write the results of the testcases "
                "(default: resu_test/`revisions`), relative to ROOT",
            )
            group.add_option(
                "--add-sequential",
                dest="add_seq",
                action="store_true",
                default=False,
                help="also run the sequential testcases (does nothing with a sequential"
                "version (default: False).",
            )
            group.add_option(
                "--clean",
                action="store_true",
                dest="clean",
                default="auto",
                help="remove the content of 'resutest' directory before starting "
                "(default: auto)",
            )
            group.add_option(
                "--no-clean",
                action="store_false",
                dest="clean",
                help="do not remove the content of 'resutest' directory",
            )
            group.add_option(
                "--nbmaxnook",
                action="store",
                metavar="N",
                help="maximum number of errors before interrupting the process "
                "(default: unlimited)",
            )
            group.add_option(
                "--timefactor",
                action="store",
                metavar="X",
                type="float",
                default=ASCFG.get("test.facmtps"),
                help="multiplicative factor of the timelimit "
                "for each testcase (default: {0})".format(ASCFG.get("test.facmtps")),
            )
            group.add_option(
                "--notify-user",
                action="store_true",
                help="notify the user on completion or failure. The address "
                "must be defined in [user] section of ~/.gitconfig",
            )
            group.add_option(
                "--notify-admins",
                action="store_true",
                help="notify the admins on completion or failure.",
            )
            group.add_option(
                "--notify-resutest",
                action="store_true",
                help="notify the resutest list on completion or failure.",
            )
            parser.add_option_group(group)

    def _parse_args(self, args=None):
        """parse command line arguments"""
        self._set_parser()
        self.opts, args = self._parser.parse_args(args or sys.argv[1:])
        if len(args) != 0:
            self._parser.error("no argument expected")
        self._normpath()

    def _normpath(self):
        """Normalize paths."""
        self._rootdir = osp.abspath(self.opts.root)
        if self.opts.resutest and self.opts.resutest != "NONE":
            self.opts.resutest = osp.abspath(self.opts.resutest)

    def _rebuild_args(self, exclude_group=None, keep=None, ignore=None):
        """Rebuilds the list of command line arguments."""
        # very dependent to optparse and some of its *private* attributes...
        exclude_group = exclude_group or []
        keep = keep or []
        ignore = ignore or []
        args = []
        for grp in self._parser.option_groups:
            testkeep = False
            for excl in exclude_group:
                if excl in grp._long_opt:
                    testkeep = True
            for opt in grp.option_list:
                longopt = opt._long_opts[0]
                if testkeep and longopt not in keep:
                    continue
                if longopt in ignore:
                    continue
                val = getattr(self.opts, opt.dest)
                if opt.action == "append" and val:
                    for i in val:
                        args.append(longopt + "=" + str(i))
                elif opt.action == "store" and val is not None:
                    args.append(longopt + "=" + str(val))
                elif opt.action == "store_true" and val is True:
                    args.append(longopt)
                elif opt.action == "store_false" and val is False:
                    args.append(longopt)
        return args

    def _error(self, message, *args):
        """Log the message error and exit"""
        logger.error(message, *args)
        logger.exit()

    def check_error(self):
        """Interrupt the process if an error occurred"""
        if not logger.is_error():
            return
        if self._notify_on_error:
            sendmail("admins", self._interrupt_message)
        self._error(self._interrupt_message)

    @stop_on_failure
    def _require_admin(self):
        """stop if the admin rights are required"""
        if self._username is None:
            self._username = newui().username()
        if not is_admin(self._username):
            logger.error(_("only administrators are authorized to perform " "this operation"))
        return logger.errcode

    def _update_repo(self, reponame, rev=None, source=None, update=True):
        """update a repository"""
        repodir = osp.join(self._rootdir, reponame)
        logger.info(_("update repository '{0}' to '{1}'").format(repodir, rev or "-"))
        if not osp.isdir(osp.join(repodir, ".hg")):
            logger.warn(_("not a repository: %s"), repodir)
            return OK
        prev = os.getcwd()
        os.chdir(repodir)
        errcode = ErrorCode()
        cmd = [ASCFG.get("hg.bin"), "pull"]
        if rev:
            cmd.extend(["--rev", rev])
        if source:
            cmd.append(source)
        errcode.from_status(call(cmd))
        if update:
            cmd = [ASCFG.get("hg.bin"), "update", "--clean"]
            if rev:
                cmd.extend(["--rev", str(rev)])
            errcode.from_status(call(cmd))
        os.chdir(prev)
        return errcode

    def _push_revision(self, reponame, rev, dest):
        """Push the given revision of 'reponame' to 'dest'"""
        repodir = osp.join(self._rootdir, reponame)
        logger.title(_("push the revision '{0}' of the repository '{1}'").format(rev, repodir))
        if not osp.isdir(osp.join(repodir, ".hg")):
            logger.error(_("not a repository: %s"), repodir)
            return logger.errcode
        prev = os.getcwd()
        os.chdir(repodir)
        errcode = ErrorCode()
        cmd = [ASCFG.get("hg.bin"), "push", "--rev", str(rev), dest]
        errcode.from_status(call(cmd))
        if errcode.is_ok():
            logger.info(_("push ended successfully"))
        else:
            logger.warn(_("no revision pushed, maybe already there"))
        os.chdir(prev)
        return errcode

    def _alias(self, dictrev):
        """Add an alias name computed from the revision of each repository"""
        alias = ["dsv"] if not c4che_ismpi(self.c4che or {}) else ["mpi"]
        for repo in self._all:
            alias.append(dictrev.get(repo, "")[:12])
        dictrev["alias"] = "-".join(alias)

    def get_reference_revisions(self, branch):
        """retreive the revision id of the reference head of each branch
        Return a dict containing the revision + an alias for the n-uplet of
        the revisions"""
        drev = {}
        for reponame in self._all:
            path = osp.join(self._rootdir, reponame)
            ref = path if getattr(self.opts, "already_updated", False) else None
            try:
                drev[reponame] = get_reference_head(path, branch=branch, source=ref)
            except IOError:
                self._error(_("can not get the reference revision of '%s'"), reponame)
        self._alias(drev)
        logger.debug("revisions read from request_queue:\n%s", str(drev))
        return drev

    def _dictrev(self, branch):
        """Return a dict containing the revision for each repository
        + an alias for the n-uplet of the revisions"""
        self.pull_update(update=False)
        drev = {}
        logger.title(_("set revisions to test"))
        defrev = self.get_reference_revisions(branch)
        for repo in self._all:
            rev = getattr(self.opts, repo if repo != "src" else "rev")
            if rev in ("reference", "unstable", "stable-updates"):
                rev = defrev.get(repo, rev)
            logger.info("repository: %-10s - revision: %s", repo, rev)
            drev[repo] = rev
        self._alias(drev)
        return drev

    def set_waf_params(self):
        """Read and store waf parameters"""
        srcdir = osp.join(self._rootdir, "src")
        bld = self.opts.builddir
        wafc4che = osp.join(srcdir, bld, "c4che", "release_cache.py")
        if self.c4che:
            logger.info("refresh waf parameters from %s", wafc4che)
        try:
            self.c4che = read_waf_parameters(wafc4che)
            assert self.c4che is not None
        except (AssertionError, IOError):
            self._error(
                _(
                    "this version must be configured first, "
                    "not found: {0}\n"
                    "Maybe you need to use --root/--builddir options.\n"
                ).format(wafc4che)
            )
        self.c4che["ASTER_HAVE_MPI"] = (
            c4che_ismpi(self.c4che)
            or self.c4che.get("LIB_MPI") is not None
            or self.c4che.get("INCLUDES_MPI") is not None
        )
        versdir = self.c4che["ASTERDATADIR"]
        logger.title(_("version directory is '%s'"), versdir)
        return self.c4che

    @stop_on_failure
    def build_testlist(self):
        """Return the list of testcase to run
        testlist: a file containing the testcases to run.
        Special values for `testlist` are 'verification', 'interact',
        'validation' or 'verification_all'.
        """
        lst = self.opts.testlist
        if lst not in (
            "src",
            "verification",
            "verification_all",
            "verification_intra",
            "validation",
            "submit",
            "interact",
            "ci",
        ):
            lst = osp.abspath(lst)
            logger.title(_("list of testcases is read from the file '%s'"), lst)
            if not osp.isfile(lst):
                logger.error(_("no such file: %s"), lst)
            self._list = osp.abspath(lst)
        else:
            if lst == "src":
                logger.warn(_("'src' is deprecated, please use 'verification' " "instead."))
                lst = "verification"
            logger.title(_("build the list of testcases"))
            check_asrun_bin()
            versdir = self.c4che["ASTERDATADIR"]
            cmd = [
                ASCFG.get("asrun.bin"),
                "--list",
                "--nodebug_stderr",
                "--vers={0}".format(versdir),
            ]
            cmd.append("--all")
            if lst in ("submit", "ci"):
                name = lst
            elif lst == "interact":
                name = "verif_interact"
            elif lst == "submit":
                name = "verification"
            else:
                name = "verification" if "verification" in lst else "validation"
            filt = []
            filt.append('--filter="%s" in testlist' % name)
            fname = "list_seq"
            if c4che_ismpi(self.c4che):
                fname = "list_par"
                if not self.opts.add_seq:
                    filt.append('--filter="parallel" in testlist')
                if "verification" in lst:
                    filt.append("--filter=mpi_nbcpu <= 8")
                    filt.append("--filter=mpi_nbnoeud <= 2")
            else:
                filt.append('--filter="parallel" not in testlist')
            fname = fname + "_" + lst
            self._list = osp.join(self._rootdir, fname)
            cmd.extend(filt)
            cmd.extend(["-o", self._list])
            if lst in ("verification", "verification_intra"):
                # only one directory
                dirtest = "src" if lst == "verification" else "validation"
                astest = osp.join(self._rootdir, dirtest, "astest")
                if osp.isdir(astest):
                    cmd.append("--astest_dir=%s" % astest)
            logger.debug(cmd)
            logger.update_from_status(call(cmd))
            exclude_test(self._list, "list.exclude")
        return logger.errcode

    def _list_errors(self, list_err):
        """Write the list of testcase failures
        This list will be used for next runs."""
        fname = "list_seq"
        if c4che_ismpi(self.c4che):
            fname = "list_par"
        fname = fname + "_err"
        self._list = osp.join(self._rootdir, fname)
        with open(self._list, "w") as fobj:
            fobj.write(os.linesep.join(list_err))

    def run_test(self, resutest, alias, prefs=None, is_success=None, reuse_previous_run=False):
        """Run the testcases"""
        # XXX do not use hostfile ?!
        assert self.c4che is not None, "c4che not set"
        assert self._list is not None, "testlist not set"
        use_tmp = resutest == "NONE"
        if use_tmp:
            resudir = tempfile.mkdtemp(prefix="resutest_")
        else:
            resudir = osp.join(self._rootdir, resutest or osp.join("resu_test", alias))
        prev = None
        if reuse_previous_run and osp.isdir(resudir):
            prev = TestResult(resudir)
            prev.read_all()
        params = {
            "version": self.c4che["ASTERDATADIR"],
            "have_mpi": c4che_ismpi(self.c4che),
            "nomjob": "run_test",
            "list": self._list,
            "resu_test": resudir,
            "cpresok": "RESOK",
            "nbmaxnook": self.opts.nbmaxnook or 99999,
            "facmtps": self.opts.timefactor,
            "args": getattr(self.opts, "args", None),
        }
        if self.opts.testlist == "interact":
            params["mode"] = "interactif"
        if prefs:
            params.update(prefs)
        if self.opts.clean and not reuse_previous_run:  # clean = True or 'auto'
            if self.opts.clean == "auto" and osp.exists(resudir) and not use_tmp:
                logger.warn(_("{0} will be removed.").format(resudir))
                answ = input(_("do you want to continue (y/n) ?"))
                if answ.lower() not in ("y", "o"):
                    self._error(_("interrupt by user"))
            clean_dir(osp.join(resudir, "*"))
        # does not exist if --vers and not --root
        desc = ""
        if osp.isdir(osp.join(self._rootdir, "src")):
            desc = get_parent_desc(osp.join(self._rootdir, "src"))
        if desc:
            logger.start_important()
            logger.title(_("current revision: %s"), desc)
            logger.stop_important()
        add_restart = getattr(self.opts, "add_restart", False)
        no_retry = getattr(self.opts, "no_retry", False)

        errcode, result = run_testcase(
            params,
            is_success=is_success,
            add_seq=self.opts.add_seq,
            add_restart=add_restart,
            no_retry=no_retry,
            resu_init=prev,
        )
        logger.debug("TestResult summary: {}".format(result.repr()))
        list_err = result.get_error()
        if use_tmp:
            clean_dir(resudir)
        self._addremove_result(resudir, alias)
        return errcode, list_err

    def _addremove_result(self, resutest, alias):
        """Apply addremove in resutest + commit a new revision"""
        if not osp.isdir(osp.join(resutest, ".hg")):
            logger.info(_("not a repository: %s"), resutest)
            return
        iret, out = hgcmd("addremove", resutest)
        if iret == 0 or not out.strip():
            logger.info(_("add/remove files in %s"), resutest)
        else:
            logger.warn(_("failure during add/remove files in %s"), resutest)
            logger.warn("\n%s", out)
            return
        if c4che_ismpi(self.c4che):
            alias = "[mpi] " + alias
        else:
            alias = "[seq] " + alias
        iret, out = hgcmd("commit", resutest, message=alias)
        if iret == 0 or not out.strip():
            logger.info(_("new results committed successfully"))
        else:
            logger.warn(_("failure during committing files in %s"), resutest)
            logger.warn("\n%s", out)

    def pull_update(self, dictrev=None, default=None, source=None, update=True):
        """update all the repositories to the revisions given in a dict
        Use 'default' if a repository is not in the dict."""
        dictrev = dictrev or {}
        errcode = ErrorCode()
        logger.title(_("update repositories"))
        for repo in self._all:
            errcode.add(
                self._update_repo(repo, dictrev.get(repo, default), source=source, update=update)
            )
        errcode.cancel_error()
        # TODO check the parents of each repository
        return errcode

    def push_revisions(self, dictrev=None):
        """For all the repositories push the revisions given in a dict"""
        dictrev = dictrev or {}
        errcode = ErrorCode()
        logger.title(_("update remote repositories"))
        self._require_admin()
        for repo in self._all:
            rev = dictrev.get(repo)
            if rev is None:
                continue
            errcode.add(self._push_revision(repo, rev, dest=ASCFG.get("admin_intg_uri")))
        errcode.cancel_error()
        return errcode

    @stop_on_failure
    def waf_command(self, command=None, options=None):
        """Execute `waf command` in `_rootdir`/src"""
        cmd = command or self.opts.command
        opts = options or self.opts.options
        if self.opts.branch != "v14":
            opts.append("--safe")
        if not cmd:
            cmd.append("install")
        if "--install-tests" in opts:
            # take the installation directory from 'prefix' options
            # because c4che is containing the path of the previous
            # installation!
            prefix = prefix_from_options(opts)
            if not prefix:
                self._error(_("--options=--prefix=XXX is required " "to install the testcases"))
            clean_dir(osp.join(prefix, "share", "aster", "tests"))
        result = run_waf_command(
            osp.join(self._rootdir, "src"),
            self.opts.waf,
            cmd,
            options=opts,
            env=self.opts.env,
            return_output=True,
        )
        errcode, output = result
        logger.info(output)
        # refresh c4che
        self.set_waf_params()
        return errcode

    @stop_on_failure
    def close_request(self, success, dictrev):
        """Close/refuse the request for integration if the testcases are
        successfull or not"""
        logger.title(_("remove the request for integration from the queue"))
        self._require_admin()
        queue = RequestQueue(notify=True)
        queue.init()
        if success:
            method = queue.close
        else:
            method = queue.refuse
        errcode = ErrorCode()
        for repo in self._all:
            rev = dictrev[repo]
            req = queue.get_by_node(rev)
            if req.status == "MARK":
                continue
            iret = method(requestkey(repo, req.branch, req.username))
            errcode.from_status(iret)
        return errcode

    def run(self, args=None):
        """main
        Any derivated 'run' method must describe its return code.
        0 should be used for a successfull termination,
        1 is the standard exit code of python for all errors (do not use it),
        (2 is usually used by python for syntax error).
        Convention: use a value between 100-127.
        """
        self._parse_args(args)
        return 0

    def end_message(self):
        """The end"""
        logf = logger.write_log(self._command, path=self._rootdir)
        text = logger.get_important_text()
        if not text.strip():
            return
        dest = []
        if self.opts.notify_admins:
            dest.append("admins")
        if self.opts.notify_resutest:
            dest.append("resutest")
        if self.opts.notify_user:
            dest.append("user")
        sendmail(dest, self._title, EMAIL_TEMPLATE % {"msg": text, "logf": logf})


class UpdateInstallation(BaseMaintenance):
    """Update an installation"""

    _interrupt_message = _("[{host}] update interrupted because of errors")
    _command = "update_install"
    _title = _("[{host}] {date} installation ended")

    def _set_parser(self):
        """set the optparser object for `action`"""
        super(UpdateInstallation, self)._set_parser()
        self._parser.add_option(
            "--last",
            action="store",
            metavar="FILE",
            help="file containing the last revisions installed/tested "
            "(default: None). If the revisions have not changed, "
            "nothing is done.",
        )
        self._parser.add_option(
            "--already-updated",
            dest="already_updated",
            action="store_true",
            help="do not try to pull new revisions, suppose to " "already be on the reference head",
        )
        group = self._parser.get_option_group("--resutest")
        group.add_option(
            "--runtest",
            action="store_true",
            help="run testcases after the update " "(default: False)",
        )

    def _check_last(self, alias):
        """Check if the revisions have already been installed"""
        last = self.opts.last
        logger.debug("last filename: %s", last)
        if not last or not osp.isfile(last):
            return False
        with open(last, "r") as fobj:
            dsv = fobj.read().strip()
        logger.title(_("last installed revisions: %s"), dsv)
        done = dsv == alias
        if done:
            logger.info(_("these revisions have already been installed, " "exit"))
        assert "--" not in alias, alias
        return done

    def _write_last(self, alias):
        """Write the installed revisions"""
        last = self.opts.last
        if last is None:
            return
        if osp.exists(last):
            shutil.copyfile(last, last + ".orig")
        with open(last, "w") as fobj:
            fobj.write(alias + os.linesep)
        logger.title(_("write installed revisions in {0}: {1}").format(last, alias))

    def run(self, args=None):
        """update the repositories to 'rev', build and install
        Return code:
        -   0: update done successfully,
        - 100: nothing done, last version was already installed
        """
        super(UpdateInstallation, self).run(args)
        self.lockfile = ".lock-maint-{}-{}".format(
            self.opts.branch.replace(os.sep, "-"), self.opts.builddir.replace(os.sep, "-")
        )
        return self._run()

    @locked
    def _run(self):
        """execute the feature with a lock file."""
        branch = self.opts.branch
        drev = self._dictrev(branch)
        done = self._check_last(drev["alias"])
        if done:
            self.end_message()
            return 100
        tools = osp.join(self._rootdir, "devtools")
        logger.title(_("update devtools repository"))
        self._update_repo(tools)
        self.pull_update(drev)
        self.set_waf_params()
        self.waf_command()
        if self.opts.runtest:
            self.build_testlist()
            self.run_test(self.opts.resutest, drev["alias"])
        self._write_last(drev["alias"])
        self.end_message()
        return 0


class ValidateRevision(BaseMaintenance):
    """Validate a revision"""

    _interrupt_message = _("[{host}] validation interrupted because of errors")
    _command = "validate_revision"
    _title = _("[{host}] {date} validation of revisions ended")

    def _set_parser(self):
        """set the optparser object for `action`"""
        super(ValidateRevision, self)._set_parser()
        group = self._parser.get_option_group("--root")
        group.add_option(
            "--ancestor",
            action="append",
            default=[],
            metavar="REV",
            help="ancestor revision(s) of 'src' to check in case of error "
            "(default: all ancestors between --rev et 'reference' will"
            " be tested)",
        )
        group = self._parser.get_option_group("--resutest")
        group.add_option(
            "--no-syntax",
            action="store_false",
            dest="syntax",
            default=True,
            help="do not run validation testcases in " "*syntax-only* mode.",
        )

    def _get_ancestors(self, rev):
        """Fill the list of ancestors of 'rev'"""
        srcdir = osp.join(self._rootdir, "src")
        query = "reverse(refe::%(rev)s) and not %(rev)s" % {"rev": rev}
        out = get_rev_log(srcdir, rev=query)
        res = out.split()
        logger.info(_("%4d ancestor revisions will be tested in case of " "failure"), len(res))
        return res

    def run(self, args=None):
        """validate or not the proposed changesets
        Return code:
        -   0: validation of the 'tip' revision done successfully,
        - 100: some testcases failed with the 'tip' revision.
        """
        super(ValidateRevision, self).run(args)
        self.lockfile = ".lock-maint-{}-{}".format(
            self.opts.branch.replace(os.sep, "-"), self.opts.builddir.replace(os.sep, "-")
        )
        return self._run()

    @locked
    def _run(self):
        """execute the feature with a lock file."""
        # always notify admins, but never resutest list
        self.opts.notify_admins = True
        self.opts.notify_resutest = False
        branch = self.opts.branch
        self.set_waf_params()
        drev = self._dictrev(branch)
        self.pull_update(drev, source=ASCFG.get("admin_push_uri"))
        ldict = [drev]
        if not self.opts.ancestor:
            self.opts.ancestor = self._get_ancestors(drev["src"])
        for anc in self.opts.ancestor:
            other = drev.copy()
            other["src"] = anc
            self._alias(other)
            ldict.append(other)
        drev["_main_"] = True
        success = False
        tfail = "list.not_rerun"
        if branch != "default":
            tfail += "." + branch
        logger.debug("ancestors dict: {0}".format(ldict))
        while ldict:
            logger.cancel_error()
            drev = ldict.pop(0)
            self.pull_update(drev, source=ASCFG.get("admin_push_uri"))
            self.waf_command()
            if drev.get("_main_"):
                self.build_testlist()
            resutest = self.opts.resutest if drev.get("_main_") else None
            errcode, list_err = self.run_test(resutest, drev["alias"])
            if drev.get("_main_"):
                success = not errcode.is_error()
                # do not automatically push changes
                if False:
                    if success:
                        self.push_revisions(drev)
                    self.close_request(success, drev)
                if not success:
                    # failed testcases at main will be run with previous
                    # revisions until they are all together ok
                    self._list_errors(list_err)
                    nbtest = exclude_test(self._list, tfail)
                    logger.debug("{0} errors, {1} after exclusion".format(len(list_err), nbtest))
                    if nbtest == 0:
                        # all the failures are well known, save the results
                        self.save_results(drev["alias"])
                        break
                else:
                    self.save_results(drev["alias"])
            if errcode.is_ok():
                # interrupt the loop
                break

        iret = 0
        if self.opts.syntax:
            success = success and self.test_syntax_validation() == 0
        if not success:
            logger.error(_("testcases failed"))
            iret = 100
        self.end_message()
        return iret

    def test_syntax_validation(self):
        """Run testcases in syntax-only mode."""
        args = self._rebuild_args(
            exclude_group=["--root", "--waf"],
            keep=["--root", "--builddir"],
            ignore=["--resutest", "--testlist", "--timefactor", "--no-syntax"],
        )
        args.extend(
            [
                "--resutest=NONE",
                "--testlist=validation",
                "--timefactor=0.1",
                "--success=Ok | Warn | NotTested | NotRun",
                "--args=--syntax",
            ]
        )
        logger.title(_("run validation testcases in syntax mode only"))
        retcode = RunTestcases(RunTestcases.__doc__).run(args)
        return retcode

    @log_as_important
    def save_results(self, alias):
        """Copy the results from temporary directory and add them in the
        repository of results"""
        origdir = osp.join(self._rootdir, "resu_test", alias)
        destdir = osp.join(REFDIR, "resu_test", self.opts.branch, "src")
        logger.info("moving results from '%s' to '%s'", origdir, destdir)
        clean_dir(osp.join(destdir, "*"), silent=True)
        allf = glob(osp.join(origdir, "*"))
        try:
            os.makedirs(osp.join(destdir, "flash"))
            for obj in allf:
                basn = obj.replace(origdir + "/", "")
                shutil.move(obj, osp.join(destdir, basn))
        except (IOError, OSError) as exc:
            logger.error("Error during moving result files:\n%s", str(exc))
        else:
            clean_dir(origdir)
            self._addremove_result(destdir, alias)


class RunTestcases(BaseMaintenance):
    """Run a list of testcases"""

    _interrupt_message = _("[{host}] test run interrupted because of errors")
    _command = "run_testcases"
    _title = _("[{host}] {date} testcases ended")

    def _set_parser(self):
        """set the optparser object for `action`"""
        self._notify_on_error = False
        super(RunTestcases, self)._set_parser(exclude=["repo", "build"])
        group = OptionGroup(self._parser, "If used from the 'src' repository")
        group.add_option(
            "--root",
            action="store",
            default=osp.join(os.environ["HOME"], "dev", "codeaster"),
            help="directory containing the repositories "
            "(default: {0})".format(ASCFG.get("devtools_root")),
        )
        group.add_option(
            "--builddir",
            action="store",
            default=ASCFG.get("waf.builddir"),
            metavar="DIR",
            help="alternative waf build directory "
            "(default: {0})".format(ASCFG.get("waf.builddir")),
        )
        self._parser.add_option_group(group)

        group = OptionGroup(self._parser, "To use a version out of a repository")
        group.add_option(
            "--vers",
            action="store",
            default=None,
            help="installation directory (or version name) of the version to use "
            "(default: None). For example: $HOME/install/std/share/aster",
        )
        # --mpi is deprecated, automatically detected
        group.add_option("--mpi", action="store_true", help=SUPPRESS_HELP)
        self._parser.add_option_group(group)

        group = self._parser.get_option_group("--resutest")
        group.remove_option("--resutest")
        group.add_option(
            "--resutest",
            action="store",
            metavar="DIR",
            help="directory to write the results of the testcases "
            "(relative to the current directory). Use NONE not to "
            "keep the result files.",
        )
        group.add_option(
            "--not-rerun",
            action="store_true",
            dest="not_rerun",
            help="do not rerun the testcases that have previously "
            "been executed (use the RESULTAT file present in "
            "the results directory).",
        )
        group.add_option(
            "--message",
            action="store",
            default="empty",
            metavar="MSG",
            help="message used when committing the result files " "(default: 'empty')",
        )
        group.add_option(
            "--args",
            action="append",
            metavar="ARGS",
            help="arguments passed to the code_aster executable " "(example: --args=--syntax)",
        )
        group.add_option(
            "--install-debug",
            dest="install_debug",
            action="store_true",
            help="use the debug installation",
        )
        # avoid '_' in option name
        group.add_option("--install_debug", action="store_true", help=SUPPRESS_HELP)
        group.add_option(
            "--success",
            action="store",
            metavar="DIAG",
            default=Diag.Success,
            help="[for advanced users] "
            "define the diagnostics considered as a success "
            "(default: 'Ok | Warn'). See enumerator "
            "`test_result.Diag` for details.",
        )
        group.add_option(
            "--no-retry",
            dest="no_retry",
            action="store_true",
            default=False,
            help="do not re-try to pass a testcase after a first "
            "failure (default: give a second chance).",
        )
        group.add_option(
            "--add-restart",
            dest="add_restart",
            action="store_true",
            default=False,
            help="automatically add a stage with POURSUITE/FIN "
            "at the end of each testcase (default: False).",
        )

    def run(self, args=None):
        """Run a list of testcases
        Return code:
        -   0: testcases run successfully,
        - 100: some testcases failed.
        """
        super(RunTestcases, self).run(args)
        if self.opts.mpi is not None:
            logger.warn(_("'--mpi' option is deprecated, MPI version is automatically detected."))
        if self.opts.vers is not None:
            self._rootdir = tempfile.mkdtemp(prefix="run_testcases_")
            self.c4che = {"ASTERDATADIR": self.opts.vers, "ASTER_HAVE_MPI": is_mpi(self.opts.vers)}
        else:
            self.set_waf_params()
        if self.opts.resutest is None:
            self._parser.error(_("--resutest option is required"))
        resutest = self.opts.resutest
        if resutest != "NONE":
            resutest = osp.join(osp.abspath(os.getcwd()), resutest)
            resutest = osp.normpath(resutest)
        self.build_testlist()
        prefs = {"debug": "debug" if self.opts.install_debug else "nodebug"}
        errcode = self.run_test(
            resutest,
            alias=self.opts.message,
            prefs=prefs,
            is_success=Diag.use(self.opts.success),
            reuse_previous_run=self.opts.not_rerun,
        )[0]
        iret = 0
        if errcode.is_error():
            iret = 100
        self.end_message()
        return iret


def build_cata(repopath, branch, tip):
    """Build text of catalog using the reference head of 'branch'.

    Arguments:
        repopath (str): Path to 'src' repository.
        branch (str): Branch to be used.
        tip (bool): If *True*, uses the tip of each branch. *False* by default.

    Returns:
        str: Text of the catalog.
    """
    if tip:
        head = get_rev_log(repopath, "last(branch({0}))".format(branch))
    else:
        try:
            head = get_reference_head(repopath, branch)
        except IOError:
            logger.warn(_("can not get reference head, use current revision"))
            head = "."
    shead = shortrev_s(head)
    logger.info("build cata.py for branch %s using %s", branch, shead)
    prev = os.getcwd()
    os.chdir(repopath)
    txt = []
    for sub in ("Commons", "Commands"):
        subd = osp.join("code_aster", "Cata", sub)
        out = hgcmd("cat", repopath, "--rev=%s" % head, subd)[1]
        txt.append(out)
    os.chdir(prev)
    cata = os.linesep.join(txt)
    return cata


def build_cata_dict(repo, branches, tip):
    """Build the content of the dictionnary of the code_aster keywords.

    Arguments:
        repo (str): Path to 'src' repository.
        branches (list[str]): List of branches to be used.
        tip (bool): If *True*, uses the tip of each branch. *False* by default.

    Returns:
        str: Text of the catalog.
    """
    rkw = re.compile("([A-Z]+[A-Z_]+)", re.M)
    reg_ign = re.compile("(#.*$)", re.MULTILINE)
    allkw = set()
    for branch in branches:
        txt = build_cata(repo, branch, tip)
        txt = reg_ign.sub("", txt)
        allkw.update(rkw.findall(txt))
    skw = set()
    for kw in allkw:
        skw.update(kw.split("_"))
    lkw = [kw for kw in skw if len(kw) > 1]
    lkw.sort()
    lkw.insert(0, "personal_ws-1.1 fr 0 utf-8")
    lkw.append("")
    content = "\n".join(lkw)
    return content


def make_dict(repo, instdir, branches=None, tip=False):
    """Make code_aster dictionnaries"""
    if not branches:
        branches = ASCFG.get("branches")
    fcata = osp.join(instdir, "code_aster_cata.aspell.per")
    logger.title("build catalog files")
    catadict = build_cata_dict(repo, branches, tip)
    logger.info("write %s", fcata)
    with open(fcata, "w") as fobj:
        fobj.write(catadict)


def is_mpi(version_path):
    """Tell a version is built with MPI support.

    Arguments:
        version_path (str): Path to the version (ends with "/share/aster").

    Returns:
        bool: *True* if the version supports MPI.
    """
    cfgfile = osp.join(version_path, "config.yaml")
    if osp.exists(cfgfile):
        with open(cfgfile, "r") as fcfg:
            content = yaml.load(fcfg)
    else:
        cfgfile = osp.join(version_path, "config.json")
        if osp.exists(cfgfile):
            with open(cfgfile, "r") as fcfg:
                content = json.load(fcfg)
        else:
            # legacy version, asrun needed
            config = get_asrun_config(version_path)
            return "_USE_MPI" in config.get_defines()
    return bool(content.get("parallel"))
