# coding: utf-8

"""
Utilities to extract the responsabilities from the source code, the bugtracker or
the documentation.
"""


import os.path as osp
import pickle
import re
from collections import UserDict
from glob import glob
from urllib.error import URLError
from urllib.request import urlopen
import csv

from aslint.logger import logger
from aslint.i18n import _
from aslint.config import ASCFG
from aslint.utils import apply_in_parallel, get_proc_numb
from aslint.string_utils import convert, search_similar
from aslint.common_checkers import CMT
import api_roundup.stats as ST

RE_PERSON = re.compile("^{0} *(person_in_charge *:) *"
                       r"(?P<main>.*?)(?:| *\*/) *$".format(CMT), re.M)
RE_AT = re.compile(" +at +", re.I)


class RespEntity(UserDict):
    """Object storing the responsability of an entity of code_aster"""

    _keys = ('type', 'id', 'doc', 'address', 'organisation')
    _strFormat = "{type:<12} {id:<32} {doc:<10} {address:<32} {organisation}"

    def key(self):
        """Primary key"""
        return (self["type"], self["id"])

    def _values(self):
        """Return the values (not null)"""
        args = self.copy()
        for key, value in list(args.items()):
            if not value:
                args[key] = "''"
            args[key] = convert(args[key])
        return args

    @classmethod
    def getKeys(cls):
        """Return the ordered list of keys"""
        return cls._keys

    @classmethod
    def title(cls):
        """Return the title line for 'str' representation"""
        args = dict([(i, i) for i in cls._keys])
        return cls._strFormat.format(**args)

    def str(self):
        """Return a representation of a responsability"""
        return self._strFormat.format(**self._values())


class Responsabilities(object):
    """Singleton object that stores all the responsabilities"""

    def __init__(self, rootdir, force=False):
        """Initialization"""
        self.rootdir = rootdir
        self.force = force
        self.numthread = get_proc_numb()
        self.entities = {}
        self.users = None
        self.usersByAddr = None
        self.docs = None

    def _extractPerson(self, type, pattern):
        """Extract the persons in charge of commands"""
        assert None not in (self.users, self.usersByAddr, self.docs)
        files = glob(pattern)
        result = apply_in_parallel(extract_person_in_file, files,
                                   self.numthread)
        count = 0
        known_addrs = list(self.usersByAddr.keys())
        for fname, addr in zip(files, result):
            for email in addr:
                user = self.usersByAddr.get(email, {})
                if not user:
                    near = search_similar(email, known_addrs, 0.1)
                    if near:
                        logger.warn(_("fix {0} by {2}, found in {1}") \
                            .format(email, fname, near))
                        email = near
                        user = self.usersByAddr[email]
                item = RespEntity()
                item["id"] = osp.splitext(osp.basename(fname))[0]
                if type == "test":
                    item["id"] = get_testcase_name(item["id"]) or item["id"]
                item["type"] = type
                item["doc"] = self.docs.get(item.key(), {}).get('key')
                item["address"] = email
                if not user:
                    logger.warn(_("unknown address in rex: {0} found in {1}") \
                        .format(email, fname))
                item["organisation"] = user.get('organisation')
                self.entities[item.key()] = item
                count += 1
        logger.debug("{0:6} found / {1:6} files".format(count, len(files)))
        return count

    def extractCommand(self):
        """Extract the persons in charge of commands"""
        logger.info("extracting from commands...")
        self._extractPerson(
            "command",
            osp.join(self.rootdir, "src", "code_aster", "Cata", "Commands",
                     "*.py"))
        self._extractPerson(
            "command",
            osp.join(self.rootdir, "src", "code_aster", "Cata", "Commons",
                     "*.py"))

    def extractTestcase(self):
        """Extract the persons in charge of testcases"""
        logger.info("extracting from testcases...")
        self._extractPerson(
            "test",
            osp.join(self.rootdir, "src", "astest", "*.comm"))
        self._extractPerson(
            "test",
            osp.join(self.rootdir, "validation", "astest", "*.comm"))

    def extractBehaviour(self):
        """Extract the persons in charge of behaviours"""
        logger.info("extracting from behaviours...")
        self._extractPerson(
            "behaviour",
            osp.join(self.rootdir, "src", "bibpyt", "Comportement", "*.py"))

    def extractModel(self):
        """Extract the persons in charge of models"""
        logger.info("extracting from models...")
        self._extractPerson(
            "model",
            osp.join(self.rootdir, "src", "catalo", "cataelem", "Elements",
                     "*.py"))

    def extractOption(self):
        """Extract the persons in charge of options"""
        logger.info("extracting from options...")
        self._extractPerson(
            "option",
            osp.join(self.rootdir, "src", "catalo", "cataelem", "Options",
                     "*.py"))

    def extractDatastructure(self):
        """Extract the persons in charge of datastructures"""
        logger.info("extracting from datastructures...")
        self._extractPerson("datastructure",
                            osp.join(self.rootdir, "src", "bibpyt", "SD",
                                     "*.py"))

    def extractFromDoc(self):
        """Extract the persons in charge from published documents"""
        logger.info("extracting from published documents...")
        for key, docinfo in list(self.docs.items()):
            item = RespEntity()
            item["id"], item["type"] = key
            item["doc"] = docinfo["key"]
            known_users = list(self.users.keys())
            username = docinfo["responsible"]
            user = self.users.get(username)
            if not user:
                near = search_similar(username, known_users, 0.2)
                if near:
                    logger.info(_("use similar username: {0} (doc) "
                                  "vs {1} (rex)").format(username, near))
                    username = near
                    user = self.users[username]
            if not user:
                logger.warn(_("unknown username in rex: {0} for {1} ({2})") \
                    .format(username, item["id"], docinfo["key"]))
            else:
                item['address'] = user['address']
                item["organisation"] = user['organisation']
            exist = self.entities.get(item.key(), {})
            if exist:
                new = (user or {}).get('address')
                if exist['address'] != new:
                    logger.warn(_("source and doc mismatch: {0} ({1}, {2}): "
                                  "{3} vs {4}")\
                        .format(item["id"], item["type"], item['doc'],
                                exist['address'], new))
                # assert exist['organisation'] == item['organisation']
            exist.update(item)

    def extractAll(self):
        """Extract the persons in charge"""
        if not self.users:
            self.users, self.usersByAddr = setUsers(self.force)
        if not self.docs:
            self.docs = setDoc()
        self.extractCommand()
        self.extractTestcase()
        self.extractBehaviour()
        self.extractModel()
        self.extractOption()
        self.extractDatastructure()
        self.extractFromDoc()

    def show(self):
        """Show the list of responsabilities by type"""
        keys = sorted(self.entities.keys())
        print(RespEntity.title())
        for i in keys:
            print(self.entities[i].str())

    def writeCSV(self, filename):
        """Write a CSV file of the responsabilities"""
        keys = sorted(self.entities.keys())
        with open(filename, 'w') as fobj:
            writer = csv.writer(fobj, delimiter=";", quoting=csv.QUOTE_MINIMAL)
            writer.writerow(RespEntity.getKeys())
            for i in keys:
                item = self.entities[i]
                row = [convert(item[k]) for k in RespEntity.getKeys()]
                writer.writerow(row)

    def applyFilter(self, filter):
        """Apply a filter on entities"""
        new = {}
        for key, item in list(self.entities.items()):
            context = item.copy()
            if eval(filter, {}, context):
                new[key] = item
        self.entities = new


def extract_person_in_text(text):
    """Return a list of the persons in charge found in text"""
    pers = []
    for mat in RE_PERSON.finditer(text):
        for addr in mat.group('main').split(","):
            addr = RE_AT.sub("@", addr.strip())
            pers.append(addr.lower())
    return pers


def extract_person_in_file(filename):
    """Return a list of the persons in charge found in a file"""
    return extract_person_in_text(open(filename, 'r').read())


# see generate_html module from docaster repository
def get_testcase_name(title):
    """Extract a testcase name from a title"""
    mat = re.search('([A-Z]{3,5}[0-9]+)', title, flags=re.I)
    if mat:
        return mat.group(1)
    return None


def setUsers(force=False):
    """Get the users informations"""
    users = ST.get_users(force)
    usersByAddr = {}
    for user in list(users.values()):
        user['address'] = (user['address'] or "").lower()
        usersByAddr[user['address']] = user
    return users, usersByAddr


def setDoc():
    """Get the document informations"""
    url = ("http://{0}/published_docs_default.pick"
           .format(ASCFG.get('doc.mirror')))
    try:
        url = urlopen(url, timeout=3)
        data = pickle.loads(url.read())
    except URLError as exc:
        logger.error(_("connection failed (url: %(url)s):\n"
                       "%(exc)s"), {'url': url, 'exc': str(exc)})
        raise RuntimeError(_("interrupted because of previous errors"))
    docs = {}
    for key, docinfo in list(data.items()):
        key = key.lower()
        docinfo['key'] = key
        title = docinfo['title']
        name = (docinfo['name'] or '').lower()
        if key[:2] in ('u4', 'u7'):
            docs[name.lower(), "command"] = docinfo
        elif key.startswith('v') and key != "v0.00.00":
            docs[name.lower(), "test"] = docinfo
    return docs
