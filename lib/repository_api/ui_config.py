# coding=utf-8

"""
This module helps for migrating scripts to git.
It emulates some features of `mercurial.ui`.
"""


import os
import os.path as osp

from configparser import ConfigParser

_booleans = {
    "1": True,
    "yes": True,
    "true": True,
    "on": True,
    "always": True,
    "0": False,
    "no": False,
    "false": False,
    "off": False,
    "never": False,
    True: True,
    False: False,
}


class ui(object):
    """Object that reads config files."""

    _cfg = None

    def __init__(self, force=False):
        if not ui._cfg or force:
            ui._cfg = self._configparser()

    def config(self, section, name, default=None):
        """Return a value from the configuration."""
        name = name.replace(".", "-").replace("_", "-")
        if self._cfg.has_option(section, name):
            return self._cfg.get(section, name)
        else:
            return default

    def configbool(self, section, name, default="no"):
        """Return a boolean value from the configuration."""
        value = self.config(section, name, default)
        vbool = _booleans.get(value, None)
        if vbool is None:
            raise ValueError("{0}.{1} is not a boolean ('{2}')".format(section, name, value))
        return vbool

    def username(self):
        """Return the 'username <email>' value."""
        name = self.config("user", "name", "unknown")
        email = self.config("user", "email", "address")
        return "%s <%s>" % (name, email)

    def prompt(self, message, default=None):
        """Read a value from keyboard."""
        answer = input(message)
        return answer or default

    @staticmethod
    def _configparser():
        """Read git config files."""
        # strict=False will ignore duplicated sections (may at least occur with fetch)
        cfg = ConfigParser(strict=False)
        cfg.read([osp.join(os.environ.get("HOME", ""), ".gitconfig"), osp.join(".git", "config")])
        return cfg

    def setconfig(self, *args):
        raise NotImplementedError("To be removed!")
