# coding=utf-8

"""
This module partially emulates `mercurial.hg` features.
"""


import os.path as osp

from .hg_utils import hg_get_rev_log, hgcmd


class ChangeContext(object):
    """Represents a revision."""

    def __init__(self, repopath, rev="INIT"):
        self._parent = repopath
        output = hg_get_rev_log(
            self._parent, rev, fmt="{rev}X!X{node}X!X{phase}" "X!X{author}X!X{desc}"
        )
        (self._rev, self._hex, self._phase, self._user, self._desc) = output.split("X!X")
        self._rev = int(self._rev)
        self._branch = None

    def rev(self):
        """Return the changeset revision number."""
        return self._rev

    def hex(self):
        """Return the sha1 of the revision."""
        return self._hex

    def phase(self):
        """Return the phase of the revision."""
        return self._phase

    def user(self):
        """Return the user who created the revision."""
        return self._user

    def description(self):
        """Return the revision log message."""
        return self._desc

    def ancestor(self, other):
        """Return the common ancestor between 'self' and 'other'."""
        sha1 = hgcmd(
            "log", self._parent, rev="ancestor({}, {})".format(self._hex, other), template="{node}"
        )[1]
        return ChangeContext(self._parent, sha1)

    def children(self):
        """Return children of the revision."""
        sha1 = hgcmd(
            "log", self._parent, rev="children({0})".format(self._hex), template="{node}\n"
        )[1]
        return [ChangeContext(self._parent, i) for i in sha1.split()]

    def branch(self):
        if self._branch is None:
            self._branch = hgcmd("log", self._parent, rev=self._hex, template="{branch}\n")[1]
        return self._branch

    def __eq__(self, other):
        """Same changeset?"""
        return self.hex() == other.hex()

    def __ne__(self, other):
        """Different changeset?"""
        return not (self == other)

    def __str__(self):
        """Representation of the changeset == its sha1."""
        return self._hex


class Repository(object):
    """Emulates the `mercurial.hg.repository` object."""

    def __init__(self, path):
        self._path = osp.abspath(path)
        if not osp.isdir(osp.join(path, ".hg")):
            raise IOError("not a hg repository: {}".format(path))

    @property
    def root(self):
        """Attribute that holds the repository root directory."""
        return self._path

    def __getitem__(self, rev):
        """Return a revision"""
        return ChangeContext(self.root, rev)


def repository(_, path):
    """For compatibility with `mercurial.hg.repository`."""
    return Repository(path)
