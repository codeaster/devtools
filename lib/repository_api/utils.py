# coding=utf-8

"""Generic utilities."""

import os.path as osp
from functools import wraps
from subprocess import PIPE, run

from aslint.logger import logger
from aslint.string_utils import convert


def repository_type(path):
    """Return the type of repository in *path*."""
    if osp.isdir(osp.join(path, ".git")):
        return "git"
    if osp.isdir(osp.join(path, ".hg")):
        return "hg"
    raise TypeError("unknown repository (%s)" % path)


def repository_switch(func):
    """Decorator that adds the repository type as first argument.
    Decorated function arguments start with : ``repotype, repopath, ...`.
    """

    @wraps(func)
    def wrapper(*args, **kwds):
        """wrapper"""
        return func(repository_type(args[0]), *args, **kwds)

    return wrapper


def getcmd(repotype, repopath, cmd, *args, **opts):
    """Return the command line.

    Returns:
        list: Command line arguments.
    """
    assert repotype in ("git", "hg")
    if "message" in opts:
        opts["message"] = repr(convert(opts["message"]))

    if isinstance(cmd, tuple):
        cmd = list(cmd)
    elif not isinstance(cmd, list):
        cmd = [cmd]
    cmd.extend(args)
    for key, value in list(opts.items()):
        if value in (None, False):
            continue
        if value is True:
            cmd.append("--{0}".format(key))
        else:
            cmd.append("--{0}={1}".format(key, value))

    cmd.insert(0, repotype)
    if repopath:
        cmd.insert(1, "-C" if repotype == "git" else "-R")
        cmd.insert(2, repopath)
    return cmd


@repository_switch
def repocmd(repotype, repopath, cmd, *args, **opts):
    """Return the output of `cmd`.

    Returns:
        (int, str): Exit code and output of the command.
    """
    return shell_cmd(getcmd(repotype, repopath, cmd, *args, **opts))


def shell_cmd(cmd):
    """Return the output of `cmd` using an external shell."""
    logger.debug(cmd)
    proc = run(cmd, stdout=PIPE, stderr=PIPE, universal_newlines=True)
    return proc.returncode, proc.stdout.strip()
