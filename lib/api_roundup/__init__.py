# coding=utf-8

"""
This package gives the functions to interact with REX Code_Aster.
"""

from .rex import (build_histor, check_status, get_connection, get_documents,
                  get_expected_issues, get_solved_issues_eda, mark_as_closed,
                  validate_issue)
