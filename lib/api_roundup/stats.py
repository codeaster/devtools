#!/usr/bin/env python3
# coding: utf-8

"""
Helper functions and objects to make a statistics report
"""


import os
import os.path as osp
import pickle
import re
from collections import UserDict
from datetime import datetime
import time
from glob import glob

from aslint.logger import logger
from aslint.i18n import _
from aslint.config import ASCFG
from aslint.utils import convert
from .rex import extract_csv
from .utils import read_issue_changelog

ASTER_TEAM = "|".join(
    [
        "abbas",
        "bereux",
        "courtois",
        "delmas",
        "kudawoo",
        "fournier",
        "desoza",
        "lefebvre",
        "pellet",
        "sellenet",
        "drouet",
        "assire",
        "durand",
        "lebouvier",
        "rezette",
        "macocco",
        "chansard",
        "ladier",
        "cheignon",
        "lecorvec",
        "bourcet",
    ]
)

CACHE_USERS = osp.join(ASCFG.get("devtools_cache"), "rex_users.pick")


class RexEntity(UserDict):
    """A REX entity as dict for statistics"""

    def str(self):
        """Return a representation of the issue content"""
        args = self.copy()
        for key, value in list(args.items()):
            if not value:
                args[key] = "''"
        return os.linesep.join(
            [
                "Issue {id}, {type} ({status}) created at {creation} by {creator} "
                "for {produit} {version} (target {versCible})",
                "Title: {title}",
                "Solved by {assignedto} (last changes {activity}) for "
                "project {projet} in {verCorrVexpl} and {verCorrVdev}, "
                "cost {nbJours} days",
            ]
        ).format(**args)

    def summary_reminder(self):
        """Return a representation of the issue content"""
        return os.linesep.join(
            [
                "Fiche {id}, {type} créée le {creation} "
                "par {creator} (version cible {versCible})",
                "    Titre: {title}",
                "    " + ASCFG.get("rex_url") + "/issue{id}",
            ]
        ).format(**self)


class EntityList(UserDict):
    """A list of REX entities, but store as a dict"""

    def __or__(self, other):
        """Return issues existing in self or in other"""
        union = self.copy()
        union.update(other)
        return union

    def __and__(self, other):
        """Return issues existing in self and in other"""
        common = set(self.keys()).intersection(list(other.keys()))
        return EntityList(dict([(id, self[id]) for id in common]))

    def filter_equal(self, key, value):
        """Filter the list, keep issues where 'key' is equal to 'value'"""
        return EntityList(dict([(id, issue) for id, issue in self.items() if issue[key] == value]))

    def filter_not_equal(self, key, value):
        """Filter the list, keep issues where 'key' is not equal to 'value'"""
        return EntityList(dict([(id, issue) for id, issue in self.items() if issue[key] != value]))

    def filter_less(self, key, value):
        """Filter the list, keep issues where 'key' is less than 'value'"""
        return EntityList(dict([(id, issue) for id, issue in self.items() if issue[key] < value]))

    def filter_greater(self, key, value):
        """Filter the list, keep issues where 'key' is greater than 'value'"""
        return EntityList(dict([(id, issue) for id, issue in self.items() if issue[key] > value]))

    def filter_regexp(self, key, regexp):
        """Filter the list, keep issues where 'key' value match a
        regular expression"""
        exp = re.compile(regexp)
        return EntityList(
            dict([(id, issue) for id, issue in self.items() if exp.search(issue[key])])
        )

    def filter_type(self, typ):
        """Filter the list of issues by type"""
        if typ in ("all", "*"):
            by_type = self.copy()
        elif typ == "anomalie":
            by_type = self.filter_equal("type", "anomalie") | self.filter_equal("type", "express")
        elif typ == "anomalie-vdev":
            by_type = (
                (self.filter_equal("type", "anomalie") | self.filter_equal("type", "express"))
                & self.not_empty("verCorrVdev")
                & self.empty("verCorrVexpl")
            )
        else:
            by_type = self.filter_equal("type", typ)
        return by_type

    def empty(self, key):
        """Filter the list, keep issues where 'key' value is empty"""
        return EntityList(dict([(id, issue) for id, issue in self.items() if not issue[key]]))

    def not_empty(self, key):
        """Filter the list, keep issues where 'key' value is empty"""
        return EntityList(dict([(id, issue) for id, issue in self.items() if issue[key]]))

    def deepcopy(self):
        """Create a new EntityList with the copy of each issue"""
        return EntityList(dict([(id, issue.copy()) for id, issue in self.items()]))

    def add_delay(self, closure_date):
        """Add a field containing the elapsed time between creation and
        closure (or last activity if closure is not known)"""
        for issue in self.values():
            creat = issue["creation"]
            end = closure_date.get(int(issue["id"]), issue["activity"])
            issue["_closure"] = end
            # offset between eda and changelog date
            issue["_delay"] = max(0, (str2date(end) - str2date(creat)).days - 7)

    def add_delay_reminder(self):
        """Add a field containing the elapsed time since the last activity
        on a issue"""
        for issue in self.values():
            start = issue["activity"]
            end = time.strftime("%Y-%m-%d")
            issue["_delay"] = (str2date(end) - str2date(start)).days

    def getValues(self, key):
        """Return unique values of a field"""
        return list(set([issue[key] for issue in self.values()]))


def csv_as_dict(content, header=1, delimiter=",", encoding=None):
    """Read a CSV file and return a list of dicts"""
    import csv

    keys = []
    rows = []
    lines = content.splitlines()
    reader = csv.reader(lines, delimiter=delimiter)
    for values in reader:
        if encoding:
            values = [str(v, encoding) for v in values]
        if reader.line_num == 1:
            keys = values
            continue
        if reader.line_num <= header:
            logger.info(_("Ignore line #{0}: {1}").format(reader.line_num, delimiter.join(values)))
            continue
        try:
            values = [v.strip() for v in values]
            drow = dict(list(zip(keys, values)))
            rows.append(drow)
        except:
            logger.error(
                _("Error line #{0}:\n keys: {1}\n values: {2}").format(
                    reader.line_num, keys, values
                )
            )
    logger.info(_("{0:4} lines read").format(len(rows)))
    return keys, rows


def download(filename, entity):
    """Extract the data from REX and write a pickled file containing the
    content as a list of dicts."""
    csv = extract_csv(entity)
    _, rows = csv_as_dict(csv)
    rows = [i for i in rows if i]
    issues = EntityList()
    for iss in rows:
        issues[iss["id"]] = RexEntity(iss)
    write_dict(filename, issues)


# downloading all users infos need admin access
def get_users(force=False):
    """Return the list of all users"""
    if not force and osp.isfile(CACHE_USERS):
        with open(CACHE_USERS, "rb") as pick:
            users = pickle.load(pick)
        logger.info(_("{1:6} users read from {0}").format(CACHE_USERS, len(users)))
    else:
        from . import get_connection

        with get_connection() as rex:
            usernames = rex.list("user")
        users = EntityList()
        for dvp in sorted(usernames):
            logger.debug(_("reading user {0}...").format(dvp))
            users[dvp] = download_user(dvp)
        with open(CACHE_USERS, "wb") as pick:
            pickle.dump(users, pick)
    return users


def download_user(username):
    """Return the users"""
    from . import get_connection

    with get_connection() as server:
        userid = server.filter("user", None, {"username": username})
        match_users = [RexEntity(server.display("user" + id)) for id in userid]

    match_users = [user for user in match_users if user["username"] == username]
    if not match_users:
        raise ValueError("user not found: {0}".format(username))
    assert len(match_users) == 1, match_users
    return match_users[0]


def write_dict(filename, issues):
    """Write the REX data into a pickle file"""
    _clean_dict(issues)
    with open(filename, "wb") as pick:
        pickle.dump(issues, pick)
    logger.info(_("data written into {0}").format(filename))


def read_dict(filename):
    """Read a pickled file containing the REX data"""
    with open(filename, "rb") as pick:
        issues = pickle.load(pick)
    logger.info(_("{1:6} issues read from {0}").format(filename, len(issues)))
    return issues


def _clean_dict(issues):
    """Clean the data"""
    re_date = re.compile("([0-9]{4}\-[0-9]{2}\-[0-9]{2})\.")
    for line in list(issues.values()):
        for key, value in list(line.items()):
            # remove working fields
            if key.startswith("_"):
                del line[key]
                continue
            if value.strip() in ("None", ""):
                line[key] = ""
                continue
            mat = re_date.search(value)
            if mat:
                line[key] = mat.group(1)


def str2date(date):
    """Convert a date from format "Y-m-d" to a datetime object"""
    return datetime(*[int(i) for i in date.split("-")])


def _diff_days(date1, date2):
    """Return the number of days between date1 and date2"""
    return (str2date(date2) - str2date(date1)).days


def tofloat(string):
    """Convert the argument to float"""
    if type(string) is float:
        return string
    return float(string.replace(",", ".").strip() or 0)


def centile(values, k):
    """Return the k-th centile of the values (k in %)"""
    if k >= 100:
        k = 99.99
    values = sorted(values)
    if len(values) == 0:
        values = [0]
    return values[int(k * len(values) / 100)]


def sum_days(issues, sumfield="nbJours"):
    """Return the sum of days of work"""
    return sum([tofloat(i[sumfield]) for i in issues.values()])


def extract_closure(pattern):
    """Extract closure date of issues from changelog files"""
    re_date = re.compile(r"([0-9]{4}\-[0-9]{2}\-[0-9]{2})")
    closure = {}
    for fname in glob(pattern):
        with open(fname, "rb") as fobj:
            txt = convert(fobj.read())
        mat = re_date.search(txt)
        if not mat:
            continue
        date = mat.group(1)
        issues = read_issue_changelog(fname)
        closure.update(dict.fromkeys(issues, date))
    return closure


def summary(issues, title, sumfield="nbJours", with_team=True):
    """Print a summary of issues"""
    nb = max(1, len(issues))
    delay = [i["_delay"] for i in issues.values()]
    mean_delay = 1.0 * sum(delay) / nb
    nbday = sum_days(issues, sumfield)
    mean_nbday = nbday / nb
    by_dvp = issues.filter_regexp("creator", ASTER_TEAM)
    print(title)
    print(
        (
            "{0:5} fiches / {1:6.1f} j, {2:6.1f} par fiche, delai moyen {3:6.1f} "
            "(80% < {4} j/ 90% < {5} j)"
        ).format(len(issues), nbday, mean_nbday, mean_delay, centile(delay, 80), centile(delay, 90))
    )
    if with_team:
        print("dont {0:5} fiches émises par les développeurs").format(len(by_dvp))


def developer_stats(issues, title, sumfield="nbJours"):
    """Statistics by developer"""
    list_dvp = sorted(issues.getValues("assignedto"))
    if title:
        print(title)
    for dvp in list_dvp:
        solved = issues.filter_equal("assignedto", dvp)
        if len(solved) == 0:
            continue
        nb = max(1, len(solved))
        delay = [i["_delay"] for i in solved.values()]
        mean_delay = 1.0 * sum(delay) / nb
        nbday = sum_days(solved, sumfield)
        mean_nbday = nbday / nb
        dvp = dvp or "-"
        print(
            (
                "{0:5} fiches pour {1:6.1f} j à {6:10}, {2:6.1f} par fiche, delai moyen {3:6.1f} "
                "(50% < {4} j/ 80% < {5} j)"
            ).format(
                len(solved),
                nbday,
                mean_nbday,
                mean_delay,
                centile(delay, 50),
                centile(delay, 80),
                dvp,
            )
        )


def project_stats(issues, title, sumfield="nbJours"):
    """Statistics by project"""
    list_project = sorted(issues.getValues("projet"))
    if title:
        print(title)
    total = sum_days(issues, sumfield)
    print("Nb jours total : {0:6.1f}".format(total))
    for proj in list_project:
        solved = issues.filter_equal("projet", proj)
        _project_summary(proj, solved, total)
    psm = (
        issues.empty("projet")
        | issues.filter_equal("projet", "AOM")
        | issues.filter_regexp("projet", "^ASTER")
        | issues.filter_equal("projet", "Bonnes idées")
        | issues.filter_equal("projet", "CODE")
        | issues.filter_equal("projet", "VALIDATION")
    )
    _project_summary("projet PSM", psm, total)
    p3m = (
        issues.filter_equal("projet", "ISTA")
        | issues.filter_equal("projet", "Méthodes Numériques")
        | issues.filter_equal("projet", "5% AMA")
    )
    _project_summary("projet 3M", p3m, total)


def extern_stats(issues):
    """Statistics by external partner"""
    list_ext = sorted(issues.getValues("intervenant"))
    for tma in list_ext:
        if not tma:
            continue
        solved = issues.filter_equal("intervenant", tma)
        nbday = sum_days(solved)
        nbreal = sum_days(solved, "realiseCharge")
        print(
            "Fiches {0} (vérif nbJours = {1:6.1f} / realiseCharge = {2:6.1f})".format(
                tma, nbday, nbreal
            )
        )
        summary(solved, "Type = all", sumfield="realiseCharge", with_team=False)
        developer_stats(solved, "", sumfield="realiseCharge")


def _project_summary(project, issues, total, sumfield="nbJours"):
    """Print a summary line for a project"""
    nbday = sum_days(issues, sumfield)
    project = project or "-"
    ratio = nbday / total * 100
    print(
        "{0:5} fiches pour {1:25}, soit {2:6.1f} j, {3:5.1f} %".format(
            len(issues), project, nbday, ratio
        )
    )
