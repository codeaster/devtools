# coding=utf-8

"""
Tests for the Docaster API.
"""

import os.path as osp
import unittest

from hamcrest import *
from project.responsability import extract_person_in_file, extract_person_in_text
from testutils import CODEASTER_SRC


def test01_extract():
    """check extraction"""
    text = "! person_in_charge: nicolas.sellenet at edf.fr, a.b@edf.fr"
    email = extract_person_in_text(text)
    assert_that(email, has_length(2))
    assert_that(email, contains("nicolas.sellenet@edf.fr", "a.b@edf.fr"))


def test02_extract():
    """check extraction in file"""
    email = extract_person_in_file(
        osp.join(CODEASTER_SRC, "code_aster", "Cata", "Commands", "debut.py")
    )
    assert_that(email, contains("j-pierre.lefebvre@edf.fr"))


if __name__ == "__main__":
    import sys
    from testutils import get_test_suite

    RET = unittest.TextTestRunner(verbosity=2).run(get_test_suite(__name__))
    sys.exit(not RET.wasSuccessful())
