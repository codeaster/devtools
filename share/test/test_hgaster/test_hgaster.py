# coding=utf-8

"""Unittests of the hgaster package"""
# needs: install_env & hooks

import os
import os.path as osp
import shutil
import sys
import tempfile
import unittest
from subprocess import PIPE, STDOUT, Popen, call

from aslint.config import ASCFG
from aslint.logger import DEBUG, WARN, logger
from aslint.string_utils import convert
from testutils import REPO, only_on_repository

from hgaster.ext_utils import is_admin


class TestHgaster(unittest.TestCase):

    """cross-modules test"""

    @only_on_repository("hg")
    def setUp(self):
        """setup two repositories: codeaster/src and integration/src as clones
        of devtools."""
        tmp = self.tmp = tempfile.mkdtemp(prefix="TestHgaster_")
        self.userrepo = osp.join(tmp, "codeaster")
        self.intgrepo = osp.join(tmp, "integration")
        self.ref = osp.join(self.intgrepo, "src")
        self.src = osp.join(self.userrepo, "src")
        self.prev = os.getcwd()
        os.chdir(tmp)
        os.makedirs(self.userrepo)
        os.makedirs(self.intgrepo)
        os.chdir(self.intgrepo)
        iret = call(
            " && ".join(
                [
                    "echo Init working repository...",
                    "hg clone {devtools} --rev 0 src",
                    "cd src",
                    "echo n | {devtools}/bin/install_env > /dev/null 2>&1",
                    "echo not_empty > mark",
                    "hg add mark",
                    "sed -i 's/^pretxncommit.check_commit/#pretxncommit.check_commit/g' .hg/hgrc",
                    "hg ci -m '[#0000] init repo'",
                    "hg tag unstable -m '[#0000] init unstable'",
                    "sed -i 's/^#pretxncommit.check_commit/pretxncommit.check_commit/g' .hg/hgrc",
                    # "cat .hg/hgrc"
                ]
            ).format(devtools=REPO),
            shell=True,
            stdout=PIPE,
        )
        assert iret == 0, iret
        os.chdir(self.userrepo)
        iret = os.system(
            " && ".join(
                [
                    "hg clone ../integration/src > /dev/null",
                    "echo n | {devtools}/bin/install_env src > /dev/null 2>&1",
                ]
            ).format(devtools=REPO)
        )
        assert iret == 0, iret
        os.chdir(self.prev)
        # logger.setLevel(DEBUG)
        logger.setLevel(WARN)
        logger.info(" ")

    @only_on_repository("hg")
    def tearDown(self):
        """cleanup function"""
        os.chdir(self.prev)
        if logger.getEffectiveLevel() <= DEBUG:
            os.system("echo && cd %s && hg glog" % self.ref)
        if True:
            shutil.rmtree(self.tmp)
        else:
            sys.stdout.write("\nworking directory: %s\n" % self.tmp)

    def _write(self, fname, cnt=""):
        """write txt in fname"""
        txt = "subroutine hgatst(dummy)\ninteger dummy\n%s\nend\n" % cnt
        with open(fname, "w") as fobj:
            fobj.write(txt)

    def _addfile(self, fname):
        """add a file"""
        return call(["hg", "add", fname], stdout=PIPE, stderr=STDOUT)

    def _commit(self, message):
        """commit"""
        return call(["hg", "commit", "-m", message], stdout=PIPE, stderr=STDOUT)

    @only_on_repository("hg")
    def test01(self):
        """simple commands"""
        os.chdir(self.ref)
        proc = Popen(["hg", "status"], stdout=PIPE, stderr=STDOUT)
        out = convert(proc.communicate()[0])
        assert proc.returncode == 0, proc.returncode
        # no change, no error message
        assert out.strip() == "", out

    @only_on_repository("hg")
    def test02_pretxncommit(self):
        """test pretxncommit hook"""
        from repository_api import UI

        ui = UI.ui()
        adm = is_admin(ui.username())
        warn = ui.config("aster", "check.commit", "error") != "error"
        os.chdir(self.src)
        self._write("file.F90", "")
        iret = self._addfile("file.F90")
        assert iret == 0, iret
        # branch name: all accepted now
        iret = self._commit("[#1111] authorized to commit in default branch")
        assert iret == 0, iret

        iret = call(["hg", "branch", "--force", "edf/unittest"], stdout=PIPE, stderr=STDOUT)
        assert iret == 0, iret
        iret = self._commit("[#1111] add a simple file")
        assert iret == 0, iret

        # branch name: all accepted now
        iret = call(["hg", "branch", "mybranch_name"], stdout=PIPE, stderr=STDOUT)
        assert iret == 0, iret
        self._write("file.F90", "dummy = dummy + 1")
        iret = self._commit("[#2222] now accepted")
        assert iret == 0 or adm or warn, iret

    @unittest.skipIf(not ASCFG.get("admin._test"), "only works in test mode")
    def test03_submit(self):
        """"""
        os.chdir(self.src)
        self._write("file.F90", "")
        iret = self._addfile("file.F90")
        assert iret == 0, iret
        iret = self._commit("[#1111] add a simple file")
        assert iret == 0, iret
        wafc4che = osp.join(self.src, ASCFG.get("waf.builddir"), "c4che", "release_cache.py")
        os.makedirs(osp.dirname(wafc4che))
        c4che = os.linesep.join(
            [
                'PREFIX = ""',
                'BINDIR = ""',
                'ASTERDATADIR = ""',
                'PYTHONARCHDIR = ""',
                'PYTHON = [""]',
                'INCLUDES = ""',
                'FCFLAGS = ""',
            ]
        )
        with open(wafc4che, "w") as fobj:
            fobj.write(c4che)
        cmd = ["submit", "--dry-run", "--local"]
        kwargs = {}
        if logger.getEffectiveLevel() <= DEBUG:
            cmd.append("--debug")
        else:
            cmd.append("--quiet")
            kwargs.update({"stdout": PIPE, "stderr": PIPE})
        iret = call(cmd, **kwargs)
        assert iret == 0, "only works with admin._test"


if __name__ == "__main__":
    import sys

    from testutils import get_test_suite

    RET = unittest.TextTestRunner(verbosity=2).run(get_test_suite(__name__))
    sys.exit(not RET.wasSuccessful())
