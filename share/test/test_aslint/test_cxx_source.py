# coding=utf-8

"""
Unittests for C++ checkers
"""

import unittest

import cxx_samples as CODE
from aslint.other import cxx_checkers
from hamcrest import *
from testutils.lint_test import check_one_file, check_snippet


def test00_interface():
    chk = cxx_checkers.MissingConstructor
    with check_snippet(CODE.INTERFACE_ERROR, "Interface.cxx") as filename:
        report = check_one_file(filename, checker=chk)
        assert_that(report.hasError())
        assert_that(report[filename], has_length(1))
        assert_that(report[filename][0].id, equal_to(chk.id))

    with check_snippet(CODE.INTERFACE_OK, "Interface.cxx") as filename:
        report = check_one_file(filename, checker=chk)
        assert_that(not report.hasError())


if __name__ == "__main__":
    import sys

    from testutils import get_test_suite

    RET = unittest.TextTestRunner(verbosity=2).run(get_test_suite(__name__))
    sys.exit(not RET.wasSuccessful())
