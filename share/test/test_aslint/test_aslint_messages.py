# coding=utf-8

"""
Tests for aslint messages.
"""


# do not add external dependencies
import os
import sys
import unittest


def setUp():
    """build checklist"""
    import aslint.base_checkers as BCH
    import aslint.common_checkers as COMM
    import aslint.check_global as GLOB
    import aslint.fortran.gfortran_checkers as GFORT
    import aslint.fortran.static_fortran_checkers as STAT
    import aslint.other.c_checkers as CC
    import aslint.other.cxx_checkers as CXX
    import aslint.other.capy_checkers as CAPY
    import aslint.other.cata_checkers as CATA
    import aslint.other.datg_checkers as DATG
    import aslint.other.mater_checkers as MATER
    import aslint.other.other_checkers as OTHER
    import aslint.python.python_checkers as PY
    import aslint.test.test_checkers as TEST
    import aslint.check_diff as DIFF
    chk = BCH.CheckList()
    chk.register(BCH.checkers_from_context(COMM.__dict__, BCH.BaseMsg))
    chk.register(BCH.checkers_from_context(GLOB.__dict__, BCH.BaseMsg))
    chk.register(BCH.checkers_from_context(GFORT.__dict__, BCH.BaseMsg))
    chk.register(BCH.checkers_from_context(STAT.__dict__, BCH.BaseMsg))
    chk.register(BCH.checkers_from_context(CC.__dict__, BCH.BaseMsg))
    chk.register(BCH.checkers_from_context(CXX.__dict__, BCH.BaseMsg))
    chk.register(BCH.checkers_from_context(CAPY.__dict__, BCH.BaseMsg))
    chk.register(BCH.checkers_from_context(CATA.__dict__, BCH.BaseMsg))
    chk.register(BCH.checkers_from_context(DATG.__dict__, BCH.BaseMsg))
    chk.register(BCH.checkers_from_context(MATER.__dict__, BCH.BaseMsg))
    chk.register(BCH.checkers_from_context(OTHER.__dict__, BCH.BaseMsg))
    chk.register(BCH.checkers_from_context(PY.__dict__, BCH.BaseMsg))
    chk.register(BCH.checkers_from_context(TEST.__dict__, BCH.BaseMsg))
    chk.register(BCH.checkers_from_context(DIFF.__dict__, BCH.BaseMsg))
    return chk

def test_messages():
    """test unicity of message ids"""
    checker = setUp()


def build_messages_doc():
    """build a reStructuredText doc of the messages"""
    import aslint.base_checkers as BCH
    checker = setUp()
    text = [
        ".. _messages-identifiers:", "",
        "=========================", "Messages identifiers",
        "=========================", "",
        "Description", "~~~~~~~~~~~~", "",
        "*This documentation is automatically extracted from* "
        ":file:`aslint.base_checkers`", "",
        BCH.__doc__, "",
        "List of messages", "~~~~~~~~~~~~~~~~", "",
    ]
    fmt = '- ``%-5s`` %s%s\n'
    for msg in checker.values:
        text.append(fmt % (msg.id, msg.doc(), msg.descr(indent=8)))
    text.append("")
    print(os.linesep.join(text))



if __name__ == "__main__":
    if '-doc' in sys.argv:
        build_messages_doc()
    else:
        import sys
        from testutils import get_test_suite
        RET = unittest.TextTestRunner(verbosity=2).run(get_test_suite(__name__))
        sys.exit(not RET.wasSuccessful())
