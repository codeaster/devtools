# coding=utf-8

"""
Tests for Git API.

submit :
.root
['.']

"""

import unittest

try:
    from repository_api import UI
except ImportError as exc:
    raise NotImplementedError("It seems that PYTHONPATH points to an old version.") from exc

from hamcrest import *
from repository_api import (
    UI,
    get_branch,
    get_description,
    get_list_of_changed_files,
    get_main_branch,
    get_parent_desc,
    get_repo_by_rev,
    get_repo_name,
    hg,
    is_repository,
    parent_is_last,
    repocmd,
)
from testutils import REPO, only_on_repository


def test_commands():
    assert_that(is_repository(REPO), equal_to(True))
    # branch name should not start with vNN in devtools
    assert_that(get_main_branch(REPO), equal_to("main"))

    assert_that(get_repo_name(REPO), equal_to("devtools"))
    assert_that(get_repo_by_rev(REPO), equal_to("devtools"))

    assert_that(parent_is_last(REPO) or True)  # at least pass!
    assert_that(get_parent_desc(REPO), instance_of(str))
    assert_that(get_description(REPO, "e3f50b9", "1b708df"), instance_of(list))

    changes = get_list_of_changed_files(REPO, ["e3f50b9", "1b708df"])
    lines = changes.splitlines()
    assert_that(lines, has_length(12))
    assert_that([i for i in lines if i.startswith("D")], has_length(2))
    assert_that([i for i in lines if i.startswith("A")], has_length(1))
    assert_that([i for i in lines if i.startswith("M")], has_length(9))

    changes = get_list_of_changed_files(REPO, ["e3f50b9", "1b708df"], files=["share/test"])
    lines = changes.splitlines()
    assert_that(lines, has_length(6))
    assert_that([i for i in lines if i.startswith("D")], has_length(0))
    assert_that([i for i in lines if i.startswith("A")], has_length(1))
    assert_that([i for i in lines if i.startswith("M")], has_length(5))


@only_on_repository("hg")
def test_hg():
    repo = hg.Repository(REPO)
    assert_that(repo.root, equal_to(REPO))

    current = repo["tip"]
    assert_that(current.rev(), instance_of(int))
    assert_that(current.hex(), instance_of(str))
    assert_that(str(current), equal_to(current.hex()))
    assert_that(int(current.hex(), 16), instance_of(int))
    assert_that(current.phase(), is_in(["secret", "draft", "public"]))
    assert_that(current.user(), instance_of(str))
    assert_that(current.description(), instance_of(str))
    assert_that(current.branch(), instance_of(str))

    same = repo["tip"]
    assert_that(current == same, equal_to(True))
    assert_that(current != same, equal_to(False))

    parent = current.ancestor(same)
    assert_that(current == parent, equal_to(True))

    assert_that(current.children(), empty())

    assert_that(calling(hg.Repository).with_args("xx"), raises(IOError))

    repo = hg.repository("fake ui", REPO)
    assert_that(repo.root, equal_to(REPO))

    ui = UI.ui()
    # hope 'username' is defined and contains an email address!
    assert_that("@", is_in(ui.config("ui", "username")))
    assert_that(ui.configbool("ui", "unknown_option"), equal_to(False))
    assert_that(calling(ui.configbool).with_args("ui", "username"), raises(ValueError))


@only_on_repository("git")
def test_utils():
    assert_that(is_repository(REPO, "git"), equal_to(True))
    iret = repocmd(REPO, ("status",))[0]
    assert_that(iret, equal_to(0))
    branch = get_branch(REPO)
    assert_that(branch, instance_of(str))


if __name__ == "__main__":
    import sys

    from testutils import get_test_suite

    RET = unittest.TextTestRunner(verbosity=2).run(get_test_suite(__name__))
    sys.exit(not RET.wasSuccessful())
